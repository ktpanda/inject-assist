
include_py('animate-common.py')

#syringe_pos_2.move(ev=31)
#wait(syringe_pos_1.move(dv=[0, 0, 2]))
#wait(syringe_pos_2)
def anim():
    global time


    puller_pos_inserted = [0, -22, 116]
    puller_pos_base = puller_pos_inserted + [-20, -40, 20]
    attach_pos_base = puller_pos_base + [0, 9, -16]

    attach_pos_end = puller_pos_inserted + [0, -19, -16]

    #attach_pos.snap([0, 5, 10])
    attach_pos.reset(attach_pos_base)
    puller_pos.reset(puller_pos_base)
    plunger_pos.reset(30)
    syringe_ofs_2.reset([0, 20, 0])

    attachment_ang.reset(180)
    puller_ang.reset(180)

    attach_pos.speed = puller_pos.speed = 30

    ptfe_ang.snap(-90)
    hand_puller_ang_1.snap(-90)

    puller_alpha.snap(1.0)

    hand_puller_pos.reset([-80, 60, 90])
    hand_puller_slide.snap(20)

    preview_ptfe_tube.snap(True)
    preview_hand_puller.snap(True)
    preview_hand_puller_rod.snap(True)
    preview_hand_puller_screw.snap(True)


    move_view(view_whole, 0)
    wait(.5)

    # zoom in
    move_view(view_close_front_2, 1.0)

    wait(.5)

    # flip puller up
    syringe_ang.speed = 90
    wait(syringe_pos.move(ev=1, speed=8))
    syringe_pos.move(ev=4, speed=8)

    wait(syringe_ang.move(ev=90))

    wait(.5)

    syringe_ang.speed = 60

    # insert syringe
    preview_syringe.snap(True)
    preview_needle_cap.snap(True)

    wait(.5)

    wait(syringe_ofs_2.move(ev=[0, 0, 0]))

    wait(.5)

    move_view(view_close_rear, 1.0)

    wait(.5)


    # move puller in

    attach_pos.move(dv=[20, 0, 0])
    wait(puller_pos.move(dv=[20, 0, 0]))

    wait(.5)

    # pull out attachment
    wait(attach_pos.move(dv=[0, -6, 0]))
    t0 = time
    wait(attach_pos.move(dv=[0, 0, -40]))
    #attachment_ang.move(ev=30, st=t0, et=time)

    wait(.5)

    t0 = time
    wait(puller_pos.move(dv=[0, 40, 0]))
    attach_pos.move(dv=[0, 35, 0], st=t0, et=time)

    wait(.5)

    # insert puller
    attach_pos.move(dv=[0, 0, -20])
    wait(puller_pos.move(dv=[0, 0, -20]))

    wait(.5)

    # attach to plunger
    t0 = time
    wait(attach_pos.move(ev=attach_pos_end + [0, 0, -15]))
    wait(attach_pos.move(ev=attach_pos_end))

    wait(.5)

    # zoom out
    xview = [view_whole[0] + [0, 50, 30], view_whole[1], view_whole[2]]
    move_view(xview, 1)

    wait(.5)

    # remove cap

    wait(needle_cap_ofs.move(ev=[0, 0, -100], dt=.75))
    preview_needle_cap.snap(False)

    wait(.5)

    puller_perspective.snap(True)

    # load syringe

    t0 = time

    wait(syringe_ang.move(ev=70))
    syringe_ang.move(ev=0)
    wait(syringe_pos.move(ev=3))
    syringe_rot_origin.move(ev=[0, 0, 93])
    syringe_pos.move(ev=32)



    #wait(syringe_ang.move(ev=30))
    wait(syringe_ang)

    #t1 = time
    #time = t0
    #wait(view_pos.move(dv=[0, 50, 50], et=t1))

    syringe_pos.move(ev=28)

    wait(syringe_pos)

anim()
wait(1)

go('animate_insert_vars')
