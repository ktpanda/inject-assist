include <common.scad>

/* [Common] */
part = "complete"; // [complete, main, stopper, top]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : 1 : 50]

/* [Preview parts] */
preview_main = true;
preview_top = true;
preview_stopper = true;

preview_cap = true;
preview_syringe = true;

preview_hinge_ang = 0; // [0 : 0.5 : 160]
preview_syringe_pos = 0; // [0 : 0.5 : 80]

/* [Parameters] */
capsize = "23g"; // [23g, 18g]

module __nc__() {} // end of customizer variables

articulation_pos = 60;

fit_clearance = 0.2;

syringe_length = 74;
cap_length = 43.2;

front_length_a = 2.5;
front_length_b = 5;

rear_length = 12;

zp1 = 46;
zp2 = 5 + cap_length;

stopper_ang = 6;
stopper_len = 45;
stopper_screw_depth = -7.2;
stopper_head_width = 7.5;
stopper_width = 4.8;
stopper_paddle_width = 11;
stopper_depth = 6.5;

stopper_depth_ofs = 4 * tan(stopper_ang);

cap_lip_diameter = 8.6;

module complete() {
    if (preview_main) {
        main();
    }
    if (preview_stopper) {
        color("green")
        tz(stopper_len - 4)
        rx(90)
        tz(-stopper_depth)
        rx(-stopper_ang)
        ty(4)
        stopper();
    }

    if (preview_syringe) {
        color("blue", 0.5)
        tz(preview_syringe_pos-18)
        syringe_model();
    }

    if (preview_cap) {
        color("red", 0.5)
        interior();
    }

    if (preview_top) {
        rpos = [0, rear_length-3, articulation_pos-4];

        translate(rpos)
        rx(-preview_hinge_ang)
        translate(-rpos)
        //tz(syringe_length+12)
        //ry(180)
        top();
    }
}

module cylinder_cutout(z1, z2, d, d2=undef) {
    d2 = is_num(d2) ? d2 : d;

    tz(z1-eps)
    cylinder(h=z2-z1+eps2, d=d);

    if (d2)
    cubex(
        xc=0, xs=d2,
        y1=-front_length_b-eps, y2=0,
        z1=z1-eps, z2=z2+eps
    );
}

module tip_23g(hh) {
    cylinder(d=cap_tip_width_23g + fit_clearance*2, h=hh);
    for (ang = [0, 90, 180, 270]) rz(ang) {
        tx(cap_tip_width_23g/2 + fit_clearance)
        cylinder(h=hh, d=1.35);
    }
}


//!hub_tip_18g();

module interior_23g() {
    cap_lip_diameter = 8.6;
    cap_hub_length = 14;

    cylx(-14, -1, d=cap_diameter_23g + 2*fit_clearance);
    cylx(-1 - fit_clearance, fit_clearance, d=cap_lip_diameter + fit_clearance);
    tz(-cap_length)
    tip_23g(cap_length+eps2);
}

module interior_18g() {
    cap_diameter_18g = 6.6;
    cap_diameter_18g_2 = 6.2;
    cap_hub_length = 25;

    cylx(-cap_length, -1, d=cap_diameter_23g + 2*fit_clearance);
    cylx(-1 - fit_clearance, fit_clearance, d=cap_lip_diameter + fit_clearance);
}

module screw_m2() {
    cylx(z1=-10, z2=eps, d=screw_diameter_m2);
    cylx(z1=0, z2=1.6, d1=screw_diameter_m2, d2=4.9);
    cylx(z1=1.6-eps, z2=5, d=4.9);
}

//screw_m2();

module stopper() {
    y1 = -.5;
    y2 = -7.5;
    y3 = -12;
    y4 = -stopper_len + 3;
    y5 = -stopper_len + .5;
    difference() {
        union() {
            cubex(
                xc=0, xs=stopper_head_width,
                y1=y1, y2=y2,
                z1=0, z2=.9
            );
            *difference() {
                cubex(
                    xc=0, xs=stopper_head_width,
                    y1=y1, y2=y2,
                    z1=0, z2=1.8
                );
                ty(-4)
                rx(stopper_ang)
                tz(stopper_depth)
                tz(stopper_screw_depth) {
                    cubex(
                        xc=0, xs=10,
                        yc=0, ys=10,
                        z1=1.8, z2=10
                    );
                }
            }
        }
        ty(-4)
        rx(stopper_ang)
        tz(stopper_depth)
        tz(stopper_screw_depth)
        screw_m2();
    }
    cubex(
        xc=0, xs=stopper_width,
        y1=y2, y2=y3,
        z1=0, z2=.9
    );
    difference() {
        union() {
            cubex(
                xc=0, xs=stopper_width,
                y1=y3, y2=y4,
                z1=0, z2=.9
            );
            cubex(
                xc=0, xs=stopper_paddle_width,
                y1=y4, y2=y5,
                z1=0, z2=.9
            );
        }
        cubex(
            xc=0, xs=2,
            y1=y5+20, y2=y5-eps,
            z1=.3, z2=2
        );
    }

    *ty(-4)
    rx(stopper_ang)
    tz(stopper_screw_depth)
    {
        screw_m2();
    }

}
//!stopper();

module main_body(width=16) {
    difference() {
        union() {
            cubex(
                xc=0, xs=width,
                y1=-front_length_a, y2=rear_length,
                z1=zp1, z2=articulation_pos,
                r1=2, r2=2
            );
            cubex(
                xc=0, xs=width,
                y1=-front_length_b, y2=rear_length,
                z1=-cap_length + 20, z2=zp1,
                r1=2, r2=2
            );

        }

        // hinge groove
        cubex(
            xc=0, xs=6,
            y1=rear_length-6, y2=rear_length+eps,
            z1=articulation_pos-8, z2=articulation_pos+eps
        );

        tz(articulation_pos-4)
        ty(rear_length-3)
        yzx() {
            render(convexity = 4)
            difference() {
                cubex(
                    x1=0, xs=4+eps,
                    y1=0, ys=4+eps,
                    zc=0, zs=width+eps
                );
                cylinder(h=width+eps2, center=true, d=8);
            }


            cylinder(h=width+eps2, center=true, d=screw_diameter_m2);
            cylx(z1=width/2+eps, zs=-1.6, d1=screw_diameter_m2, d2=4.8);
            tz(-width/2-eps) rz(30) hexnut_hole(h=2.5, d=nut_diameter_m2);
        }


        // main groove
        cubex(
            xc=0, xs=stopper_width + .6,
            y1=0, y2=stopper_depth - stopper_depth_ofs,
            z1=0, z2=stopper_len - 8 + eps
        );

        spw = stopper_paddle_width + 1.2;
        // wide point at bottom
        cubex(
            xc=0, xs=spw,
            y1=cap_lip_diameter/2 - .1 - eps, y2=stopper_depth - stopper_depth_ofs,
            z1=0, z2=4
        );

        ty(cap_lip_diameter/2 - .1)
        linear_extrude(syringe_length+eps)
        polygon([
            [-spw/2, -1.2],
            [-spw/2,  0],
            [ spw/2,  0],
            [ spw/2, -1.2],
            [0, -4],
        ]);

        tz(stopper_len - 4)
        {

            ty(stopper_depth)
            ry(-90)
            linear_extrude(stopper_head_width + .3, center=true, convexity=3)
            polygon([
                [4, stopper_depth_ofs],
                [-4, -stopper_depth_ofs],
                [-4, -stopper_depth],
                [4, -stopper_depth]
            ]);


            ty(7)
            tz(4)
            rx(-stopper_ang) {
                *cubex(
                    xc=0, xs=stopper_head_width + .3,
                    y1=-6, y2=0,
                    z1=-9, z2=0
                );
            }

            rx(90) {
                //tz(-7)
                tz(stopper_screw_depth)
                screw_m2();
                tz(-12-eps)
                hexnut_hole(h=3, d=nut_diameter_m2);
            }
        }
        *cubex(
            xc=0, xs=16+eps,
            y1=-front_length_b-eps, y2=1,
            z1=-45-eps, zs=25
        );

        cylinder_cutout(0, zp1, syringe_diameter_wide_1 + fit_clearance, 6);
        cylinder_cutout(zp1, syringe_length, syringe_diameter_wide_1 + fit_clearance);
    }
    tz(stopper_len - 4)
    ty(9)
    rx(90)
    bridge_support();

}

module interior() {
    if (capsize == "18g") interior_18g();
    if (capsize == "23g") interior_23g();
}

module main() {
    difference() {
        main_body();
        interior();
    }
}

module top() {
    difference() {
        union() {
            color("gold")
            cubex(
                xc=0, xs=16,
                y1=-front_length_a, y2=rear_length,
                z1=articulation_pos, z2=syringe_length + 12,
                r1=2, r2=2
            );
            color("green")
            cubex(
                xc=0, xs=6 - fit_clearance,
                y1=rear_length-6, y2=rear_length,
                z1=articulation_pos-4, z2=articulation_pos
            );

            color("green")
            tz(articulation_pos-4) ty(rear_length-3)
            yzx() cylinder(h=6 - fit_clearance, center=true, d=6);

        }
        tz(articulation_pos-4) ty(rear_length-3)
        yzx() cylinder(h=6, center=true, d=screw_diameter_m2);
        cylinder_cutout(articulation_pos, syringe_length, syringe_diameter_wide_1 + fit_clearance);
        cylinder_cutout(syringe_length, syringe_length + 12, syringe_diameter_wide_2 + fit_clearance + .4);
    }
}

module preview_main() {
}

module preview_stopper() {
}

module preview_top() {
}

module preview_complete() {
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
            preview_complete();
        } else {
            complete();
        }
    }
    else if (part == "main") {
        if ($preview) {
            main();
            preview_main();
        } else {
            rx(-90)
            main();
        }
    }
    else if (part == "stopper") {
        if ($preview) {
            stopper();
            preview_stopper();
        } else {
            stopper();
        }
    }
    else if (part == "top") {
        if ($preview) {
            top();
            preview_top();
        } else {
            rx(-90)
            top();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
