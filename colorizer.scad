
use <animate.scad>
use <animate-insert.scad>
use <animate-assemble.scad>

$fn = 30;

main_color = [1.0,0.53,0.847];

accent_color_1 = [0.13,0.13,0.13];

accent_color_2 = [0.93,0.93,0.93];


/* [Viewport] */
lock_viewport = true;

$vpr = lock_viewport ? [60, 0, 90 + 40] : $vpr;
$vpt = lock_viewport ? [0, 40, 80] : $vpt;
$vpd = lock_viewport ? 500 : $vpd;
$vpf = lock_viewport ? 22.5 : $vpf;

animate_insert_vars($t=0) {
    $main_color = main_color;

    $accent_color_1 = accent_color_1;

    $accent_color_2 = accent_color_2;

    $syringe_pos = 29;
    $plunger_pos = 15;

    $syringe_ofs_2 = [0, 0, 0];
    $preview_syringe = true;
    $puller_ang = 90;
    $attachment_ang = 90;

    $hand_puller_slide = 30;

    $hand_puller_pos = [-80, 40, 40];

    $hand_puller_ang_1 = 0;
    $ptfe_ang = 0;


    $puller_pos = [0, 18, 120 + $syringe_pos];
    $attach_pos = [0, 0, $puller_pos.z + $plunger_pos - 13];


    animate_main(show_rot_origin=false, lock_viewport=false);
}
//animate_assemble_vars() animate_main(show_rot_origin=false);
