include <ktpanda/consts.scad>
use <ktpanda/funcs.scad>
use <ktpanda/partbin.scad>
use <ktpanda/shapes.scad>

include <tray-params.scad>

module hinge_outer(base_size=7.5, ww=12, iw=6, xheight=10, cutofs=0) {
    xw = .6;
    difference() {
        tt = base_size;

        union() {
            yzx() {
                linear_extrude(ww, center=true, convexity=1)
                polygon([
                    [0, -xheight],
                    [0, tt/2],
                    [-tt, tt/2],
                    [-tt, -xheight+tt],
                ]);
            }

            tz(tt/2)
            tx(ww/2)
            ty(-tt/2)
            ry(-90)
            cylinder(h=ww, d=tt);
        }


        tz(tt/2)
        tx(ww/2+eps)
        ty(-tt/2)
        ry(-90) {
            cylinder(h=ww+eps2, d=screw_diameter_m2);
            hexnut_hole(h=2, d=nut_diameter_m2);

            cylx(z1=ww-1.6, z2=ww+eps2, d1=screw_diameter_m2, d2=4.8);
        }

        cubex(
            xc=0, xs=iw,
            y1=eps, ys=-tt - eps2,
            z1=-xheight+tt - cutofs, z2=tt + eps
        );
    }
}

module hinge_inner(base_size=7.5, ww=6, xlen=0, vofs1=0, vofs2=0) {
    tt = base_size;

    ty(-tt/2)
    yzx()
    {
        difference() {
            hull() {
                pts = [
                    [0, -tt/2],
                    [0, tt/2],
                    [tt/2+xlen, tt/2+vofs1],
                    [tt/2+xlen, -tt/2+vofs2]
                ];
                linear_extrude(ww, center=true)
                polygon(pts);
                cylinder(h=ww, d=tt, center=true);
            }
            cylinder(h=ww+eps2, center=true, d=screw_diameter_m2);
        }
    }
}

module tray_lid_interface(fc) {
    r = 2 + fc;
    sz(-1)
    ty([40, $tray_size.y - 15]) {
        lathe(r=[
            [-5, r],
            [3, r],
            [3 + r/2, r/2],
        ]);
    }
}

module tray_mainbox_outer() {
    minkowski(convexity=4) {
        cubex(
            x1 = $tray_outer_radius,
            x2 = $tray_size.x - $tray_outer_radius,
            y1 = $tray_outer_radius,
            y2 = $tray_size.y - $tray_outer_radius,
            z1 = -$tray_bot_height,
            z2 = 0 - $tray_outer_radius/2
        );
        union() {
            cylinder(h=$tray_outer_radius/2, r1=$tray_outer_radius, r2=$tray_outer_radius/2);
            cylinder(h=1, r=$tray_outer_radius);
        }

    }

}

module tray_groove() {
    for (xx = [20, $tray_size.x - 20]) tx(xx) {
        sz(.6) {
            ry(45)
            cubex(xc=0, xs=5, zc=0, zs=5, y1=-eps, y2=$tray_size.y+eps);
            cubex(xc=0, xs=5*sqrt(2), z1=0, zs=-5, y1=-eps, y2=$tray_size.y+eps);
        }
    }
}

module infill_split(y1=0, y2=$tray_size.y, xs=.1) {
    npts = 5;
    for (xx = [1 : 1 : npts-1]) {
        tx(xx*($tray_size.x)/npts)
        squarex(
            xc=0, xs=xs,
            y1=y1, y2=y2
        );
    }
}

module hinge_transform() {
    tx($tray_size.x / 2) rfx()
    tx($tray_hinge_spacing / 2)
    children();
}


module screw_cutout() {
    tx($tray_size.x/2)
    ty($tray_size.y)
    xzy() {
        tz(-8) {
            cylinder(h=8, d=screw_diameter_m3);
            tz(1)
            hull() {
                for (yy = [0, 10]) ty(yy)
                rz(30)
                hexnut_hole(h=2.5, d=nut_diameter_m3);
            }
        }
    }
}

module latch_cut(l, dia, main_dia) {
    ang_a = 90;
    ang_b = 0;
    npts = 50;
    r1_a = l + main_dia/2 - 0.5;
    r1_b = l + main_dia/2 + 1.5;
    r2_a = l - main_dia/2;
    r2_b = dia/2;

    function polar(r, t) = [r*cos(t), r*sin(t)];
    function latchpts(v, ra, rb) = [
        for (i=v) polar(lerp(ra, rb, i), lerp(ang_a, ang_b, i))
    ];

    ppts = [
        each latchpts([0 : 1/npts : 1], r1_a, r1_b),
        each latchpts([1 : -1/npts : 0], r2_a, r2_b)
    ];

    polygon(ppts);
}

module tray_latch(h1=5, h2=5, l=11.2, dia=10) {
    head_dia = 5.62;

    s_thick = 1.6;


    difference() {
        union() {
            hull() {
                for (yy = [0, l]) ty(yy)
                cylinder(h1, d=dia);
            }
            //cylx(z1=0, z2=h2, d=dia);
        }

        cylx(z1=-eps, z2=h2+eps, d=screw_diameter_m3);
        cylx(z1=s_thick, z2=h2+eps, d=head_dia+.1);

        tz(s_thick)
        linear_extrude(h1+eps2, convexity=3)
        latch_cut(l, dia, head_dia);

        tz(-eps)
        linear_extrude(h1+eps2, convexity=3)
        latch_cut(l, 1.5*dia, screw_diameter_m3);

        ty(l) {
            cylx(z1=s_thick, z2=h1+eps, d=head_dia);
            cylx(z1=-eps, z2=h2+eps, d=screw_diameter_m3);
        }

    }
}

module tray_custom_inlay() {

}

module tray_inlay() {
    tray_custom_inlay();


    //color("gold")
    //tz($tray_top_height - pattern_thickness)
    difference() {
        infill_split(
            y1=$tray_outer_radius/2 + 1,
            y2=$tray_size.y - $tray_outer_radius/2 - 1,
            xs=3,
        );

        offset(6)
        tray_custom_inlay();
    }
}

module tray_inlay_part() {
    pattern_thickness = layer_height * $tray_pattern_layers;
    lxt($tray_top_height, zs=-pattern_thickness)
    tray_inlay();
}

module tray_inner() {

}

module tray_lid() {
    difference() {
        color("#333333")
        minkowski() {
            cubex(
                x1 = $tray_outer_radius,
                x2 = $tray_size.x - $tray_outer_radius,
                y1 =  $tray_outer_radius,
                y2 = $tray_size.y - $tray_outer_radius,
                z1 = -$tray_outer_radius,
                z2 = $tray_top_height - $tray_outer_radius/2
            );
            cylinder(h=$tray_outer_radius/2, r1=$tray_outer_radius, r2=$tray_outer_radius/2);
        }
        minkowski() {
            tz(.2)
            tray_mainbox_outer();
            cube(0.005, center=true);
        }

        tray_inner();

        *tz($tray_top_height)
        sz(-1)
        tray_groove();

        sz(-1)
        tz(-5)
        screw_cutout();

        if (enable_pattern) {
            color("gold")
            lxt($tray_top_height - layer_height * $tray_pattern_layers, $tray_top_height + eps)
            tray_inlay();
        }



        *tz($tray_top_height)
        infill_split();
    }

    tz(-.2) {
        tx(8)
        tray_lid_interface(-fit_clearance);

        tx($tray_size.x - 8)
        tray_lid_interface(-fit_clearance);

        ty(-1)
        hinge_transform()
        hinge_inner(base_size=$tray_hinge_size - 2, xlen=1+eps2, vofs1=4.5, vofs2=0.65);
    }

}

module tray_main() {
    difference() {
        color("#333333")
        tray_mainbox_outer();

        tz(eps)
        tx(8)
        tray_lid_interface(fit_clearance);

        tz(eps)
        tx($tray_size.x - 8)
        tray_lid_interface(fit_clearance);

        tray_inner();

        cubex(
            x1 = 0,
            x2 = $tray_size.x,
            y1 = 0,
            y2 = $tray_size.y,
            z1 = -.8,
            z2 = eps
        );

        tz(-6)
        screw_cutout();
    }

    translate([$tray_size.x / 2, $tray_size.y])
    lxt(z1=-$tray_bot_height, zs=4) difference() {

        let(rad = $tray_handle_rad + $tray_handle_thick)
        squarex(
            xc = 0, xs = $tray_handle_size.x + 2*$tray_handle_thick,
            y1=-eps, y2=$tray_handle_size.y + $tray_handle_thick,
            r3=rad, r4=rad
        );

        let(rad = $tray_handle_rad)
        squarex(
            xc = 0, xs = $tray_handle_size.x,
            y1=-eps, y2=$tray_handle_size.y,
            r3=rad, r4=rad
        );

    }

    tz(-$tray_hinge_size/2)
    hinge_transform()
    sx(-1)
    hinge_outer(base_size=$tray_hinge_size, ww=16, cutofs=1);
}

module tray_preview(lid_ang=0) {
    tray_main();

    rc = [0, -$tray_hinge_size/2, 0];

    translate(rc)
    rx(lid_ang)
    translate(-rc)
    tz(0.4)
    tray_lid();

    ty($tray_size.y)
    tx($tray_size.x/2)
    tz(-6)
    rx(-90)
    rz(180)
    tray_latch();

}
