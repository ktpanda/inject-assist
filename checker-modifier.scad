include <ktpanda/funcs.scad>
include <ktpanda/shapes.scad>

gridsize = 50;
height = 200;

for (xx = [0 : gridsize : 250], yy = [0 : gridsize : 250]) {
    if (((xx + yy) / gridsize) % 2 == 0) {
        cubex(
            x1=xx, xs=gridsize-eps,
            y1=yy, ys=gridsize-eps,
            z1=0, z2=height
        );
    }
}