include <common.scad>
use <syringe-tray.scad>

/* [Common] */
part = "main"; // [main, spring, springx8]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : 1 : 50]

/* [Preview parts] */
preview_spring = true;
preview_boxes = false;

/* [Parameters] */

module __nc__() {} // end of customizer variables

box_height = 120; // [20 : 2 : 120]
spring_height = 50; // [50 : 2 : 80]
spring_z = 2; // [0 : 2 : 80]
test_slice = false;
spring_pv_z = 0; // [-2 : .1 : 2]
spring_pv_y = 0; // [0 : .1 : 2]

box_count = 4;
wall_thick = 6;

tray_width_extra = 10;
tray_count = 5;
tray_spacing = 18;

tray_height = 33.2;
tray_width = 1.2 + tray_width_extra * 2 + tray_spacing * tray_count;

box_spacing = 8;

spring_catch_len = .8;

fit_clearance = .1;

//shift = box_count == 1 ? 0 : 5;
shift = 0;

outer_extent_a = [
    wall_thick,
    wall_thick,
    2
];
outer_extent_b = [
    tray_width + wall_thick,
    wall_thick + ((box_spacing + tray_height) * box_count) - box_spacing,
    box_height
];
//echo(outer_extent_b);

module slot_transform() {
    for (i = [0 : 1 : box_count - 1]) ty((box_spacing + tray_height) * i)
    children();
}

module spring_transform() {
    pts = [
        [-outer_extent_a.x + 25 + shift, -1],
        [outer_extent_b.x - 25 - shift, -1],
        [(tray_width/2) - shift, -1],
        [(tray_width/2) + shift, 1],
        [-outer_extent_a.x + 25 - shift, 1],
        [outer_extent_b.x - 25 + shift,  1]
    ];
    for (zz = [spring_z : spring_height + 10 : box_height - 5 - spring_height]) tz(zz)
    tz(spring_z)
    ty(tray_height/2) {
        for (pt = pts) tx(pt.x) sy(pt.y) ty(-tray_height/2) {
            ty(-3.5)
            rz(90)
            rx(90)
            children();
        }
        tx(tray_width/2)
        for (xx = [-1, 1]) sx(xx) tx(-tray_width/2) {
            tx(-3.5)
            rx(90)
            children();
        }
    }
}
module slot_cutout(zofs=0) {
    tx(tray_width)
    ry(-90)
    *linear_extrude(tray_width, convexity = 2)
    polygon([
        [0, 0],
        [0, tray_height],
        [outer_extent_b.z-2, tray_height],
        [outer_extent_b.z+eps, tray_height+2],
        [outer_extent_b.z+eps, -2],
        [outer_extent_b.z-2, 0],
    ]);

    cubex(
        x1=0, x2=tray_width,
        y1=0, y2=tray_height,
        z1=0, z2=outer_extent_b.z+eps
    );
    tz(outer_extent_b.z-4)
    minkowski() {
        //sx(1)
        cylinder(4, r1=0, r2=3.5);
        cubex(
            x1=0, x2=tray_width,
            y1=0, y2=tray_height,
            zc=0, zs=eps
        );
    }
    spring_transform() {
        fc = fit_clearance;
        sz = spring_height - 2.6;
        linear_extrude(8.2, center=true, convexity=2)
        polygon([
            [ 0, spring_height + .8],
            [ 0, sz- spring_catch_len],
            [ 1+fc, sz- spring_catch_len],
            //[.5, sz ],
            //[.5, sz],
            [ 1+fc, sz],
            [ 2, sz],

            [ 2, 6],
            [ 1, 4],

            [1, -3],
            [8, -3],
            [8, spring_height + .8],

        ]);
        *cubex(
            x1=1, x2=3,
            zc=0, zs=8,
            y1=0, y2=box_height - 4 + .4
        );
        *cubex(
            x1=0, x2=3,
            zc=0, zs=8,
            y1=box_height - 6.6, y2=box_height - 4 + .4
        );
    }
}

module spring() {
    step = spring_height / 100;
    arc_scale = 4;
    arc_ofs = 3;
    end_len = 2.5;
    function arc(a, b, ofs) = [
        for(i = [each [a : step : b], b])
        [ofs + arc_scale * sin(i*180/spring_height), i]
    ];

    //echo(arc(end_len, spring_height-end_len, arc_ofs - 0.8)[0]);

    render(convexity=2)
    difference() {
        linear_extrude(8, center=true, convexity=2) {
            polygon([
                [0, spring_height],
                [0, 5],
                [1, 5],
                [1, 0],
                each arc(0, spring_height, arc_ofs)
            ]);

        }
        linear_extrude(10, center=true, convexity=2) {
            polygon([
                [1, spring_height-end_len],
                [1, spring_height-end_len-spring_catch_len],
                [-eps, spring_height-end_len-spring_catch_len],
                //[-eps, end_len+spring_catch_len],
                [-eps, end_len],
                [1, end_len],
                each arc(end_len, spring_height-end_len, arc_ofs - 1.2)
            ]);

        }
    }
}

module springx(n=8, spc=10) {
    difference() {
        cubex(
            x1=0, x2=(n)*spc,
            y1=-2, y2=spring_height+2,
            z1=0, z2=0.2
        );
        for (i=[0 : 1 : n-1]) tx(spc*(i)) {
            hull() {
                spring();
                tx(-1)
                spring();
            }
        }
    }
    tz(4)
    for (i=[0 : 1 : n-1]) tx(spc*(i)) {
        spring();
    }
}

module main() {
    difference() {
        cubex(
            x1 = -outer_extent_a.x,
            x2 = outer_extent_b.x,
            y1 = -outer_extent_a.y,
            y2 = outer_extent_b.y,
            z1 = -outer_extent_a.z,
            z2 = outer_extent_b.z,
            r=4
        );
        slot_transform() {
            slot_cutout();
        }
    }

    if ($preview && preview_spring) {
        slot_transform() {
            spring_transform() {
                tx(spring_pv_y)
                ty(spring_pv_z)
                color("green")
                spring();
            }
        }
    }
    if ($preview && preview_boxes) {
        slot_transform() {
            cubex(
                x1=0, x2=tray_width,
                yc=tray_height/2, ys=32.7,
                z1=1, zs=100
            );
        }
    }
    *render()
    slot_transform() {
        intersection() {
            tx(tray_width_extra + 1)
            rx(-90) {
                tz(.9)
                tray_groove();

                tz(tray_height-.9)
                sz(-1)
                tray_groove();
            }

            slot_cutout(-tray_height/2);
        }
    }
}

module preview_main() {
}

module preview_spring() {
}

module preview_springx(n) {
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "main") {
        if ($preview) {
            main();
            preview_main();
        } else {
            main();
        }
    }
    else if (part == "spring") {
        if ($preview) {
            spring();
            preview_spring();
        } else {
            spring();
        }
    }
    else if (part == "springx8") {
        if ($preview) {
            springx(8);
            preview_springx(8);
        } else {
            springx(8);
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
