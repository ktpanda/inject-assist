include <ktpanda/funcs.scad>
include <ktpanda/partbin.scad>
include <ktpanda/shapes.scad>

$fn = 50;
fit_clearance = 0.1;

bearing_diameter_o = 15;
bearing_length = 24;

// height above skin
bearing_dist = 20;

// length of connected hub from bottom of syringe to start of needle
needle_hub_length = 15.5;

// widest point of needle hub
needle_hub_diameter = 5.75;

syringe_diameter = 9.6;

rod_spacing = 40;
outer_width = 60;
inner_width = 13;
main_length = 16;
sleeve_diameter_o = 20;

module bearing_sleeve() {
    nut_flange_x1 = -sleeve_diameter_o/2 - 6;
    nut_flange_x2 = -(bearing_diameter_o + sleeve_diameter_o)/4;
    nut_x = nut_flange_x1 + 3.5;
    nut_z1 = 4;
    nut_z2 = bearing_length - nut_z1;

    difference() {
        union() {
            cylinder(h=bearing_length, d=sleeve_diameter_o);
            cubex(
                x1=0, x2=rod_spacing/2,
                yc=0, ys=main_length,
                z1=0, z2=bearing_length
            );
            cubex(
                x1=nut_flange_x1, x2=nut_flange_x2,
                yc=0, ys=7,
                z1=0, z2=bearing_length
            );

        }

        tz(-eps)
        cylinder(h=bearing_length + eps2, d=bearing_diameter_o+0.1);
        cubex(
            x1=nut_flange_x1-eps, x2=0,
            yc=0, ys=1,
            z1=-eps, z2=bearing_length+eps
        );

        for (zz = [nut_z1, nut_z2]) {
            tx(nut_x)
            ty(-4.5-eps)
            tz(zz)
            rx(-90)
            rz(30) {
                hexnut_hole(d=nut_diameter_m2, h=3);
                cylinder(h=10, d=screw_diameter_m2);
                tz(6)
                cylinder(h=3+eps2, d=nut_diameter_m2);
            }

        }
    }
}

module main() {
    z2 = needle_hub_length;
    z1 = z2 - (syringe_diameter - needle_hub_diameter);
    z3 = bearing_dist + bearing_length + 25;
    echo(z3);
    difference() {
        union() {
            cubex(
                xc=0, xs=inner_width,
                yc=0, ys=main_length,
                z1=0, z2=z3
            );
            for (xx = [-1, 1]) {
                sx(xx)
                tz(20)
                tx(-rod_spacing/2)
                //tx(-sleeve_diameter_o/2)
                bearing_sleeve();
            }
        }
        tz(-eps)
        cylinder(z1 + eps2, d=needle_hub_diameter);
        tz(z1)
        cylinder(z2 - z1 + eps, d2=syringe_diameter, d1=needle_hub_diameter);
        tz(z2)
        cylinder(z3 - z2 + eps, d=syringe_diameter);
        cubex(
            xc=0, xs=syringe_diameter,
            y1=1.5, ys=main_length/2 + eps,
            z1=z3-10, z2=z3+eps
        );

        //needle_slot_width = syringe_diameter/2+1;
        //echo(needle_slot_width);
        *tx(needle_slot_width/2)
        ry(-90)
        linear_extrude(needle_slot_width) {
            xx = z3 - 45;
            yy = main_length/2 + eps;
            polygon([
                [xx, 1],
                [xx+yy-1, yy],
                [z3, yy],
                [z3, 1]
            ]);
        }
        cubex(
            xc=0, xs=needle_hub_diameter, // 4
            y1=1, ys=main_length/2 + eps,
            z1=z3-50, z2=z3+eps
        );


    }
}

//rx($preview ? 90 : 0)
main();
