include <common.scad>

use <plunger-puller.scad>

/* [Common] */
part = "complete"; // [complete, main, pusher, pincher-ring, screw]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .2 : 140]

/* [Preview parts] */
preview_main = true;
preview_pusher = true;
preview_pincher_ring = true;
preview_screw = false;
$preview_brim = true;

preview_cutout = false;
preview_support = false;
preview_tube = false;


rod_pos = 0; // [0 : .5 : 130]

/* [Parameters] */

module __nc__() {} // end of customizer variables

/*************************************************************
* Fixed params                                              */

pusher_width = 12;
pusher_height = 10;
pusher_main_length = 115;

screw_params = screw_def(1.2, 7, tan(50));

cable_z = 2;

main_ext = 12;

main_width = 20;
main_thick = 2;

finger_dia_inner = 19;
finger_dia_outer = finger_dia_inner + 4;

thumb_dia = 26;

finger_pos_1 = [18, 15];
//finger_pos_2 = finger_pos_1 + rotpt([finger_dia_inner/2 + finger_dia_outer/2, 0], -24);

main_length = finger_pos_1.x + finger_dia_outer / 2;

screw_r1 = screw_params[2] / 2;
screw_r2 = 4.5;

pincher_base_thick = 1.6;
pincher_ring_thick = 2.0;

//------------------------------------------------------------

/*************************************************************
* Derived params                                             */

//------------------------------------------------------------

module needle_model() {
    color("#cccccc")
    cylx(0, needle_length, d=true_needle_dia);
}

module preview_cutout() {
    if (preview_cutout) {
        render(10)
        xslice(x1=0, x2=-100) children();
    } else {
        children();
    }
}

module pusher_xform() {
    ty(-rod_pos)
    children();
}

module screw_xform() {
    translate([0, pusher_main_length, cable_z])
    children();
}

module complete() {
    $preview_brim = false;

    if (preview_main) {
        color("#6666aa")
        main();
    }

    if (preview_pusher) {
        color("#ff88d8")
        pusher_xform()
        pusher();
    }

    if (preview_screw) {
        //z1 = pincher_base_thick;
        //z2 = pincher_ring_thick;

        color("#ff99e0")
        pusher_xform()
        screw_xform()
        turn_screw(screw_params, -1.0)
        screw();
    }

    let(h = 140 / 6)
    if (preview_tube) {
        ft = tmat(x=- h - 1);
        rt = rzmat(-25);

        t1 = rt;
        t2 = t1 * ft * rt;
        t3 = t2 * ft * rt;
        t4 = t3 * ft * rt;
        t5 = t4 * ft * rt;
        t6 = t5 * ft * rt;
        t7 = t6 * ft * rt;

        translate([-40, 4, 0]) {
            for (xf = [t1, t2, t3, t4, t5, t6])
            multmatrix(xf)
            ry(90)
            rz(90)
            tz(-h)
            color("#ff99e0")
            ptfe_tube_segment(h, tube_pos=[[0, 0]]);

            color("#6666aa")
            multmatrix(t7) {
                //translate()
                rz(-90) ry(180) translate([0, -16, -16])
                plunger_puller_syringe_interface(-90);

                rx(180)
                rz(90)
                translate([0, 8, -16])
                plunger_attachment();
            }
        }

    }

}

module main() {
    finger_rad_outer = finger_dia_outer / 2;
    x1 = finger_pos_1.x + finger_rad_outer;
    y1 = 0;
    y2 = main_width;

    ptfe_d1 = 1.4;

    path_data = ptfe_tube_data(join_paths([
        path_start(tmat(z=cable_z, y=eps) * rymat(90) * rxmat(180)),
        path_straight(x1)
    ]), d1=ptfe_d1);

    part_color(0)
    difference() {
        union() {
            lxt(-main_thick, pusher_height + main_thick)  difference() {
                union() {
                    pt1 = finger_pos_1 - [finger_rad_outer, 0];
                    polyx(rfx([
                        [0, 0],
                        [x1, 0],
                        [x1, finger_pos_1.y],
                        finger_pos_1,
                        pt1,
                        pt1 - [0, 5],
                    ]));

                    rfx() {
                        translate(finger_pos_1)
                        circle(d = finger_dia_outer);

                        *translate(finger_pos_2)
                        circle(d = finger_dia_outer);
                    }


                }
                rfx() {
                    translate(finger_pos_1)
                    circle(d = finger_dia_inner);

                    *hull() {
                        translate(finger_pos_2)
                        circle(d = finger_dia_inner);
                        let(
                            pt1 = finger_pos_1 + [finger_dia_outer/2, 0],
                            pt2 = pt1 + [0, 10],
                        )
                        *polyx([
                            finger_pos_2,
                            finger_pos_2 + [eps, 0],
                            pt1,
                            pt2
                        ]);
                    }
                }
            }
            yzx() lxt(-x1, -x1 - 9)
            squarex(x2 = 7.5, y1 = -2, y2 = cable_z + 4, r = -1);
        }

        cubex(
            xs = pusher_width + .6,
            y1 = -eps, y2 = y2 + eps,
            z1 = -.2, z2 = pusher_height + .6
        );

        ty(3.5)
        rz(-90)
        ptfe_holder_tube_inner(path_data, ptfe_d1);

    }

    part_color("support-volume", color="green", alpha=.5, flag=preview_support) {
        cubex(
            xs = pusher_width - .2,
            y1 = y1, y2 = y2,
            z1 = -1, z2 = pusher_height + 1
        );

    }
}

function lerprange(a, b, base=0, stepn=1, stepd=1) = [
    for (t = closed_range(0, 1, base=base, stepn=stepn, stepd=stepd))
    lerp(a, b, t)
];

module pusher() {
    thumb_y = -thumb_dia/2 - 2;
    screw_spc = 3;
    screw_y1 = pusher_main_length - screw_spc / 2;
    screw_y2 = pusher_main_length + screw_spc / 2;

    part_color(0)
    difference() {
        preview_cutout()
        lxt(0, pusher_height) difference() {
            union() {
                let(r=2)
                squarex(
                    xs = pusher_width,
                    y1 = 0, y2 = pusher_main_length + pusher_width/2,
                    r3=r, r4=r
                );

                let(w = 8)
                squarex(y2 = -w, xs = 40, r = w/2);
            }
        }

        tz(cable_z) {
            rfx()
            tx(-pusher_width/2)
            xzy() lxt(2, screw_y2)
            polyx([
                [-1, 0],
                [1, 0],
                [0, 1],
                [-1, 1]
            ]);

            ty([screw_y1, screw_y2])
            yzx()
            lxt(zs=pusher_width+eps)
            polyx(rfx([
                [.6, 0],
                [.6, 1],
                [0, 1.6],
            ]));

        }

        screw = screw_offset(screw_params, .4);

        ty(pusher_main_length)
        screw_thread(
            screw,
            cable_z + 2, pusher_height,
            ltaper=1, utaper = -1,
            uext = eps,
            lext = 2
        );
    }

    part_color("brim", "yellow", flag=$preview_brim)
    lxt(.4) {
        ty(pusher_main_length + 6) brim_tab_2d(w=5, d=16);
        rfx() ty(lerpn(10, pusher_main_length, 2)) tx(pusher_width/2) rz(-90) brim_tab_2d(w=5, d=16);
        ty(-8) rfx() tx(10) rz(180) brim_tab_2d(w=5, d=16);
    }
}

module pincher_ring() {
    preview_cutout()
    lxt(0, pincher_ring_thick) {
        difference() {
            circle(r=6);
            difference() {
                circle(r=screw_r2 + .06);

                rz(half_open_range_a(0, 360, stepn=360, stepd=4))
                tx(screw_r2 + .06)
                rz(45)
                square(sqrt(2), center=true);
            }
        }

    }
}

module screw_base(inner) {
    xeps = inner ? eps : 0;
    fc = inner ? .2 : 0;

    z1 = 0;
    z2 = pincher_base_thick;

    screw = screw_offset(screw_params, 2 * fc);
    r1 = screw[2] / 2;
    r2 = screw_r2 + fc;

    z3 = z2 + (r2 - r1);
    union() {
        lathe(r=[
            [z1, r2],
            [z2, r2],
            //[z3, r1],
        ]);

        tz(z2) screw_taper(pusher_height - cable_z -  z2, screw, taper_top = 1, taper_bot = -1);
    }

}

module screw() {
    screw = screw_params;

    z1 = cable_z + 2;
    z2 = pusher_height + .6;
    z3 = z2 + 1.4;

    part_color(0)
    difference() {

        preview_cutout()
        color("#ff60a0")
        union() {

            screw_thread(
                screw,
                z1, z2,
                ltaper=1, utaper = -1,
                uext = 0,
                lext = 2.6
            );

            cylx(z2, z3, d=pusher_width);
        }

        lxt(pusher_height - 1, pusher_height + 4) {
            squarex(xs = 4.8, ys = 1.4);
        }
    }

    let(d = pusher_width)
    tz(z3) sz(-1)
    part_color("brim", "yellow", flag=$preview_brim) render(10) difference() {
        rz(half_open_range_a(0, 360, stepn=360, stepd=3)) ty(d / 2 - 1) brim_tab(l=2, d=16);
        cylx(-1, 1, d=d);
    }
}

module screw_base_old(inner) {
    xeps = inner ? eps : 0;
    fc = inner ? .2 : 0;

    z1 = 0;
    z2 = 6;

    z4 = cable_z;
    z3 = z4 - 1;

    screw = screw_offset(screw_params, 2 * fc);

    d1 = screw[2];
    d2 = screw[1];
    d3 = d2 + 2.5;

    screw_taper(z2 - z1, screw, taper_top = -1, taper_bot = inner ? -1 : 1);

    lathe(d=[
        [z2, d2],
        [z3, d2],
        [z4, d3],
        [pusher_height - 1, d3]
    ]);

}

module screw_old() {
    part_color(0)
    difference() {
        screw_base(false);
        lxt(pusher_height - 4, pusher_height)
        squarex(xs = 6, ys = 1.4);
    }

    let(d = screw_params[1] + 2.5)
    tz(pusher_height - 1) sz(-1)
    part_color("brim", "yellow", flag=$preview_brim) render(10) difference() {
        rz(half_open_range_a(0, 360, stepn=360, stepd=3)) ty(d / 2 - 1) brim_tab(l=2, d=16);
        cylx(-1, 1, d=d);
    }
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
        } else {
            complete();
        }
    }
    else if (part == "main") {
        if ($preview) {
            main();
        } else {
            main();
        }
    }
    else if (part == "pusher") {
        if ($preview) {
            pusher();
        } else {
            pusher();
        }
    }
    else if (part == "pincher-ring") {
        if ($preview) {
            pincher_ring();
        } else {
            pincher_ring();
        }
    }
    else if (part == "screw") {
        if ($preview) {
            screw();
        } else {
            ry(180)
            screw();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
