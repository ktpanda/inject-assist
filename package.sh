#!/bin/bash

IFS=

date=`date +%Y-%m-%d`
mydir=`cd \`dirname $0\`; pwd`

pname=injector

src_zip=$mydir/dist/$pname-src-$date.zip
stl_zip=$mydir/dist/$pname-stl-$date.zip

tmpd=`mktemp -d`

mkdir -p dist
mkdir -p $tmpd/src/ktpanda $tmpd/src/syringe_model # $tmpd/src/images
mkdir -p $tmpd/stl

# {
#     cat <<EOF
# <!DOCTYPE html>
# <html>
# <head>
#   <title>Dry box</title>
#   <style type="text/css">
#     body {
#       font-family: sans-serif;
#     }
#     h1, h2, h3 {
#       background-color: #f2f2f2;
#       font-weight: bold;
#       border-bottom: 1px solid #eee;
#     }
#     h2, h3 {
#       margin-top: 1.5em;
#     }
#     pre {
#       margin-left: 30px;
#       margin-right: 30px;
#       background-color: #f8f8f8;
#       border: 1px solid #ddd;
#       border-radius: 3px 3px 3px 3px;
#     }
#     p > code {
#       background-color: #f8f8f8;
#       border: 1px solid #ddd;
#       border-radius: 3px 3px 3px 3px;
#     }
#   </style>
# </head>
#   <body>
# EOF

#     markdown README.md

# cat <<EOF
#   </body>
# </html>
# EOF
# } > $tmpd/src/README.html

#ls $pname-*.stl | xargs -n1 -P12 ./render-image.sh

for fil in $pname-*.stl; do
    echo "copy $fil to stl"
    #cp $fil $tmpd/src
    cp $fil $tmpd/stl
done

for fil in $pname.scad \
               $pname.json \
               cap-puller-articulating.scad \
               estradiol-molecule.svg \
               common.scad \
               makefile.py \
               make \
               genmake \
               README.md; do
    cp $fil $tmpd/src
done

#cp images/* $tmpd/src/images

for fil in consts.scad screw-defs.scad shapes.scad funcs.scad partbin.scad tests.scad scadutils.py stl.py build.py utils.py make.py autoindent.py; do
    cp ktpanda/$fil $tmpd/src/ktpanda/
done

for fil in syringe_model/*.stl; do
    cp $fil $tmpd/src/$fil
done

rm -rf "$src_zip" "$stl_zip"
(cd $tmpd/src; zip "$src_zip" -r .)
(cd $tmpd/stl; zip "$stl_zip" -r .)

rm -rf $tmpd
