include <ktpanda/funcs.scad>
include <ktpanda/shapes.scad>
use <cap-puller.scad>

difference() {
    cubex(xc=0, xs=12, yc=0, ys=12, z1=0, z2=2);
    tz(-eps)
    tip_23g(5);
}