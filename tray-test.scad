include <ktpanda/consts.scad>
use <ktpanda/funcs.scad>
use <ktpanda/partbin.scad>
use <ktpanda/shapes.scad>

highres = false;
$fn = (!highres && $preview) ? 30 : 100;


preview_main = true;
lid_ang = 0; // [0 : 1 : 180]

/* [Parameters] */
enable_pattern = false;


module __nc__() { }

include <tray-params.scad>

include <tray-base.scad>

module tray_inner() {
    translate([20, 20]) {
        cylx(-5, 5, d=5);
    }
}

tray_preview(lid_ang, $tray_bot_height = 24, $tray_top_height = 16);
