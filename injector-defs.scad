
inner_width = 16;

tower_y1 = -12;
tower_y2 = 8;

screw_y = tower_y1 + 1.5;

syringe_type_index = (
    (syringe_type == "wide") ? 0 :
    (syringe_type == "narrow") ? 1 :
    undef
);

syringe_data = [
    [ // wide
        /* main_height: */ 76,
        /* syringe_diameter: */ syringe_diameter_wide_1,
        /* syringe_diameter_upper: */ syringe_diameter_wide_2,
        /* syringe_diameter_lower: */ syringe_diameter_wide_1,
        /* syringe_lip_thick: */ 2.2,
        /* syringe_lip_length: */ 24.8,
        /* syringe_lip_width_2: */ 12.6,
        /* syringe_lip_width_2: */ 8.2,
        /* syringe_height: */ syringe_height_wide,
        /* plunger_height: */ plunger_height_wide,
        /* tower_expand */ 2.5
        ,
    ],
    [ // narrow
        /* main_height */ 76,
        /* syringe_diameter: */ syringe_diameter_narrow_1 + .3,
        /* syringe_diameter_upper: */ syringe_diameter_narrow_1 + .6,
        /* syringe_diameter_lower: */ syringe_diameter_narrow_2 + .3,
        /* syringe_lip_thick: */ 1.8,
        /* syringe_lip_length: */ 21.5,
        /* syringe_lip_width: */ 11.4,
        /* syringe_lip_width_2: */ 6.8,
        /* syringe_height: */ syringe_height_narrow,
        /* plunger_height: */ plunger_height_narrow,
        /* tower_expand */ 0.0,
    ]
][syringe_type_index];

// height from bottom of syringe to top of main injector
main_height = syringe_data[0];

main_ang = subq ? -45 : 0;
main_rot_origin = [0, 0, 0];

// how much the syringe is lifted by stopper
needle_lift = 28;

// how high the syringe must be to insert it
insert_lift = 31;

// main syringe diameter
syringe_diameter = syringe_data[1];

// upper syringe diameter (only really applies to wide syringe)
syringe_diameter_upper = syringe_data[2];

// lower syringe diameter (only applies to narrow syringe)
syringe_diameter_lower = syringe_data[3];

syringe_lip_thick = syringe_data[4];

syringe_lip_length = syringe_data[5];

syringe_lip_width = syringe_data[6];

syringe_lip_width_2 = syringe_data[7];

syringe_height = syringe_data[8];

plunger_height = syringe_data[9];

tower_expand = syringe_data[10];

tower_width = inner_width + 2 * tower_expand;

syringe_bump_ofs = .15;

// height of the base of the syringe when it is fully inserted
syringe_z1 = needle_hub_length - 2;

top_z = needle_hub_length + main_height;

grabber_y1 = -syringe_diameter/2 - 2.0;
grabber_base_thick = 2.0;

grabber_data = [
    [needle_lift + 10, 6],  // wide
    [insert_lift + 15, 0], // narrow
][syringe_type_index];

grabber_z1 = syringe_z1 + grabber_data[0];
grabber_z2 = top_z - grabber_data[1];

grabber_screw_z = [lerp(grabber_z1, grabber_z2, .5)];

grabber_slot_width = tower_width - 4;
grabber_outer_dia = syringe_diameter + 2.6;

syringe_stop_ang_1 = 26;

syringe_stop_pos = [
    0,
    -8,
    syringe_z1 + needle_lift - 12
];

syringe_stop_spring_pos = 10;
syringe_stop_lever_length = 28;

cord_x = tower_width/2 + 4;
cord_pincher_x = -23;

shipping_clip_ang = 65;
shipping_clip_yofs = -3;

main_z1 = 3;
