
include <ktpanda/consts.scad>
use <ktpanda/funcs.scad>
use <ktpanda/partbin.scad>
use <ktpanda/shapes.scad>

fit_clearance = 0.2;

tube_dia = 2.1;

cap_diameter_23g = 6.5;
cap_tip_width_23g = 4.9;

syringe_height_wide = 84.5;

syringe_diameter_wide_1 = 9.62;
syringe_diameter_wide_2 = 10.8;

syringe_diameter_narrow_1 = 6.9;
syringe_diameter_narrow_2 = 9.5;

syringe_height_narrow = 83.6;

plunger_height_wide = 96.7;
plunger_height_narrow = 94;

battery_size_9v = [26.8, 16.75, 45];
battery_terminal_spacing_9v = 12.4;

injector_base_width = 50;
injector_base_length_f = 40;
injector_base_length_b = 80;
injector_base_thick = 3;

// length of connected hub from bottom of syringe to start of needle
needle_hub_length = 16.5;

// widest point of needle hub
needle_hub_diameter = 5.75;

needle_hole_diameter = 7;

// 18g
needle_fin_width_18g = 5.63;
needle_fin_hub_diameter_18g = 3.0;
needle_fin_thickness_18g = 0.8;
needle_fin_length_18g = 4;


gearbox_w = 22.5;
gearbox_h = 18.6;

gearbox_total_h = 29.6 + 6;

gearbox_x2 = 14 - 2.4;
gearbox_x1 = gearbox_x2 - 37;
gearbox_screw_x = -20.2;

gearbox_screw_spacing = 17.6;

gearbox_tab_z1 = 9.0;
gearbox_tab_z2 = 7.4;

module preview_cutout() {
    if (!is_undef($preview_cutout) && $preview_cutout) {
        render(10)
        xslice(x1=0, x2=-100) children();
    } else {
        children();
    }
}

module syringe_text(t) {
    rz(180) text(str(t), size=2.6, font="Noto Sans", valign="center", halign="center", $fn=10);
}

module gearbox_model() {
    difference() {
        color("yellow")
        union() {
            lxt(0, gearbox_h) {
                squarex(
                    x1 = gearbox_x1, x2 = gearbox_x2,
                    yc = 0, ys = gearbox_w,
                    r2 = 2, r4 = 2

                );

                squarex(
                    x1 = gearbox_x1 - 28, x2 = gearbox_x1,
                    yc = 0, ys = gearbox_w - 6,
                );
            }


            tx(-11) cylx(gearbox_h, zs=2, d=4);

            tx(gearbox_x2) lxt(gearbox_tab_z2, gearbox_h - gearbox_tab_z1)
            difference() {
                squarex(ys=6, x2 = 5.0);
                tx(2.5) circle(d=2.8);
            }
        }

        tx(gearbox_screw_x) {
            rfy() ty(gearbox_screw_spacing/2) cylx(-eps, gearbox_h + eps, d=3.0);
        }


    }

    rfz(gearbox_h/2)
    color("#eeeeee")
    tz(gearbox_h) {
        rz(90 - (360 * motor_turn))
        linear_extrude(9) intersection() {
            circle(d=5.4);
            square([6, 3.7], center=true);
        }
    }
}

module gearbox_cutout(full_width_tab = false, leng = 60) {
    // main gearbox space
    cubex(
        xc = 0, xs = gearbox_w + .4,
        y1 = -gearbox_h, y2 = 0,
        z1 = -gearbox_x2, z2 = leng,
    );

    // space for gearbox tab
    cubex(
        xc = 0, xs = 6,
        y1 = -gearbox_tab_z1, y2 = -gearbox_h + gearbox_tab_z2,
        z1 = -gearbox_x2, zs = -5.5,
    );

    xzy() {
        lxt(0, -gearbox_h - 10.5) {
            circle(d=9);
        }
        lxt(0, 3) {
            hull() ty([0, 12]) circle(d=5);
        }
    }

}

module hinge_outer(base_size=7.5, ww=12, iw=6, xheight=10, cutofs=0) {
    xw = .6;
    difference() {
        tt = base_size;

        union() {
            yzx() {
                linear_extrude(ww, center=true, convexity=1)
                polygon([
                    [0, -xheight],
                    [0, tt/2],
                    [-tt, tt/2],
                    [-tt, -xheight+tt],
                ]);
            }

            tz(tt/2)
            tx(ww/2)
            ty(-tt/2)
            ry(-90)
            cylinder(h=ww, d=tt);
        }


        tz(tt/2)
        tx(ww/2+eps)
        ty(-tt/2)
        ry(-90) {
            cylinder(h=ww+eps2, d=screw_diameter_m2);
            hexnut_hole(h=2, d=nut_diameter_m2);

            cylx(z1=ww-1.6, z2=ww+eps2, d1=screw_diameter_m2, d2=4.8);
        }

        cubex(
            xc=0, xs=iw,
            y1=eps, ys=-tt - eps2,
            z1=-xheight+tt - cutofs, z2=tt + eps
        );
    }
}

module hinge_inner(base_size=7.5, ww=6, xlen=0, vofs1=0, vofs2=0) {
    tt = base_size;

    ty(-tt/2)
    yzx()
    {
        difference() {
            hull() {
                pts = [
                    [0, -tt/2],
                    [0, tt/2],
                    [tt/2+xlen, tt/2+vofs1],
                    [tt/2+xlen, -tt/2+vofs2]
                ];
                linear_extrude(ww, center=true)
                polygon(pts);
                cylinder(h=ww, d=tt, center=true);
            }
            cylinder(h=ww+eps2, center=true, d=screw_diameter_m2);
        }
    }
}

puller_screw_outer = [1.25, 5.7940 + .5, 4.5960 + .5];
puller_screw_inner = [1.25, 6.2940 + .5, 5.1530 + .5];

module syringe_puller_screw(brim=true) {
    z1 = 8;
    z2 = z1 + 6;

    part_color(0) {
        screw_taper(z1 - 1, puller_screw_outer, taper_bot=1, taper_top=1);

        *cylx(-4.2, 0, d=2.5);

        lxt(z1, z2) {
            square([12, 2], center=true);
        }

        tz(z1) lathe(d=[
            [-1, puller_screw_outer[2]],
            [0, puller_screw_outer[2]],
            [2.0, 2]
        ]);
    }

    part_color("brim", "yellow", flag=brim) {
        tz(z2) lxt(-.4) {
            rfx() rfy() tx(4.5) ty(1) {
                brim_tab_2d(l=1, d=7);
                *tz(.6) yzx() lxt(zc=0, zs=2) scale(1.3) polyx([[0, 0], [0, 1], [.4, 1], [1.4, 0]]);
            }
        }
    }


}

module plunger_puller_base(inner, z2=10) {
    w1 = 18;
    w2 = 22;

    xeps = inner ? eps : 0;

    fc = inner ? .2 : 0;

    z1 = -7.3;

    xzy() {
        lxt(-fc, 2 + fc) {
            difference() {
                let(x1 = w2 / 2 + fc, q = 2.5)
                rfx() polyx([
                    [0, z1 - (inner ? 2 : 0)],
                    [0, z2],
                    [x1, z2],
                    [x1, z1 + 2*q],
                    [x1 - .8, z1 + 1*q],
                    [x1, z1],
                    if (inner) [x1, z1 - 2]
                    //[x1 - .8, z1 - q],
                    //[x1 - .8, z1 - 5]
                ]);

                if (inner) {
                    squarex(xc = 0, xs = 4, y1 = -10, y2 = 0);

                }
            }
        }
        lxt(0, 5) {
            squarex(xc = 0, xs = w1 + 2*fc, y1 = -1, y2 = z2);
        }
    }

}

function ptfe_tube_data(start, d1=3) = (
    let(
        d2 = tube_dia,
        d3 = d2 + 1.6,
        path = join_paths([
            start,
            "base",
            [[let(s=d2/d1) rxmat(-2.3) * tmat(y=1, xx=s, zz=s), 1]],
            "tube_begin",
            path_straight(5),
            [[let(s=d3/d2) tmat(y=3, xx=s, zz=s), 3]],
            "tube_end",
            path_straight(1),
        ]),

        marks = path_marks(path),
    )
    [
        path,
        marks,
        getmap(marks, "base")[0],
        xform_pt(getmap(marks, "tube_begin")[0], [0, 0, 0]),
        xform_pt(getmap(marks, "tube_end")[0], [0, 0, 0]),
    ]
);

module ptfe_holder_tube_inner(path_data, d1=3, z1 = -4, z2 = 4, flip = false) {
    orig_rad = d1/2;
    rad = max(orig_rad, 1);

    rawpts = poly_ccw(arcpts2(1, 0, 180));

    pts = xzpoints(rawpts * orig_rad);
    pts2 = xzpoints([for (pt = (rawpts * rad)) [pt.x, pt.y > orig_rad ? orig_rad : pt.y < -orig_rad ? -orig_rad : pt.y]]);

    path = path_data[0];
    marks = path_data[1];
    base_index = getmap(marks, "base")[2] + 1;

    path1 = sublst(path, 0, base_index);
    path2 = sublst(path, base_index);

    // inlined path_extrude
    slices = [
        each [for (v = path1) xform_pts(v[0], pts2)],
        each [for (v = path2) xform_pts(v[0], pts)],
    ];

    vertices = flatten(slices);

    faces = extrusion_faces(len(rawpts), len(slices));

    render(5)
    rfx()
    xslice(x1 = 0, x2 = -20) {
        polyhedron(vertices, faces);
    }
    //path_extrude(pts, path_data[0]);

    //color("#00ff00", alpha=.5)
    multmatrix(path_data[2]) ry(-90) lxt(z1 - eps, z2 + eps) {
        ofs = 1.8;
        ofs1 = flip ? 0 : ofs;
        ofs2 = flip ? 1 : 0;
        rfx() polyx([
            [0, ofs2 - (flip ? ofs : 0)],
            [.2, ofs2 - (flip ? ofs : 0)],
            [2, ofs2],
            [2, ofs2 + 1],
            [.2, ofs2 + 1 + (flip ? 0 : ofs)],
            [.2, 11],
            [0, 11],
        ]);
    }
}

module plunger_puller_syringe_interface(tube_ang = 0) {
    height = 20;

    y2 = 5;
    y1 = 0;

    // y position of center of tube bend
    tube_y = tube_ang == 0 ? 2.5 : 2;
    tube_z1 = height - 4;
    tube_z2 = tube_z1 - 5;
    tube_bend_rad = 2.5;



    d1 = 1.0;
    d2 = tube_dia;
    d3 = d2 + 1;

    path_data = ptfe_tube_data(join_paths([
        path_start(tmat(z=tube_z2, y = tube_y) * rymat(90) * rzmat(-90) * rxmat(180)),
        //path_straight(-(tube_y - y2 + tube_bend_rad)),
        //path_bend(-90, tube_bend_rad),
        path_straight((tube_z1 - tube_z2 - tube_bend_rad)),

        if (tube_ang != 0) path_bend(tube_ang, tube_bend_rad),
        path_straight(tube_ang == 0 ? 1.5 : 2.5),
    ]), d1);

    path = path_data[0];
    marks = path_data[1];
    base_xform = path_data[2];
    tube_begin = path_data[3];
    tube_end = path_data[4];


    difference() {
        union() {
            intersection() {
                plunger_puller_base(false, z2=height);
                if (tube_ang == 0) {
                    xzy() lxt(zs=30) {
                        rfx() polyx([
                            [0, -20],
                            [20, -20],
                            [20, height - 21],
                            [0, height - 1]
                        ]);

                    }
                }
            }

            xmat = (
                tube_ang == 0 ? (tmat(y=tube_y, z=tube_z1 - 1)) :
                tube_ang == -90 ? (tmat(y=y2, z=tube_z1) * rxmat(-90)) :
                ident_mat()
            );

            multmatrix(xmat)
            cubex(
                ys = tube_ang == 0 ? 7.0 : 8.0,
                z2 = 10,
                xs = 8,
                r = -1,
            );
        }

        xzy() lxt(y1 - eps, y2 + eps) {
            hull() ty([0, -20]) circle(d=6);
            rfx() tx(6.8) squarex(y1 = -1, y2 = -20, xs=1.2);
        }

        ptfe_holder_tube_inner(path_data, d1, flip = (tube_ang == 0));

        rfx() tx(6.7) xzy() lxt(zs=20) squarex(x2 = 1.6, ys = d1);

        if (tube_ang != 90) {
            difference() {
                union() {
                    difference() {
                        xzy()
                        lxt(-3, y2 + 1) {
                            hull() ty([tube_z1, tube_z2]) intersection() {
                                circle(d=2);
                                squarex(y2=1, xs=d1);
                            }
                        }

                        yzx()
                        cubex(zc = 0, zs = 3, x1 = tube_y, x2 = y2 + 5, y1 = tube_z1, y2 = tube_z2 - 5, r3=3);
                    }

                    p1 = [0, tube_z2];
                    p2 = [7.2, 0];
                    p3 = intercept_x(p1, p2, tube_z2 - 4);

                    xzy() lxt(y1 - eps, tube_y + 1) rfx() {
                        linehull([p1, p2]) circle(d=d1);
                    }

                    intersection() {
                        xzy() lxt(y1 - eps, tube_y + 1) rfx() {
                            polyx([p1, [0, p3.y], p3]);

                        }

                        yzx() lxt(zs=8) {
                            polyx([
                                [tube_y + 1, p1.y],
                                [tube_y + 1, p3.y + 2.5],
                                [y1 - eps, p3.y],
                                [y1 - eps, p1.y],

                            ]);
                        }
                    }

                }

                *tz(tube_z2 - 1.6) xzy() lxt(-4, y1 + 1) squarex(xs=7, y2=-1.6);
            }

            *let(
                w1 = .35,
                w2 = 1.6,
            ) {

                ty(tube_y)
                lathe(d=[
                    [height + eps, w2],
                    [height - .6, w2],
                    [height - 1.6, w1],
                    [tube_z1, w1]
                ]);

                cubex(
                    xc = 0, xs = w1,
                    y1 = tube_y, y2 = -10,
                    z1 = height + eps, z2 = tube_z1 - eps
                );

            }

        }
    }

}

module plunger_attachment() {
    plunger_thick = 1.6;
    plunger_dia = 10.6;
    inner_w = 11.5;
    shaft_dia = 4.3;

    z1 = -12;
    z2 = 6.5;
    outer_w = 14;
    y1 = -3.2;
    y2 = 2.75;

    iz1 = z1 + 1;
    iz2 = inner_w/2;

    part_color(0) {
        difference() {
            union() {
                // main body
                cubex(xc = 0, xs = outer_w, z1 = z1, z2 = z2, y1 = y1, y2 = y2);

                // cord tabs
                rfx() tx(outer_w / 2 + 1) xzy() lxt(y1, 0) difference() {
                    hull() {
                        circle(d=5.5);
                        tx(-1) rfy() polyx([[0, 0], [eps, 0], [0, 5.6]]);
                    }
                    squarex(x1 = -1, xs=2, ys=1.2);
                }
            }

            xzy() {
                // main head space
                lxt(zs = plunger_thick)
                squarex(xs = inner_w, y1 = -plunger_dia/2 - 1, y2 = z2 + eps);

                // extra head space
                cylx(-plunger_thick/2, zs=-.6, d1 = plunger_dia, d2 = plunger_dia + .4);

                //hull() ty([0, 10]) circle(d=inner_w);

                // shaft space
                lxt(z1 = plunger_thick/2 - eps, z2 = y2 + eps) {
                    hull() ty([0, 10]) circle(d=shaft_dia);
                }

            }

            // clip space
            lxt(iz1, z2 + eps) rfx() {
                squarex(
                    x1 = shaft_dia/2 - .6, x2 = inner_w/2,
                    y1 = plunger_thick/2 - .6, y2 = y2 + eps
                );
            }

        }


        tab_thick = y2 - plunger_thick / 2;

        basepts = [
            [y2, iz1],

            each [
                for (t = closed_range(0, 1, stepd = 20))
                let(
                    z = lerp(-plunger_dia/2, plunger_dia/2, t),
                    xo = 5.4 * (t) * (t - 1),
                )
                [y2 + xo, z]
            ],
            [y2, z2],

        ];

        pts = [
            each basepts,
            each vadd([-tab_thick, 0], reverse(basepts))
        ];

        rfx() yzx() lxt(shaft_dia/2, inner_w/2 - .5) polyx(pts);
    }

    part_color("brim", "yellow", flag=$preview_brim) {
        tz(z1) lxt(.4) {
            rfy2(y2, y1) brim_tab_2d(d=12, w=6);
            rfx() tx(outer_w/2) rz(-90) brim_tab_2d(d=12, w=4);
        }
    }
}


//hinge_inner(vofs1=2, vofs2=4);
