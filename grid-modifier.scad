include <ktpanda/funcs.scad>
include <ktpanda/shapes.scad>

gridsize = 25;
height = .2;

for(yy = [0 : gridsize : 301]) {
    cubex(
        x1=0, xs=300-.2,
        y1=yy, ys=.2,
        z1=0, z2=height
    );
}

for (xx = [0 : gridsize : 301]) {
    cubex(
        x1=xx, xs=.2,
        y1=0, ys=300-.2,
        z1=0, z2=height
    );
}
