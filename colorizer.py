#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import tempfile

from pathlib import Path
from PIL import Image, ImageChops

BLACK = "#000000"
GRAY = "#888888"
WHITE = "#ffffff"

def do_render(main, acc_1, acc_2):
    tf = tempfile.NamedTemporaryFile(suffix='.png', delete=False)
    tf.close()

    try:
        osargs = [
            'openscad',
            'colorizer.scad',
            '--export-format', 'png',
            '-o', tf.name,
            '--colorscheme=ClearSky',
            '--imgsize=3000,3000',
            f'-Dmain_color="{main}"',
            f'-Daccent_color_1="{acc_1}"',
            f'-Daccent_color_2="{acc_2}"',
        ]

        subprocess.call(osargs)

        img = Image.open(tf.name)
        img.load()
        return img

    finally:
        try:
            os.unlink(tf.name)
        except OSError:
            pass

def render_diff(which):
    args = dict(main=GRAY, acc_1=GRAY, acc_2=GRAY)

    args[which] = BLACK
    img1 = do_render(**args)

    args[which] = WHITE
    img2 = do_render(**args)

    dif = ImageChops.difference(img1, img2)
    dif = dif.point(lambda v: 0 if v == 0 else 255).convert('1')
    return dif


filt = Image.BICUBIC
def mask_image(img, mask):
    newimg = Image.new("RGBA", img.size, (0, 0, 0, 0))
    newimg.paste(img, (0, 0), mask)
    return newimg

colors = {
    'black': '#333333',
    'white': '#ffffff',
    'silver': '#dddddd',
    'purple': '#553598',
    'blue': '#55bce0',
    'green': '#1ea45a',
    'red': '#c62013',
    'yellow': '#f4cb0f',
    'pink': '#ff88d8',
    'brown': '#a37e4d',
}

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('files', nargs='*', help='files')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    args = p.parse_args()

    dif_main = render_diff('main')
    dif_acc_1 = render_diff('acc_1')
    dif_acc_2 = render_diff('acc_2')

    # combined = ImageChops.logical_or(dif_main, dif_acc_1)
    # combined = ImageChops.logical_or(combined, dif_acc_2)
    # combined = ImageChops.invert(combined)

    bg = do_render(BLACK, BLACK, BLACK)
    bg.save('clr-bg.png')

    dif_main.save('diff-main.png')
    dif_acc_1.save('diff-acc1.png')
    dif_acc_2.save('diff-acc2.png')

    for cname, clr in colors.items():
        mask_image(do_render(clr, BLACK, BLACK), dif_main).save(f'clr-main-{cname}.png')
        mask_image(do_render(BLACK, clr, BLACK), dif_acc_1).save(f'clr-acc1-{cname}.png')
        mask_image(do_render(BLACK, BLACK, clr), dif_acc_2).save(f'clr-acc2-{cname}.png')


if __name__ == '__main__':
    main()
