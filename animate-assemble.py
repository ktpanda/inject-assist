include_py('animate-common.py')

def anim():
    preview_base.reset(0)
    preview_screw.reset(1)
    base_screw_turn.reset(0)

    move_view(view_under, 1)

    wait(.5)

    base_screw_tool_ofs.reset([0, 0, -20], speed=30)
    preview_screw_tool.snap(1)

    wait(base_screw_tool_ofs.move(ev = [0, 0, -.2]))

    wait(.2)

    wait(base_screw_turn.move(ev=-5.1, speed=5))

    screw_move = 15
    base_screw_ofs.move(dv=[0, 0, -screw_move], speed=30)
    wait(base_screw_tool_ofs.move(dv=[0, 0, -screw_move], speed=30))


    wait(.5)

    preview_screw_tool.snap(0)
    preview_screw.snap(0)

    move_view(view_base, .6)

    wait(.3)

    dt = .8
    view_pos.move(ev=[0, 0, 12], dt=dt)

    base_ofs.reset([0, 80, 0], speed=120)
    preview_base.snap(1)
    wait(base_ofs.move(ev=[0, 0, 0], dt=dt))

    wait(.5)

    move_view(view_under, .6)

    preview_screw_tool.snap(1)
    preview_screw.snap(1)

    wait(.5)

    base_screw_ofs.move(dv=[0, 0, screw_move], speed=30)
    wait(base_screw_tool_ofs.move(dv=[0, 0, screw_move], speed=30))

    wait(base_screw_turn.move(ev=0, speed=5))

    wait(.2)

    wait(base_screw_tool_ofs.move(ev = [0, 0, -20]))
    preview_screw_tool.snap(0)

    wait(.3)

    move_view(view_whole, 1)

anim()
wait(1)

go('animate_assemble_vars')
