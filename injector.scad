
function syringe_xform_mat(pos, ang, origin = [0, 0, top_z]) = (
    rxmat(ang, ofs=origin) *
    tmat(z=pos + syringe_z1)
);


module xform_syringe() {
    main_xform()
    multmatrix(syringe_xform_mat($syringe_pos, $syringe_ang))
    children();
}

module xform_syringe_holder() {
    xform_syringe()
    tz(syringe_height + eps)
    rz(180)
    rx(90)
    children();
}

module preview_cutout() {
    if ($preview_cutout) {
        xslice(x1=0, x2=-100) children();
    } else {
        children();
    }
}

module main_xform() {
    rx(main_ang, ofs=main_rot_origin)
    ty(lerp(0, tower_y1 - tower_y2, $base_slide))
    children();
}

$syringe_rot_origin = [0, 0, top_z];

module complete() {
    $preview_brim = false;

    //render(convexity=4)
    if ($preview_main) {
        main_xform()
        preview_cutout() {
            color($main_color)
            main();

            preview_main();

        }
    }

    if ($preview_base) {
        preview_cutout() {
            color($main_color)
            render(10)
            base();
        }
    }

    if ($preview_screw) {
        ty(screw_y)
        base_screw();
    }

    if ($preview_align_tool) {
        tz(-3) align_tool();
    }

    main_xform()
    if ($preview_grabber)
    color("#222222") render(10)
    syringe_grabber(brim=false);

    main_xform()
    if ($preview_syringe_stopper) {
        translate(syringe_stop_pos) {
            rx(-$stopper_ang) {
                ry(90) rz(90)
                color($accent_color_1)
                render(10) {
                    syringe_stop();
                }

                *tz(-syringe_stop_spring_pos)
                ty(tower_y1 - syringe_stop_pos.y - 4.5)
                xzy() color("#00ff99", 0.4)
                lxt(0, 9) difference() {
                    circle(d=6);
                    circle(d=5);
                }
            }

        }
    }

    main_xform()
    if ($preview_safety_lock) {
        color($accent_color_2)
        tx(-.2)
        render(10) preview_cutout()
        translate(syringe_stop_pos + [0, 0, 8])
        ry(180)
        translate([inner_width/2 + 1.8, -1.3, 0])
        rz(90)
        ry(90)
        safety_lock();
    }

    if ($preview_syringe_holder) {
        xform_syringe_holder()
        color($accent_color_1)
        syringe_holder();
    }

    if ($preview_syringe_puller) {
        xform_syringe_holder() {
            color($accent_color_1)
            render(10)
            syringe_puller();

            preview_syringe_puller();
        }
    }

    if ($preview_plunger_puller) {
        xform_syringe_holder() {
            color($accent_color_2)
            translate([0, 2, 0])
            render(10)
            plunger_puller_syringe_interface(tube_ang=-90);
        }
    }

    if ($preview_plunger_attachment) {
        xform_syringe_holder() {
            color($accent_color_2)
            translate([0, $plunger_pos + plunger_height - syringe_height - 2, 0])
            ry(180)
            rx(180)
            render(10)
            plunger_attachment();
        }
    }

    if ($preview_syringe) {
        xform_syringe()
        rz(90) {
            //cubex(z1=0, z2=0.1, xc=0, yc=0, xs=30, ys=30);
            tz($plunger_pos)
            if (syringe_type == "narrow") {
                plunger_model_narrow();
            } else {
                plunger_model_wide();
            }

            needle_model_23g();
            rz(180)
            if (syringe_type == "narrow") {
                syringe_model_narrow();
            } else {
                syringe_model_wide();

            }
        }
    }

}

module main_tower_outer(taper=true, flat=false, height=top_z, y1=tower_y1, y2=tower_y2, z1=main_z1) {
    xr1 = flat ? 0 : 2;
    xr2 = 2;
    n = $fn/4;

    let(
        z1 = main_z1,
        z2 = height,
        z3 = grabber_z1 - tower_expand - 3,
    )
    if (tower_expand == 0) {
        cubex(
            xs=inner_width,
            y1 = y1, y2 = y2,
            z1 = z1, z2 = z2,
            r1 = xr1, r2 = xr1, r3 = xr2, r4 = xr2, n=n
        );
    } else {
        slicepts = [for (w = [inner_width, tower_width])
            reverse(squarepts(
                xs = w,
                y1 = y1, y2 = y2,
                r1 = xr1, r2 = xr1, r3 = xr2, r4 = xr2, n=n
            ))
        ];

        slice_extrude([
            xypoints(slicepts[0], z1),
            xypoints(slicepts[0], z3),
            xypoints(slicepts[1], z3 + tower_expand),
            xypoints(slicepts[1], z2),

        ]);
    }
}

module zcircle(d) {
    $fn = 100;
    difference() { intersection() {circle(d=d); squarex(x2=d, y2=-d);} circle(d=d - .2); }
}

module main_tower_inner(height=top_z, needle_hole=true, y1=tower_y1, y2=tower_y2) {
    if (needle_hole) {
        // bottom cutout for needle hub
        cylx(z1=-eps, z2=syringe_z1 + eps2, d=needle_hole_diameter);
    }

    // rear cutout for grabber
    lxt(grabber_z1, height + eps) {
        squarex(y1=grabber_y1 - .15, y2 = grabber_y1 + grabber_base_thick + .15, xc = 0, xs = grabber_slot_width + .4);
    }

    // main cutout for grabber
    let(
        yp = grabber_y1 + grabber_base_thick,
        d1 = grabber_outer_dia + ((syringe_type == "narrow") ? .52 : 1.52),
        r1 = d1 / 2,
        xp = sqrt(r1 ^ 2 - yp ^ 2),
    )
    lxt(grabber_z1 - 1, height + eps) {
        expand_ang = (syringe_type == "narrow") ? -20 : -18;

        xf1 = rzmat(-4, ofs=[xp, yp, 0]);
        xf2 = rzmat(expand_ang, ofs=vec3(rotpt([d1/2, 0], 15))) * xf1;

        $fn = 100;
        rfx()
        intersection() {
            union() {
                circle(d=d1);

                multmatrix(xf1)
                rz(45)
                circle(d=d1);

                multmatrix(xf2)
                rz(90)
                circle(d=d1);
            }
            //squarex(y1=grabber_y1, y2 = grabber_outer_dia + 1, xc=0, xs=tower_width);
        }
    }

    // grabber screw cutouts
    tz(grabber_screw_z) {
        let(d1=screw_head_diameter_m2*1.2, d2=2.3, z1=screw_head_length_m2)
        xzy() tz(y1 + 1) lathe(d=[
            [-4, d1],
            [0, d1],
            [z1, d2],
            [y2, d2],
        ]);
    }

    translate(syringe_stop_pos)
    ry(90) rz(90) {
        syringe_stop_base(true);
        syringe_stop_safety_pin_cutout(true);
    }

    if (syringe_type == "narrow") {
        pz1 = syringe_z1;
        pz2 = syringe_z1 + insert_lift;
        pz3 = pz2 + 8;
        d1 = syringe_diameter_lower + .2;
        d2 = syringe_diameter + .5;

        pz4 = pz3 + (d1 - d2) / 2;

        // syringe space
        lathe(d=[
            [pz1, d1],
            [pz2 - 2.4, d1],
            [pz2, d1 + 1.5],
            [pz2 + .75, d1],
            [pz3 + 3, d1],
            [pz4 + 3, d2],
            [height + eps, d2]
        ]);

        // syringe insert space
        xzy() lxt(0, y2 + eps) rfx() polyx([
            [0, pz2],

            [d1/2, pz2],

            [d1/2, pz3],
            [d2/2, pz4],
            [d2/2, height + eps],
            [0, height + eps]
        ]);

        // needle hub / needle cutout, front
        cubex(
            xc=0, xs=needle_hub_diameter,
            y1=1, y2=y2 + 20,
            z1=6, z2=syringe_z1 + insert_lift + eps
        );
    } else {
        // main syringe cutout
        let(zb = grabber_z1 - 4, d1 = syringe_diameter + .5, d2 = d1 + 3.5)
        lathe(d=[
            [syringe_z1, d1],
            [zb, d1],
            [zb + 3, d2],
            [zb + 5, d2],
            [zb + 7, d1],
            [height, d1],
        ]);

        // top alignment cutout, front
        cubex(
            xc=0, xs=syringe_diameter + 2,
            y1=0, y2=y2+eps,
            z1=grabber_z1 - 1, z2=height+eps
        );

        // needle hub / needle cutout, front
        cubex(
            xc=0, xs=needle_hub_diameter,
            y1=1, y2=y2+eps,
            z1=6, z2=height+eps
        );
    }
}

module anchor() {
    rx(-90)
    difference() {
        union() {
            cylinder(h=2, center=true, d=4.5);

            for (ang = [45, 135]) rz(ang)
            cubex(
                xc=0, xs=4.5,
                y1=0, y2=5,
                zc=0, zs=2
            );
        }
        cylinder(h=2+eps, center=true, d=2.6);

    }
}

module tower_base() {
    lxt(0, injector_base_thick) difference() {
        squarex(
            xc=0, xs=injector_base_width,
            y1=-injector_base_length_f, y2=injector_base_length_b,
            r = 5
        );

        rad = min(injector_base_width / 2 - 8, 15);
        yext = 15 - rad;

        ty(35)
        hull()
        ty([-yext, yext])
        circle(r=rad);

        //ty([0, 35])
        rfx() tx(injector_base_width/2) rz(45) square(4, center=true);

        //ty(-injector_base_length_f) rz(45) square(4, center=true);
    }
}

module base_fit_test() {
    intersection() {
        base($part_color = 0);
        cubex(
            xs = inner_width + 12,
            y1 = tower_y1 - 6,
            y2 = tower_y2 + 8,
            z1 = 0,
            z2 = 10,
        );
    }
}

module main_fit_test() {
    intersection() {
        main($part_color = 0);
        cubex(
            xs = inner_width + 10,
            y1 = tower_y1 - 10,
            y2 = tower_y2 + 10,
            z1 = 0,
            z2 = 12,
        );
    }
}

module preview_base_fit_test() {
    if ($preview_main) {
        main_xform()
        color("#f080f0")
        render(10)
        main_fit_test();
    }
}

module base_interface_screw(inner=false, threads=true) {
    defn = screw_def(1.0, 5.4 + (inner ? .4 : 0), tan(50));

    d2 = threads ? defn[2] : defn[1];
    d1 = defn[2] + (inner ? 3.6 : 3.2);

    z1 = (threads && inner) ? 2.2 : 1.6;
    z2 = z1 + (d1 - d2) / 2 * .8;
    z3 = 10 + (inner ? 1 : 0);


    lathe(d=[
        [inner ? -eps : .4, d1],
        [z1, d1],
        [z2, d2],
        [z3, d2],
    ]);

    if (threads) {
        screw_thread(defn, 1.2, z3 - 1.4, utaper = 1);
    }
}

module base_screw_head(inner) {
    x1 = 3.2;
    z1 = 3;

    fc = inner ? .16 : 0;

    d = 9;
    t = 1.2 + 2*fc;

    lxt(-eps, 1.6) intersection() {
        circle(d = d + 2*fc);
        squarex(xs = t, ys = d + 1);
    }

    let(d2 = t + 1.3)
    lathe(d=[
        [-eps, d2],
        [inner ? 5.6 : 5.2, d2],
        if (inner) [6.8, .8]
    ]);

    *render(10)
    rz(half_open_range_a(0, 360, stepn=360, stepd=4))
    yzx() lxt(zs = thk) polyx([
        [0, -5],
        [0, z1],
        [.8, z1],
        [x1, .6],
        [x1, -5],
    ]);
}

module base_screw() {
    difference() {
        base_interface_screw(inner=false, threads=true);
        base_screw_head(true);
    }
}

module multi_xform(nx = 3, ny = 3, spc = [10, 8], brim_d = 3) {
    xpts = [for (p = [0 : nx - 1]) p];
    ypts = [for (p = [0 : ny - 1]) p];

    function screw_pt(xx, yy) = [spc.x * (xx + (yy % 2 == 0 ? .5 : 0)), spc.y * yy];

    pts = [
        for (xx = xpts)
        for (yy = ypts)
        screw_pt(xx, yy)
    ];

    part_color(0) {
        for (pt = pts) translate(pt)
        children(0);
    }

    let(d=3)
    tz(.4)
    part_color("brim", "yellow", flag=$preview_brim)
    lxt(.6) difference() {
        union() {
            for (xx = xpts)
            linehull([for (yy = ypts) screw_pt(xx, yy)]) circle(d=d);

            for (xx = sublst(xpts, 0, -1))
            linehull([for (yy = ypts) screw_pt(xx + (yy % 2 == 1 ? 1 : 0), yy)]) circle(d=d);

            for (yy = ypts)
            linehull([screw_pt(0, yy), screw_pt(nx - 1, yy)]) circle(d=d);
        }
        for (pt = pts) translate(pt)
        children(1);

    }

}

module base_screw_multi() {
    multi_xform(3, 3, [10, 8] * 1.2) {
        base_screw();
        circle(d=8.6);
    }

}

module base_screw_tool() {
    d = 9;
    z1 = -12;
    base_screw_head(false);

    *lxt(z1, zs = 4) squarex(xs=3, ys=20);

    cylx(z1, -.8, d=13);
    lxt(z1, .4) circle(d=d);
}

module base_screw_tool_multi() {
    multi_xform(3, 3, [10, 8] * 1.6) {
        tz(12.4)
        base_screw_tool();
        circle(d=13);
    }
}

module preview_base_screw_tool() {
    color("#b0a0ff", .5) render(10) base_screw();
}

function lerpn(a, b, n) = [
    for (i = [0 : n - 1])
    lerp(a, b, i / (n - 1))
];


module base_interface(inner) {
    xeps = inner ? eps : 0;

    fc = inner ? .2 : 0;

    leng = tower_y2 - tower_y1;

    hw = inner_width/2 + fc;

    y1 = tower_y1 + 1;
    y2 = tower_y2;


    tz(main_z1)
    difference() {

        n = ceil($fn/4);

        faces = [
            for (ofs = [-2, inner ? 2 : 1.2])
            let(
                x1=-hw - ofs, x2=hw + ofs,
                y1 = y1, y2 = y2 + ofs,
                r = 2 + ofs,
                xofs = .35
            )
            reverse(rfx([
                [x2 + xofs + (inner ? 1 : 0), y1 - xeps],
                if (inner) lerp([x2 + xofs, y1], [x2, y2 - 2], .2),
                each corner_arc_4(2, x2, y2, n),
                [0, y2 + .6],
            ]))
        ];

        slice_extrude(
            inner ? [
                each lerpn(xypoints(faces[1], 0), xypoints(faces[0], 4), 10),
                //xypoints(faces[0], 5),
            ] : [
                xypoints(faces[1], 0),
                each lerpn(xypoints(faces[1], .8), xypoints(faces[0], 4), 10),
            ]
        );

        if (!inner) {
            cubex(
                xs = inner_width - 3,
                y1 = 2, y2 = -leng,
                z1 = -eps,
                z2 = 5,
            );

        }
    }

}

module main() {
    elastic_holder_height = 14;
    elastic_holder_z = [syringe_stop_pos.z, top_z];

    difference() {
        color("#ff88d8")
        union() {
            main_tower_outer(true);
            base_interface(false);

            ty(screw_y)
            cylx(main_z1, 12, d = 10);

            // elastic holders
            let(
                w = 7,
                x1 = inner_width/2 - eps,
                x2 = cord_x  + w/2,
            )
            tz(elastic_holder_z) rfx()
            intersection() {
                lxt(-elastic_holder_height, 0) {
                    squarex(
                        x1 = x1, x2 = x2,
                        yc = 0, ys = w,
                        r2 = w/3, r4 = w/3
                    );
                }

                xzy() lxt(zc=0, zs=w)
                polyx([
                    [x2, 0],
                    [x1, 0],
                    [x1, -elastic_holder_height],
                    [x2, -elastic_holder_height + (x2 - x1) * tan(52)]
                ]);
            }
        }

        main_tower_inner();

        ty(screw_y)
        base_interface_screw(inner=true, threads=true);

        // elastic holes
        tz(elastic_holder_z) rfx() tx(cord_x) {
            lxt(-elastic_holder_height - eps, eps) hull() {
                circle(d=2.8);
                polyx([[0, 0], [0, eps], [2.2, 0]]);
            }
        }

        // rear elastic hole
        translate([0, tower_y1 + 3.5, injector_base_thick + 13]) yzx() cylx(zc=0, zs = tower_width + eps, d=2.6);
    }

    color("#ff88d8") {
        *tz(injector_base_thick){
            ty(-24) xzy() lxt(-6, 0)
            hook_interface_poly();

            ty(12) xzy() lxt(0, 6)
            hook_interface_poly();
        }
    }
}

module base() {
    difference() {
        union() {
            tower_base();

            tz(main_z1)
            let(
                hw = inner_width/2 + .1,
                b = 3.4,
                x1 = hw + 5,
                y1 = tower_y1 + 1,
                y2 = tower_y2 + 2,
            )
            intersection() {
                xzy() lxt(y1, y2 + 5) {
                    polyx(rfx([
                        [x1, 0],
                        [x1 - b, b],
                    ]));
                }
                yzx() lxt(zs = inner_width + 10) {
                    polyx([
                        [y1, 0],
                        [y1, 5],
                        [y2 - 2, 5],
                        [y2 + 3, 0],
                    ]);
                }
            }
        }

        render(10)
        base_interface(true);

        render(10) {

            cylx(-eps, injector_base_thick + eps, d=inner_width - 6);

            let(l = tower_y2 - tower_y1)
            lxt(main_z1, zs=8) {
                *squarex(
                    xs = inner_width + 4,
                    y1 = tower_y1 + 2, y2 = tower_y1 - l - 1,
                );

                squarex(
                    xs = inner_width + .4,
                    y1 = tower_y1, y2 = tower_y2,
                    r3 = 2, r4 = 2
                );


            }

            // screw holes
            rfx() tx(injector_base_width/2 - 6) {
                rfy2(-injector_base_length_f + 6, injector_base_length_b - 6) countersink_m2(-eps, injector_base_thick + eps, head_extra=.4 + eps);
            }
        }

        ty(screw_y) base_interface_screw(inner=true, threads=false);
    }

    part_color("brim", "yellow", flag=$preview_brim) {
        rfy2(injector_base_length_b, -injector_base_length_f) rfx() tx(injector_base_width/2) brim_tab_corner(d=20, w=8);
    }
}

module inlay_estradiol() {
    color("#222222") {
        ty(64)
        lxt(injector_base_thick - .32, injector_base_thick) {
            estradiol_molecule();
        }
    }
}

module brim() {
    difference() {
        tz(main_z1)
        lxt(.6) difference() {
            union() {
                ty(lerp(tower_y1, tower_y2, .5))
                rfx() tx(inner_width/2 + 1.2) rz(-90) brim_tab_2d(d=50, w=12, l=1);
                rfy2(tower_y2, tower_y1) brim_tab_2d(d=50, w=8, l=2);
            }
            ty(screw_y) circle(d=10);
        }

        base_interface(false);
    }
}

module hook_interface_poly() {
    rfx()
    scale(1.25)
    polyx([
        [0, 0],
        [0, 2],
        [2, 2],
        [1, 1],
        [1, 0]
    ]);
}

module estradiol_molecule(scl = .22, offset = .3) {
    imgw = 159;
    imgh = 97;

    scale([scl, scl])
    tx(-imgw/2)
    ty(-imgh/2)
    ty(-5) tx(-5)
    offset(offset)
    import("estradiol-molecule.svg");

}

module inlay_test() {
    z1 = 0;
    z2 = 1;
    part_color(0) {
        lxt(0, z2) {
            square([44, 28], center=true);
        }
    }

    part_color("inlay-e", "#222222", pxf=tmat(z=eps)) {
        lxt(z2, zs=.2) {
            estradiol_molecule();
        }
    }

}

module fit_test() {
    z1 = 38;
    tz(-z1)
    zslice(z1=z1, zs=24) main();
    lxt(0, 2.0) difference() {
        squarex(xc=0, xs=inner_width, y1=tower_y1, y2=tower_y2, r=-4);
        circle(d=11);
    }
}

module syringe_grabber(brim=true) {
    y1 = grabber_y1;

    xfc = .1;

    part_color(0)
    difference() {
        union() {
            xzy()
            lxt(y1 - xfc, y1 + grabber_base_thick + xfc)
            squarex(
                xc=0, xs = grabber_slot_width,
                y1 = grabber_z1, y2 = grabber_z2,
                r=-1
            );
            lxt(grabber_z1, grabber_z2) difference() {
                circle(d=grabber_outer_dia + (syringe_type == "narrow" ? .5 : 1.5));
                rfx() rz(40) tx(1) ty(2) square(10);
            }
        }

        difference() {
            cylx(syringe_z1, top_z + eps, d=syringe_diameter + .6);

            let(hh = 1.5, n = (syringe_type == "narrow") ? 3 : 5)
            for (i = [0 : n-1])
            let(
                w = .5,
                ofs=chord_dist(w, 2),
                z = lerp(grabber_z1 + 2, grabber_z2 - 4, i / (n-1)),
                angles = (i == 0 || i == n - 1) ? [-135, 0, 135] : [-135, 45, -45, 135]
            )
            tz(z)
            rz(180)
            rz(angles) {
                ty(syringe_diameter/2 - syringe_bump_ofs + w + ofs) lathe(r=[
                    [-4, 0],
                    [0, w + ofs],
                    [hh, w + ofs],
                    [hh + 4, 0]
                ]);

            }
        }

        tz(grabber_screw_z) {
            xzy() cylx(grabber_y1 - eps, grabber_y1 + 5, d=2.8);
        }
    }

    part_color("brim", "yellow", flag=$preview_brim)
    tz(grabber_z1) {
        ty(y1) rz(180) brim_tab(d=14, w=5);
        for (a = [70, -70]) rz(a) ty(syringe_diameter/2 + 1) brim_tab(d=14, w=4);
    }

}

/////////////////////////////////////////////////////
// Top
/////////////////////////////////////////////////////

module syringe_stop() {
    difference() {
        syringe_stop_base(false);
        rz(syringe_stop_ang_1 - 4)
        syringe_stop_safety_pin_cutout(false);
    }
}

module syringe_stop_safety_pin_cutout(main) {
    xofs = main ? .5 : 0;
    translate([-.5, 8]) {

        xzy()
        lxt(zc=0, zs=5.4) rfy() {
            let(t = 2 + (main ? 0 : .2), x1 = -t/2 + (main ? -.3 : 0), x2 = x1 + t)
            polyx([
                [-t/2, 0],
                [t/2 + xofs, 0],
                [t/2 + xofs, 4],
                [t/2, inner_width/2],
                [-t/2, inner_width/2],
            ]);


            *squarex([2.0, inner_width + eps], center=true);
        }

        *if (main && tower_expand > 0) {
            rfz() yzx()
            cubex(
                y1 = inner_width/2, y2 = tower_width/2 + 2,
                zs = 6,
                x1 = -8, x2 = 6.5,
                r1 = -2, r2= -2

            );
        }
    }
}

module safety_lock() {
    let(w=5 / 2, l=17.4, y1=1.5, y2=4, bl=1, r=9, h=4)
    difference() {
        union() {
            ty(-r) cylx(0, h, r=r + 2);
            lxt(0, 1.8) {
                polyx(rfx([
                    //[-w, 0],
                    [w, 0],
                    [w, l],
                    [w + .45, l + y1 - bl/2],
                    [w + .45, l + y1 + bl/2],
                    [w - 1, l + y2],
                    [.7, l + y2],
                    [.7, l / 2 + 4],
                    //[-w + 1, l],
                    //[-w, l - 2]
                ]));
            }
        }
        ty(-r) cylx(-eps, h + eps, r=r);
    }

}

module syringe_stop_base(inner) {
    center_rad = 3;
    spring_w = 4.9;
    spring_z = -syringe_stop_spring_pos;

    edge_y = tower_y1 - syringe_stop_pos.y;

    rear_x1 = edge_y - .4;
    rear_x2 = rear_x1 - 3.5;

    rear_y1 = -syringe_stop_lever_length;

    target1 = [0, -syringe_diameter_lower/2, syringe_z1 + needle_lift] - syringe_stop_pos;
    urad = norm(target1);


    target2 = intercept_cx(urad, target1.y + 4.5);
    target3 = intercept_cx(urad, edge_y - 1);


    main_w = spring_w / sqrt(2);
    main_thick = 7 + (inner ? .4 : 0);

    ang1 = -110;
    ang2 = -86;


    spring_y1 = spring_z - main_w/2;
    spring_y2 = spring_z + main_w/2;


    difference() {
        echo(target2, target3);
        lxt(zc=0, zs=main_thick)
        difference() {
            // Offset the inner cutout to add a bit of space
            offset(delta = inner ? .3 : 0)
            union() {
                rz(inner ? 0 : syringe_stop_ang_1) {

                    // main stopper
                    let(
                        a1 = atan2(target2.y, target2.x),
                        a2 = atan2(target3.y, target3.x),
                        n = 40,
                    )
                    union() {
                        circle(r=center_rad);
                        polyx([
                            [center_rad, 0],
                            each [
                                for (a = [0 : n])
                                let(
                                    t = 1 - a/n,
                                    a = lerp(a2, a1, t),
                                    c = cos(a),
                                    s = sin(a),
                                    r = (
                                        urad +
                                        lerp(0, .75, t) +
                                        lerp2c(0, .8, .54, 1, t)
                                    )
                                ) [r * c, r * s]

                            ],
                            [edge_y - 1, 0]
                        ]);
                    }

                    if (!inner) {
                        // tab to prevent stopper from going in too far
                        squarex(
                            x1 = rear_x1 - 1.6, x2 = edge_y,
                            y1 = 0, y2 = urad + 4.2
                        );
                    }


                }

                // main lever
                squarex(
                    x1 = edge_y - 1, x2 = 0,
                    y1 = -center_rad, y2 = 0
                );

                squarex(
                    x1 = rear_x2, x2 = rear_x1,
                    y1 = spring_z, y2 = urad - 4
                );

                // extra space at spring level
                squarex(
                    x1 = rear_x2 - 4, x2 = rear_x1,
                    y1 = spring_y1 - 5, y2 = spring_y2 + 5,
                    r3 = 2
                );


                squarex(
                    x1 = rear_x2 - 4, x2 = rear_x2,
                    y1 = rear_y1, ys = 12
                );
            }

            // cutouts surrounding spring
            if (!inner)
            //rfy2(spring_y2, spring_y1)
            squarex(x1 = edge_y - 4.5, x2 = edge_y - .2, y1 = spring_y1 - 1.8, y2 = spring_y2 + 1.8);

            // screw hole
            if (!inner)
            circle(d=screw_diameter_m2 + .1);
        }

        if (!inner) {
            d = 5.1;

            a = -22;

            pts1 = arcpts2(d/2, -90, a);
            pts2 = [
                each pts1,
                let(lpt=at(pts1, -1)) intercept_y(lpt, lpt + rotpt([0, 1], a), 4),
            ];

            pts3 = rotpts(uniq([
                each [for (pt = reverse(pts2)) [-pt.x, pt.y]],
                each pts2
            ]), -90);
            //!lxtd(0, 1) polyx(pts2);


            rz(syringe_stop_ang_1 - 2) tx(-syringe_stop_pos.y) {
                path_extrude(pts3, join_paths([
                    path_start(),
                    path_straight(urad - 3),
                    path_bend(-80, 5),
                    path_bend(80, 8),
                    //path_straight(4)
                ]));
                *lxt(0, urad) circle(d=5.4);
            }

            *rz(95) tx(urad + 1.4) xzy() lxt(-5, 5)
            let(w = 1, l = 4, ofs = chord_dist(w, l))
            intersection() {
                squarex(yc=0, ys=20, x1=0, x2=-w);
                tx(ofs)
                circle(r = ofs + w);
            }

        }
    }
    lxt(zc=0, zs=3 + (inner ? .4 : 0)) {

        squarex(
            x1 = rear_x2 - 1,
            x2 = edge_y + 5.8 + (inner ? .4 : 0),
            y1 = spring_y1 - (inner ? .4 : 0),
            y2 = spring_y2 + (inner ? 1.2 : 0)
        );
    }

    *lxt(-main_thick/2, 10)
    offset(delta = inner ? .3 : 0) {
        squarex(
            x1 = rear_x2 - 4, x2 = rear_x2,
            y1 = rear_y1, ys = 8
        );
    }

    if (inner) {
        rz(30)
        countersink_m2(inner_width/2 + eps, -inner_width/2 - eps, nut_length=2.5 + eps, expand=1.2);

        let(center_rad = center_rad + .4)
        render(5) let(w = main_thick/2, x2=urad + 2.0, b = 1.6) {
            yzx() lxt(rear_x1, 6)
            polyx([
                [-center_rad, -w],
                [-center_rad, w],
                [x2, w],
                [x2 + b, w - b],
                [x2 + b, -w + b],
                [x2, -w]
            ]);

            let(x3=6, x4 = -syringe_stop_pos.y - syringe_diameter_lower/2 + .9, b2 = 2) {
                ty(x2 + b)
                lxt(-w + b, w - b)
                polyx([
                    [x3, 0],
                    [x3, b2 * .75],
                    [x4, b2 * .75],
                    [x4 - b2, 0],
                ]);

                let(yo = 4.4)
                ty(-center_rad)
                lxt(-w, w)
                polyx([
                    [x3, 0],
                    [x3, -yo],
                    [x4 + 1, -yo],
                    [x4 - 1.7, 0],
                ]);


            }

        }
    }

}

module syringe_holder_base_inner(y1, y2, z1, z2) {
    xzy() lxt(y1 - eps, y2 + eps) {
        circle(d=syringe_diameter_upper + .2);
        squarex(xs = syringe_diameter_upper - 1, y1 = 0, y2 = 10);
    }

    let(lng=syringe_lip_length + .5) {
        xzy() lxt(0, -syringe_lip_thick) {
            squarex(xc=0, xs=lng, y1 = 0, y2 = 10);
            hull() {
                circle(r = syringe_lip_width / 2 + .1);
                intersection() {
                    squarex(xc=0, xs=lng, y1 = -(syringe_lip_width_2 / 2) - .1, y2 = 10);
                    *circle(r=lng/2);

                }
            }
        }

        let(x1=syringe_lip_thick/2)
        ty(-x1)
        tz(z2)
        yzx() lxt(zc=0, zs=lng) {
            polyx([
                [-x1, -3],
                [x1, -3],
                [x1 + 1.0, 0],
                [x1 + 1.0, eps],
                [-x1 - .5, eps],
                [-x1 - .5, 0],
            ]);
        }
    }
}

module syringe_holder() {
    w = 20;
    cr1 = 9;
    cr2 = 12;
    cy = 24;

    y1 = -2.7 - .8;
    y2 = 7.5 - .8;

    z1 = -7;
    z2 = 5;

    difference() {
        lxt(z1, z2) {
            rfx()
            difference() {
                union() {
                    squarex(
                        x1 = 0, x2 = w - 5,
                        y1 = y1, y2 = y2
                    );

                    r2 = (y2 - y1) / 2;
                    hull() {
                        translate([w, cy]) circle(r=cr1 + 2);
                        translate([w - (cr1 + 2) + r2, lerp(y1, y2, .5)]) circle(r = r2);
                    }
                    *squarex(xc=w, xs=2 * (cr1 + 2), y1=y1, y2=cy, r2=3);
                }
                //translate([-w, cy]) circle(r=cr1);
                translate([w, cy]) circle(r=cr1);
            }
        }

        syringe_holder_base_inner(y1, y2, z1, z2);
    }
}

cord_tube_w = 3.0;
cord_tube_h = 3.0;

module cord_tube_poly() {
    let(w=cord_tube_w, h=cord_tube_h)
    ty(h) polyx([
        [-w/2, -h], [-w/2, 0], [0, w/2], [w/2, 0], [w/2, -h]
    ]);

}

module syringe_puller() {
    w = 28;

    z1 = -8;
    z2 = 6;

    puller_y1 = -5.5;
    puller_y2 = -0.6;
    puller_y3 = 6.7;

    puller_y = 2;
    puller_z = 0;

    mpy = lerp(puller_y1, puller_y3, .5);

    clip_ext = 2.2;

    difference() {
        // outer body
        lxt(z1=z1, z2=z2) {
            rfx()
            squarex(
                x1 = 0, x2 = w,
                y1 = puller_y1, y2 = puller_y3,
                r2 = 2, r4 = 2
            );

            *squarex(
                xs = syringe_diameter_upper + 2 * clip_ext,
                y1 = puller_y1 - 2, y2 = puller_y1,
                //r2 = 2, r4 = 2
            );

        }

        rfx() tx(syringe_diameter_upper / 2 + clip_ext)
        cubex(
            x1 = 0, xs = .75,
            y1 = puller_y1, y2 = 0,
            z1 = -syringe_diameter_upper / 2 - 1, z2 = z2 + eps
        );

        // pincher tube
        xzy() {
            translate([cord_pincher_x, -2])
            ry(syringe_type == "narrow" ? 25 : 20)
            lxt(puller_y1 * 2, puller_y3 * 2) {
                cord_tube_poly();
            }
        }

        sz = 2.3 / sqrt(2);
        sz2 = .5;
        pathpts = [[0, sz], [sz, 0], [0, -sz], [-sz2, -.8], [-sz2, .8]];

        cx1 = max(cord_x, syringe_lip_length/2 + 1.2);
        cx2 = cord_x + (syringe_type == "narrow" ? 3 : 1);

        // elastic holes
        rfx()
        tx(cx1) {
            y4 = mpy + 1;
            path_extrude(pathpts, join_paths([
                path_start(tmat(y=puller_y1 - 1)),
                path_straight(puller_y2 - puller_y1 + 1),

                let(f=y4 - puller_y2)
                [[tmat(xy=(cx2 - cx1) / (puller_y3 - puller_y2)) * tmat(y=f), f]],

                // path_straight(y3 - puller_y2 + eps),
                [[tmat(y=2, xx=1.8, zz=1.8), 0]],
                path_straight(puller_y3)
            ]));
        }

        // elastic slots for flip
        rfx()
        //multmatrix(tmat(xy=(cx2 - cx1) / (puller_y3 - puller_y1)))
        xzy() lxt(puller_y1 - 1, puller_y2) {
            p1 = [cord_x, z1];
            p2 = [cx1, 0];
            polyx([
                p1 + [-sz2, -eps],
                p2 + [-sz2, 0],
                p2 + [sz, 0],
                p1 + [sz, -eps],
            ]);
        }

        // screw interface
        translate([cord_pincher_x, mpy, -2]) {
            screw_taper(z2 + 2 + eps, puller_screw_inner, taper_top=-1, taper_bot=1);
        };


        syringe_holder_base_inner(puller_y1 - 2, puller_y3, z1, z2);

        // interface to plunger puller
        translate([0, puller_y, puller_z])
        plunger_puller_base(true);
    }

}

module preview_syringe_puller() {
    if ($preview_syringe_puller_screw) {
        color($accent_color_1)
        translate([cord_pincher_x, 1.6, -2])
        turn_screw(puller_screw_inner, $screw_turn)
        syringe_puller_screw(brim=false);
    }
}

module preview_main() {
    if ($preview_inlay_estradiol) {
        inlay_estradiol();
    }
}

module preview_syringe_stop() {
}

module preview_complete() {
}

module align_tool() {
    y2 = 35 - 15 + 4/sqrt(2);
    lxt(0, 1.6) {
        difference() {
            squarex(
                xs=injector_base_width + 12,
                ys = 32,
                r = 8,
                //r3 = -2, r4 = -2,
            );

            *squarex(
                xs = injector_base_width - 10,
                y1 = y2, ys = 20,
                r1 = 3, r2 = 3
            );

            circle(r=12);

            rfx() tx(injector_base_width/2 + 1/sqrt(2)) intersection() {
                rz(45)
                square(5, center=true);
                squarex(xs = 10, ys = 2.8);
            }

            *ty([-injector_base_length_f, y2]) rz(45) square(4, center=true);
        }

    }
    lxt(0, .8) {

        let(hw=1, ofs=.8)
        rz(half_open_range_a(0, 360, stepn=360, stepd=4)) {
            polyx(rfx([
                [0, ofs],
                [hw, ofs + hw * 1.8],
                [hw, 12]
            ]));

            *rz(45)
            squarex(y1 = 5, y2 = 12, xs = 2 * hw);
        }


        let(r1 = 5)
        difference() {
            circle(r=r1 + 1);
            circle(r=r1);
        }
    }
}

module shipping_clip() {

    fc = .15;

    difference() {
        y1 = tower_y1 - 1.6;
        y2 = tower_y2 + 1.6;

        z0 = -2.4;
        z1 = 12.2;
        z2 = 11;
        z3 = 22;

        union() {
            ty(shipping_clip_yofs)
            lxt(z0, z0 + 1.6) {
                squarex(xs = 40, ys = 6);
                rfx() tx(20) squarex(x2 = -4, ys = 18, r=1);

            }
            intersection() {
                lxt(z0, z3) {
                    squarex(
                        xs = inner_width + 5,
                        y1 = y1, y2 = y2,
                        r = 1
                    );
                }
                let(x1 = 3, zz1 = 3)
                yzx() lxt(zs = inner_width + 5)
                polyx([
                    [y1, z3],
                    [y1, z2],
                    [-x1 + shipping_clip_yofs, zz1],
                    [-x1 + shipping_clip_yofs, z0],

                    [x1 + shipping_clip_yofs, z0],
                    [x1 + shipping_clip_yofs, zz1],
                    [y2, z2],
                    [y2, z3],
                ]);
            }
        }

        // syringe puller cut
        ty(shipping_clip_yofs)
        rz(-shipping_clip_ang)
        difference() {
            lxt(0, z3 + eps) {
                squarex(
                    xs = 50,
                    y1 = -7.4, y2 = 6.4,
                );
                *rz(shipping_clip_ang + 90)
                squarex(
                    xs = 50,
                    y1 = -5, y2 = 5,
                );

            }

            lxt(-eps, 5) {
                squarex(y1 = 10, y2 = -.5, xs = 17);
            }
            lxt(-eps, z2) {
                squarex(y1 = 10, y2 = .5, xs = 5);
                circle(d=6.75);
            }
        }

        lxt(z1, z3 + eps) {
            squarex(
                xs = inner_width + 2 * fc,
                y1 = tower_y1 - fc, y2 = tower_y2 + fc,
                r = 2 + fc
            );

            rz([0, 180])
            let(ys = 3.5 + fc)
            squarex(
                x2 = inner_width/2 + 5,
                y1 = -ys - 10, y2 = ys,
            );
        }
        lxt(z2, z3 + eps) {
            for (i = [0, 1])
            hull()
            for (pt = [
                [i ? cord_x : -cord_x, 0],
                rotpt([i ? cord_x : -cord_x, 0], -shipping_clip_ang) + [0, shipping_clip_yofs],
                (i ? 1 : -1) * [cord_x, -10],

            ]) translate(pt)
            circle(d=2);

        }
    }
}

module preview_shipping_clip() {
    if ($preview_syringe_puller) {
        tz(6.9)
        ty(shipping_clip_yofs)
        rz(-shipping_clip_ang)
        rx(-90)
        color("#0000ff")
        render(10)
        syringe_puller();
    }
}
