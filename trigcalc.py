#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path
from collections import namedtuple

class Term(namedtuple('Term', ['sp', 'cp'])):
    pass

def termstr_var(func, pwr):
    return func[0] + (str(pwr) if pwr > 1 else '')

def termstr_py(func, pwr):
    if pwr == 1:
        return f'{func}(t)'
    else:
        return f'{func}(t) ** {pwr}'

def termstr_scad(func, pwr):
    if pwr == 1:
        return f'{func}(t)'
    else:
        return f'{func}(t) ^ {pwr}'

class ExprFormatter:
    def __init__(self, termstr=termstr_py, multiline=False):
        self.termstr = termstr
        self.text = []
        self.multiline = multiline
        self.level = 0

    def write(self, txt):
        self.text.append(txt)

    def newterm(self):
        if self.multiline:
            self.write('\n' + '    ' * self.level)
        else:
            self.write(' ')

class Expr:
    def _tostr(self, fmt):
        pass

    def tostr(self, termstr=termstr_py, multiline = False):
        fmt = ExprFormatter(termstr, multiline)

        self._tostr(fmt)

        return ''.join(fmt.text)

    def constant_value(self):
        return None

    def simplify(self):
        return self

    def derive(self):
        return self._derive().simplify()

    def _derive(self):
        pass

    def __str__(self):
        return self.tostr()

    def __repr__(self):
        return(f'{type(self).__name__}<{self}>')

class MultiExpr(Expr):
    def __init__(self, subterms=()):
        self.subterms = list(subterms)

    def __iter__(self):
        return iter(self.subterms)

    def simplify(self):
        nterms = []
        for t in self.subterms:
            t = t.simplify()
            if t.constant_value() == self.empty_value:
                continue

            if type(t) == type(self):
                nterms.extend(t.subterms)
            else:
                nterms.append(t)

        if not nterms:
            return TrigPoly.constant(self.empty_value)

        if len(nterms) == 1:
            return nterms[0]

        self.subterms = nterms
        return self

class MulExpr(MultiExpr):
    empty_value = 1

    def _derive(self):
        terms = self.subterms
        if len(terms) > 2:
            ta = terms[0]
            tb = MulExpr(terms[1:])
        elif len(terms) == 2:
            ta = terms[0]
            tb = terms[1]
        elif len(terms) == 1:
            return terms[0]._derive()
        else:
            return TrigPoly.constant(0)

        dta = ta._derive()
        dtb = tb._derive()

        return AddExpr([MulExpr([ta, dtb]), MulExpr([tb, dtb])])

    def _tostr(self, fmt):
        terms = self.subterms

        first = True
        for term in self:
            if not first:
                fmt.write(' * ')
            first = False
            term._tostr(fmt)

    def flatten(self):
        rp = TrigPoly.constant(1)
        for t in self:
            rp = rp * t.flatten()
        return rp

class AddExpr(MultiExpr):
    empty_value = 0

    def _derive(self):
        return AddExpr([t._derive() for t in self.subterms])

    def _tostr(self, fmt):
        terms = self.subterms

        fmt.write('(')
        fmt.level += 1
        fmt.newterm()

        first = True
        for term in self:
            if not first:
                fmt.newterm()
                fmt.write('+ ')
            first = False
            term._tostr(fmt)

        fmt.level -= 1
        fmt.newterm()
        fmt.write(')')

    def flatten(self):
        rp = TrigPoly()
        for t in self:
            rp.add_terms(t.flatten())
        return rp

class TrigPoly(Expr):
    def __init__(self, coef=None):
        if coef is None:
            coef = {}
        else:
            coef = dict(coef)
        self.coef = coef

    @classmethod
    def constant(cls, val):
        '''Returns a TrigPoly representing a constant term.'''
        rp = TrigPoly()
        rp.add_term(Term(0, 0), val)
        return rp

    def constant_value(self):
        if not self.coef:
            return 0

        if len(self.coef) == 1:
            term, coef = next(iter(self.coef.items()))
            if term.sp == 0 and term.cp == 0:
                return coef

        return None

    def copy(self):
        '''Return a copy of the polynomial'''
        return TrigPoly(self.coef)

    def flatten(self):
        return self

    def add_term(self, term, coef):
        '''Adds a term (coef * sin(t) ** term.sp * cos(t) ** term.cp) to the polynomial,
        modifying it in place.'''
        ncoef = self.coef.get(term, 0) + coef
        if ncoef == 0:
            self.coef.pop(term, None)
        else:
            self.coef[term] = ncoef

    def add_terms(self, poly):
        '''Adds the terms of the given polynomial to this polynomial, modifying it in place.'''
        for term, coef in poly:
            self.add_term(term, coef)

    def __iter__(self):
        return iter(self.coef.items())

    # Operators

    def __add__(self, o):
        if isinstance(o, (int, float)):
            o = self.constant(o)

        rp = self.copy()
        rp.add_terms(o)
        return rp

    __radd__ = __add__

    def __sub__(self, o):
        return self + (-o)

    def __rsub__(self, o):
        return (-self) + o

    def __neg__(self):
        rp = TrigPoly()
        for term, coef in self:
            rp.coef[term] = -coef
        return rp

    def __mul__(self, o):
        rp = TrigPoly()
        if isinstance(o, TrigPoly):
            for aterm, acoef in self:
                for bterm, bcoef in o:
                    nterm = Term(aterm.sp + bterm.sp, aterm.cp + bterm.cp)
                    rp.add_term(nterm, acoef * bcoef)
        elif o != 0:
            for term, coef in self:
                rp.coef[term] = coef * o

        return rp

    def __pow__(self, pwr):
        if not isinstance(pwr, int):
            raise TypeError(f'TrigPoly can only take power to an integer, not {type(pwr)}')
        if pwr < 0:
            raise ValueError(f'Cannot take negative power of poly')

        cv = TrigPoly({Term(0, 0): 1})
        b = self

        if pwr == 1:
            return self

        while pwr > 0:
            if pwr & 1:
                pwr -= 1
                cv = cv * b

            pwr //= 2
            b = b * b

        return cv

    __rmul__ = __mul__

    def _derive(self):
        '''Returns the derivative of this polynomial with respect to `t`'''
        rp = TrigPoly()
        for term, coef in self:
            if term.cp == 0:
                rp.add_terms(derive_sin_pwr(term.sp) * coef)
            elif term.sp == 0:
                rp.add_terms(derive_cos_pwr(term.cp) * coef)
            else:
                sp = sin_pwr(term.sp)
                cp = cos_pwr(term.cp)
                dsp = derive_sin_pwr(term.sp)
                dcp = derive_cos_pwr(term.cp)
                rp.add_terms((sp * dcp + cp * dsp) * coef)
        return rp

    def divide_term(self, t):
        '''Divide the polynomial by the given term, and return the quotient and remainder'''
        quot = TrigPoly()
        rem = TrigPoly()

        for term, coef in self:
            nterm = Term(term.sp - t.sp, term.cp - t.cp)
            if nterm.sp >= 0 and nterm.cp >= 0:
                quot.add_term(nterm, coef)
            else:
                rem.add_term(term, coef)

        return quot, rem

    def unflatten(self, levels=100):
        '''Recursively factor out sin(t) and cos(t) and return a nested expression which is equivalent.'''
        return self._unflatten(levels).simplify()

    def _unflatten(self, levels):
        # If there are no powers of sin(t) or cos(t) greater than 1, just return self
        if levels == 0 or not any(term.cp > 1 or term.sp > 1 for term in self.coef):
            return self

        # Factor out sin(t)
        qsin, rem = self.divide_term(Term(1, 0))

        # Factor out cos(t) from the remainder
        qcos, rem = rem.divide_term(Term(0, 1))

        # Build our return expression: (rem + sin(t) * qsin + cos(t) * qcos), skipping
        # any empty terms.
        rtn = AddExpr()
        if rem.coef:
            rtn.subterms.append(rem)

        if qsin.coef:
            # Recursively unflatten the sin(t) quotient
            qsin = qsin._unflatten(levels - 1)
            rtn.subterms.append(MulExpr([sin_pwr(1), qsin]))

        if qcos.coef:
            # Recursively unflatten the cos(t) quotient
            qcos = qcos._unflatten(levels - 1)
            rtn.subterms.append(MulExpr([cos_pwr(1), qcos]))

        return rtn

    def _tostr(self, fmt):
        first = True
        terms = list(self)
        if not terms:
            fmt.write('0')
            return

        terms.sort()
        if len(terms) > 1:
            fmt.write('(')
            fmt.level += 1
            fmt.newterm()

        for term, coef in terms:
            if coef == 0:
                continue

            if not first:
                fmt.newterm()
                if coef < 0:
                    fmt.write('- ')
                    coef = -coef
                else:
                    fmt.write('+ ')

            first = False

            tt = []
            if coef != 1:
                tt.append(str(coef))

            if term.sp != 0:
                tt.append(fmt.termstr('sin', term.sp))

            if term.cp != 0:
                tt.append(fmt.termstr('cos', term.cp))

            fmt.write(' * '.join(tt) or '1')

        if len(terms) > 1:
            fmt.level -= 1
            fmt.newterm()
            fmt.write(')')

def sin_pwr(p):
    '''Returns a TrigPoly representing sin(t)**p'''
    return TrigPoly({Term(p, 0): 1})

def cos_pwr(p):
    '''Returns a TrigPoly representing cos(t)**p'''
    return TrigPoly({Term(0, p): 1})

def derive_sin_pwr(p):
    '''Returns a TrigPoly representing the derivative of sin(t)**p: p * sin(t)**(p - 1) * cos(t)'''
    return TrigPoly(None if p == 0 else {Term(p - 1, 1): p})

def derive_cos_pwr(p):
    '''Returns a TrigPoly representing the derivative of cos(t)**p: -p * cos(t)**(p - 1) * sin(t)'''
    return TrigPoly(None if p == 0 else {Term(1, p - 1): -p})

def add_ang(pa, pb):
    '''
    Calculates expressions for the sum of the angles represented as cosine / sine pairs.

    Given pa = (cos(a), sin(a)) and pb = (cos(b), sin(b)), returns (cos(a + b), sin(a + b)) as
    (cos(a) * cos(b) - sin(a) * sin(b), sin(a) * cos(b) + cos(a) * sin(b))
    '''
    ca, sa = pa
    cb, sb = pb

    sn = sa * cb + ca * sb
    cn = ca * cb - sa * sb

    return cn, sn

def main():
    p = argparse.ArgumentParser(description='')
    args = p.parse_args()

    # We have a series of segments of equal length, normalized here as a length of 1.
    # The first segment starts at [0, 0] and each subsequent segment starts at the end
    # of the previous segment, and is rotated by `t`.
    #
    # Therefore:
    #   The end of segment 1 is at [cos(t), sin(t)]
    #   The end of segment 2 is at [cos(t), sin(t)] + [cos(2t), sin(2t)]
    #   The end of segment 3 is at [cos(t), sin(t)] + [cos(2t), sin(2t)] + [cos(3t), sin(3t)]
    # etc.
    #
    # We can use the trigonometric identities:
    #   sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
    #   cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
    #
    # to derive the sin and cos of any whole multiple of t:
    #   sin(2t) = sin(t + t)
    #   sin(3t) = sin(2t + t)
    # etc.
    #
    # This expression can be folded into a polynomial of powers of sin(t) and cos(t).
    #
    # Once we have an expression for the final point, we can calculate the square distance
    # of that point from the origin, then calculate the derivative of that expression
    # so we can use Newton's method to solve for `t`.

    # Calculate expressions for [cos(t), sin(t)], [cos(2t), sin(2t)], etc
    ang1 = cos_pwr(1), sin_pwr(1)
    angles = [ang1]
    for j in range(5):
        angles.append(add_ang(angles[-1], ang1))

    # Now sum them all together
    tcos = TrigPoly()
    tsin = TrigPoly()
    for vcos, vsin in angles:
        tcos.add_terms(vcos)
        tsin.add_terms(vsin)

    # [tcos, tsin] now represents the end of the last segment. Generate an expression for
    # the square distance. We don't need to take the square root yet because we can just
    # square the target value we pass to _newton()
    dist2 = tsin * tsin + tcos * tcos

    dist2c = dist2.unflatten()

    deriv = dist2.derive().unflatten()

    # Verify that expanded expression is equivalent
    assert dist2.coef == dist2c.flatten().coef

    # Output OpenSCAD functions for the expressions
    multiline = False
    print(f'function _tube_curve_dist(s, c) = {dist2c.tostr(termstr_var, multiline)};')
    print(f'function _tube_curve_dist_d(s, c) = {deriv.tostr(termstr_var, multiline)};')

if __name__ == '__main__':
    main()
