#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

import argcomplete

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('gcode', type=Path)
    args = p.parse_args()

    delete = 'G0 X250 Y0 F10000\nM600\n'

    text = args.gcode.read_text()
    text = text.replace(delete, '', 1)
    args.gcode.write_text(text)


if __name__ == '__main__':
    main()
