include <common.scad>

/* [Common] */
part = "complete"; // [complete, main, base, syringe-stop, syringe-grabber, syringe-holder, syringe-puller, brim, inlay-estradiol, syringe-puller-screw, safety-lock, align-tool, base-screw, base-screw-tool, base-screw-multi, base-screw-tool-multi, inlay-test, base-fit-test, main-fit-test]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .25 : 140]

/* [Preview parts] */
$preview_main = true;
$preview_base = true;
$preview_syringe_stopper = true;
$preview_syringe = true;
$preview_grabber = true;
$preview_screw = false;
$preview_safety_lock = false;
$preview_syringe_holder = false;
$preview_syringe_puller = true;
$preview_syringe_puller_screw = true;
$preview_plunger_puller = false;
$preview_plunger_attachment = false;
$preview_align_tool = false;

$preview_inlay_estradiol = false;
$preview_shipping_clip = false;

$preview_cutout = false;

$base_slide = 0;  // [0 : .05 : 1]
$syringe_ang = 0; // [0 : 1 : 45]
$syringe_pos = 0; // [0 : 0.05 : 100]
$stopper_ang = 16; // [0 : .2 : 26]
$plunger_pos = 0; // [0 : 0.25 : 70]
$screw_turn = 0; // [-1 : 0.25 : 10]
$preview_brim = false;

/*
$main_color = [1.0, .51, .81];
$accent_color_1 = [.11, .11, .11];
$accent_color_2 = [.91, .91, .91];
*/

$main_color = "#ff88d8";
$accent_color_1 = "#222222";
$accent_color_2 = "#eeeeee";
/* [Parameters] */
syringe_type = "narrow"; // [wide, narrow]
subq = false;

include <models.scad>
include <injector-defs.scad>
//include <injector-tower.scad>
include <injector.scad>

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
            preview_complete();
        } else {
            complete();
        }
    }
    else if (part == "main") {
        if ($preview) {
            main();
            preview_main();
        } else {
            main();
        }
    }
    else if (part == "base") {
        if ($preview) {
            base();
        } else {
            base();
        }
    }
    else if (part == "syringe-stop") {
        if ($preview) {
            syringe_stop();
            preview_syringe_stop();
        } else {
            syringe_stop();
        }
    }
    else if (part == "syringe-grabber") {
        if ($preview) {
            syringe_grabber();
        } else {
            syringe_grabber();
        }
    }
    else if (part == "syringe-holder") {
        if ($preview) {
            syringe_holder();
        } else {
            syringe_holder();
        }
    }
    else if (part == "syringe-puller") {
        if ($preview) {
            syringe_puller();
            preview_syringe_puller();
        } else {
            syringe_puller();
        }
    }
    else if (part == "brim") {
        if ($preview) {
            brim();
        } else {
            brim();
        }
    }
    else if (part == "inlay-estradiol") {
        if ($preview) {
            inlay_estradiol();
        } else {
            inlay_estradiol();
        }
    }
    else if (part == "syringe-puller-screw") {
        if ($preview) {
            syringe_puller_screw();
        } else {
            rx(180)
            syringe_puller_screw();
        }
    }
    else if (part == "safety-lock") {
        if ($preview) {
            safety_lock();
        } else {
            safety_lock();
        }
    }
    else if (part == "align-tool") {
        if ($preview) {
            align_tool();
        } else {
            align_tool();
        }
    }
    else if (part == "base-screw") {
        if ($preview) {
            base_screw();
        } else {
            base_screw();
        }
    }
    else if (part == "base-screw-tool") {
        if ($preview) {
            base_screw_tool();
            preview_base_screw_tool();
        } else {
            base_screw_tool();
        }
    }
    else if (part == "base-screw-multi") {
        if ($preview) {
            base_screw_multi();
        } else {
            base_screw_multi();
        }
    }
    else if (part == "base-screw-tool-multi") {
        if ($preview) {
            base_screw_tool_multi();
        } else {
            base_screw_tool_multi();
        }
    }
    else if (part == "inlay-test") {
        if ($preview) {
            inlay_test();
        } else {
            inlay_test();
        }
    }
    else if (part == "base-fit-test") {
        if ($preview) {
            base_fit_test();
            preview_base_fit_test();
        } else {
            base_fit_test();
        }
    }
    else if (part == "main-fit-test") {
        if ($preview) {
            main_fit_test();
        } else {
            main_fit_test();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
