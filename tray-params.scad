
fit_clearance = 0.1;

layer_height = .2;

$tray_hinge_size = 8.1;
$tray_hinge_spacing = 60;

$tray_handle_thick = 3;
$tray_handle_size = [50, 12];
$tray_handle_rad = 5;

$tray_size = [
    5 * 18 + 20,
    98 + 80,
];

$tray_top_height = 16;
$tray_bot_height = 16;

$tray_outer_radius = 8;

$tray_pattern_layers = 2;
//$tray_pattern_thickness = layer_height * pattern_layers;

outer_extent_a = [0, 0, $tray_bot_height];
outer_extent_b = vec3($tray_size, $tray_top_height);
