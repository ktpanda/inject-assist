module needle_model_23g() {
    color("#00cc88", 0.6)
    import("syringe_model/needle-model-23g-teal.stl");

    color("#f8f8f8")
    import("syringe_model/needle-model-23g-white.stl");

    color("#bbbbbb")
    import("syringe_model/needle-model-23g-metal.stl");
}

module needle_cap_model() {
    color("#eeeeee", 0.4)
    import("syringe_model/needle-cap-model.stl");
}

module syringe_model_narrow() {
    color("#eeeeee", 0.4)
    import("syringe_model/syringe-model-narrow-clear.stl");

    color("#222222")
    import("syringe_model/syringe-model-narrow-black.stl");
}

module plunger_model_narrow() {
    color("#ffffff")
    import("syringe_model/plunger-model-narrow-white.stl");

    color("#222222")
    import("syringe_model/plunger-model-narrow-black.stl");
}

module syringe_model_wide() {
    color("#eeeeee", 0.4)
    import("syringe_model/syringe-model-wide-clear.stl");

    color("#222222")
    import("syringe_model/syringe-model-wide-black.stl");
}

module plunger_model_wide() {
    color("#ffffff")
    import("syringe_model/plunger-model-wide-white.stl");

    color("#222222")
    import("syringe_model/plunger-model-wide-black.stl");
}
