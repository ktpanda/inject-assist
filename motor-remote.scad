include <common.scad>

/* [Common] */
part = "complete"; // [complete, direction-switch, direction-switch-jig, battery-holder, base, top]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .2 : 140]

/* [Preview parts] */
preview_base = true;
preview_top = true;
preview_battery_holder = true;
preview_battery = true;
preview_pcb = true;

switch_slide = 0; // [0 : .05 : 1]

remote_debug = false;

$preview_cutout = false;
$preview_brim = true;

module __nc__() {} // end of customizer variables

/*************************************************************
* Fixed params                                              */

remote_tab = span(pc=42, ps=15);
remote_tab_w = .8;
remote_tab_h = 1.2;

remote_y1 = -4;
remote_y2 = 84;

top_thick = 7.0;
pcb_switch_size = [3.82, 8.64, 3.8];
pcb_switch_travel = 2.5;

battery_holder_wall_thick = 1.5;
battery_holder_height = 20;
battery_holder_bottom_thick = 3.2;

battery_connector_w = 25;

pcb_space_under = 3;

battery_y = 10;

//------------------------------------------------------------

/*************************************************************
* Derived params                                             */

remote_z1 = -battery_size_9v.y/2 - battery_holder_wall_thick - 2;
remote_z2 = battery_size_9v.y/2 + battery_holder_wall_thick;
remote_z3 = remote_z2 + pcb_space_under;
remote_z4 = remote_z3 + 1.6;
remote_z5 = remote_z4 + top_thick;

split_z = remote_z3 + .8;

remote_w = battery_size_9v.x + 2 * battery_holder_wall_thick + 4;

battery_holder_size = vec2(battery_size_9v) + (2 * battery_holder_wall_thick) * [1, 1];

align_posts = [for (xx = [-1, 1], yy = [-1, 74]) [xx * (remote_w/2 - 3.5), yy]];

//------------------------------------------------------------

module xform_battery_holder() {
    ty(battery_y)
    rz(180)
    rx(90)
    children();
}

module complete() {
    $preview_brim = false;

    size = battery_size_9v;

    //preview_slice("y", vc=46, vs=1)

    if (preview_base) {
        base();
    }

    //preview_slice("y", vc=46.5, vs=1)
    if (preview_top) {
        top();
    }

    if (preview_battery_holder) {
        xform_battery_holder()
        render(10) battery_holder();
    }

    if (preview_pcb) {
        tz(remote_z3) proto_pcb_model();
    }

    if (preview_battery) {
        xform_battery_holder()
        battery_model_9v();
    }
}

module battery_model_9v() {
    size = [25.35, 16.5, 45.5];
    ry(180, ofs=[0, 0, size.z/2]) {
        color("#ff5080")
        cubex(xs = size.x, ys = size.y, z2 = size.z, r = 1);

        render(10)
        color("#cccccc")

        tz(size.z) {
            tx(battery_terminal_spacing_9v/2) {
                lxt(0, .2)
                circle(d=8);
                lxt(0, 2.65)
                difference() {
                    circle(d=5.7);
                    circle(d=5.7 - 1.6);
                }
            }

            hexpts = poly_cw([for (s = [0 : 5]) rotpt([0, 1], s * 60)]);

            tx(-battery_terminal_spacing_9v/2) {
                slice_extrude([
                    xypoints(hexpts * 3.0, 0),
                    xypoints(hexpts * (8.0/2), 1.0),
                    xypoints(hexpts * (9/2), 3),
                    xypoints(hexpts * (6.4/2), 3),
                    xypoints(hexpts * (6.4/2), 1.5),
                ]);
            }
        }
    }
}

module battery_holder() {
    size = battery_size_9v;
    wt = battery_holder_wall_thick;
    bt = battery_holder_bottom_thick;
    height = battery_holder_height;
    board_y = pcb_space_under;

    difference() {
        cubex(
            xs = size.x + 2 * wt, ys = size.y + 2 * wt,
            z1 = -bt, z2 = height,
            r = 2
        );

        cubex(
            xs = size.x, ys = size.y, z2 = battery_holder_height + eps
        );

        lxt(-bt - eps, eps) {
            hull() rfx() tx(12.4 / 2) circle(d=9.8);
        }
    }

    ty(size.y/2 + wt) {
        difference() {
            lxt(-bt, height) difference() {
                squarex(
                    xc = 0, xs = 25,
                    y1 = 0, y2 = board_y + 3,
                    r3 = 2, r4 = 2
                );

                squarex(
                    xc = 0, xs = 17,
                    y1 = -eps, y2 = board_y + 3 + eps,
                );
            }

            // board space
            ty(board_y)
            let(w = 20.15, h = 1.68)
            lxt(-bt - eps, height + eps) {
                squarex(
                    xc = 0, xs = w,
                    y1 = 0, y2 = h,
                );

                rfx2(w/2, -w/2) rfy2(h, 0) rz(45) square(.5, center=true);
            }
        }
    }
}

module proto_pcb_model() {
    nx = 6;
    ny = 28;

    size = [20, 80];
    midpt = [0, size.y/2];
    basept = midpt + [(nx - 1) * -2.54 / 2, (ny - 1) * -2.54 / 2];

    *color("#00bb00", 0.4)
    lxt(0, 1.6) {

        difference() {
            squarex(xc=0, xs=size.x, y1=0, y2=size.y);

            translate(basept)
            for (xx = [0 : nx - 1], yy = [0 : ny - 1]) translate([xx, yy] * 2.54) {
                circle(d=1, $fn=10);
            }
        }
    }

    tz(1.6) {
        ty(basept.y + 2.54 * 26) lxt(0, 4) square(6, center=true);

        for (xx = [1, 3])
        translate(basept + 2.54 * [xx, 19])
        let(s=pcb_switch_size) cubex(xs = s.x, ys = s.y, z1=0, z2=s.z);

        translate(basept + 2.54 * [2, 19])
        ty(lerp(0, pcb_switch_travel, switch_slide - .5))
        rz(90)
        rx(90)
        color("#ff5ec6", .75)
        render(10)
        direction_switch();
    }
}

module base_debug() {
    if (remote_debug) {
        y1 = remote_tab[0] - .2;
        y2 = remote_tab[1] + .2;

        yslice(y1=y1, y2=y2)
        base();
        rfy2(y2, y1) cubex(
            z1 = remote_z1, z2 = remote_z2,
            y1 = 0, y2 = 3,
            xc = 0, xs = remote_w
        );
    } else {
        base();
    }
}

module align_posts(inner) {
    xeps = inner ? eps : 0;
    fc = inner ? .2 : 0;
    z1 = split_z + xeps;
    z2 = split_z - 1.2 - fc;
    z3 = z2 - 1.6 - fc;

    d1 = 3.5 + 2*fc;
    d2 = 2 + 2*fc;

    for (pt = align_posts) translate(pt) {
        lathe(d=[
            [z1, d1],
            [z2, d1],
            [z3, d2]
        ]);
    }
}

module remote_inner(zofs=0) {
    split = split_z + zofs;

    size = battery_size_9v;

    holder_y1 = battery_y - battery_holder_bottom_thick;
    holder_y2 = battery_y + battery_holder_height;

    // rear slot for battery connector
    cubex(
        xc = 0, xs = battery_connector_w,
        y1 = holder_y1 - 5, y2 = holder_y1 + eps,
        z1 = -size.y/2 - .5, z2 = split
    );

    // middle slot
    xzy()
    lxt(holder_y1 - .2, holder_y2 + .2) {
        squarex(
            xc = 0, xs = battery_holder_size.x + .4,
            y1 = -size.y/2 - 3, y2 = split,
        );
        squarex(
            xc = 0, xs = 25.4,
            y1 = -size.y/2 - 3, y2 = remote_z3 + 3.2,
            r3 = 2, r4 = 2
        );
    }

    // front slot for battery
    cubex(
        xs = size.x,
        y1 = holder_y2 - eps, y2 = battery_y + size.z + 2,
        z1 = -size.y/2 - .5, z2 = split_z + zofs
    );

    // space below board
    yzx()
    lxt(zs=18)
    squarex(x1 = 2, x2 = 78, y1 = remote_z2, y2 = remote_z3 + zofs, r2 = -2);

    // space for board itself
    lxt(remote_z3, remote_z4)
    squarex(xc=0, xs=20.4, y1=0, y2=80.2);

    // space above board itself
    lxt(remote_z4 - eps, remote_z4 + .8)
    squarex(xc=0, xs=18, y1=2, y2=78);

    // cutout for cable
    cubex(xs = 2.54 * 4, y1 = remote_y1 - eps, ys = 12, z1 = remote_z3 - 4, z2 = split_z + zofs);

}

module shell() {
    let(
        r = 2,
        slope = tan(40),
        yofs = remote_z4 + 1 - remote_z1,
        xofs = slope * yofs,

        basepts = poly_cw(squarepts(xs = remote_w, y1 = remote_y1, y2 = remote_y2, r=r)),

        btmpts = [for (pt = basepts) [pt.x, pt.y < 10 ? pt.y : pt.y - xofs]],

        slice1 = xypoints(btmpts, remote_z1),
        slice2 = xypoints(basepts, remote_z1 + yofs),
        slice3 = xypoints(basepts, remote_z5),
        slices = [slice1, slice2, slice3],
    )
    slice_extrude(slices);
}

module base() {
    size = battery_size_9v;

    y1 = remote_y1;
    y2 = remote_y2;

    *union() {

        cubex(
            xc = 0, xs = 26,
            y1 = size.z, y2 = 82,
            z1 = remote_z2, z2 = remote_z4,
            r = 1
        );
    }

    difference() {
        preview_cutout()
        render(5)
        zslice(z1 = remote_z1, z2 = split_z)
        shell();

        // tabs
        let (t = remote_tab)
        rfx() xzy()
        lxt(t[0] - .2, t[1] + .2)
        translate([remote_w/2 - 2, split_z - 2])
        polyx([
            [-4, 5],
            [0, 5],
            [0, remote_tab_h],
            [remote_tab_w, 0],
            [remote_tab_w, -.4],
            [0, -remote_tab_h - .4],
            [0, -2],
            [-4, -2],
        ]);

        remote_inner(zofs = eps);

        align_posts(true);
    }

}

module top() {
    size = battery_size_9v;

    y1 = remote_debug ? remote_tab[0] : remote_y1;
    y2 = remote_debug ? remote_tab[1] : remote_y2;

    btn_pos = [0, 71.75 + .4, remote_z4];
    switch_pos = [0, 54];
    switch_size = [15.4, 4.8 + pcb_switch_travel];


    btn_ls = [11, 11];
    btn_us = [16, 17];
    btn_r = 1;
    btn_z = 4.6;

    module switch_cut() {
        translate(switch_pos)
        tz(remote_z4 - .3)
        yzx() lxt(zs = switch_size.x) {
            offset(delta = .4)
            direction_switch_outer_2d(true);
        }
    }

    part_color(0) {
        difference() {
            preview_cutout()
            render(5)
            zslice(z1 = split_z, z2 = remote_z5)
            shell();

            // tab cutout
            xzy() lxt(remote_tab[0] - .6, remote_tab[1] + .6)

            rfx() tx(remote_w/2){
                squarex(x1 = -4.8, x2 = eps, y1 = remote_z2 - eps, y2 = remote_z5 - 1);
            }

            // direction switch
            switch_cut();

            // button cutout
            translate(btn_pos) {
                f1 = poly_cw(squarepts(xs = btn_ls.x, ys = btn_ls.y, r = btn_r, n=$fn/4));
                f2 = poly_cw(squarepts(xs = btn_us.x, ys = btn_us.y, r = btn_r, n=$fn/4));

                slice_extrude([
                    xypoints(f1, -eps),
                    xypoints(f1, top_thick - 4.0),
                    xypoints(f2, top_thick - .6),
                    xypoints(f2, top_thick + 1),
                ]);

            }

            render(10)
            remote_inner(-eps);
        }

        preview_cutout() {

            // button
            let(
                tw = 5, tt = .8,
            )
            preview_cutout()
            translate(btn_pos) {
                f1 = poly_cw(squarepts(xs = btn_ls.x - 1, ys = btn_ls.y - 1, r = btn_r, n=$fn/4));
                f2 = poly_cw(squarepts(xs = btn_us.x - 1, ys = btn_us.y - 1, r = btn_r, n=$fn/4));

                lxt(max(top_thick - tt, btn_z), top_thick)
                squarex(xs = tw, y1 = 0, y2 = btn_us.y/2);

                slice_extrude([
                    xypoints(f1, btn_z),
                    xypoints(f2, top_thick - .4),
                    xypoints(f2, top_thick)
                ]);
            }

            align_posts(false);

            // tabs
            let(
                h = 3,
                s = 1.7,
                x1 = remote_w / 2,
                x2 = x1 - 2,
                x3 = x1 - 1.0,

                x4 = x1 - 4,

                z1 = split_z - 2,
                z2 = split_z,
                z3 = remote_z5 - 1,
            )
            xzy()
            lxt(remote_tab[0], remote_tab[1])
            rfx()
            //!lxtd(0, .1)
            polyx([
                [x4, z3 - 2 - (x3 - x4) * 1.25],
                [x3, z3 - 2],
                [x3, z3],
                [x1, z3],
                [x1, z2],
                [x2, z2],
                [x2, z1 + remote_tab_h],
                [x2 + remote_tab_w, z1],
                [x2, z1 - remote_tab_h],
                [x4, z1 - remote_tab_h],
            ], txtrot=45);
        }
    }

    part_color("inlay", "#222222", pxf=tmat(z=eps))
    preview_cutout()
    render(10)
    difference() {
        tz(remote_z5)
        lxt(-.2, 0) {
            translate(btn_pos) {
                difference() {
                    squarex(xs = btn_us.x - 1, ys = btn_us.y - 1, r=btn_r);
                    squarex(xs = btn_us.x - 3.5, ys = btn_us.y - 3.5, r=btn_r - .5);
                }
                difference() {
                    circle(d=10);
                    circle(d=8);
                }
            }
            let(sz = switch_size)
            translate(switch_pos) {
                squarex(xs = sz.x + 2.5, ys = sz.y + 2.5, r=1);
            }
        }

        switch_cut();
    }
}

module direction_switch_outer_2d(cut) {
    expand = [.6, pcb_switch_travel, .3];
    size = pcb_switch_size + expand;

    lext = cut ? pcb_switch_travel : 0;

    top_y = remote_z5 - remote_z4 + .2;
    mid_w = 4;

    difference() {
        union() {
            // main body
            let(xs = size.y + 4.0 + lext, xs2 = xs + 2.5)
            hull() {
                squarex(
                    xs = xs,
                    y2 = size.z + 1.0,
                    r3 = .8,
                    r4 = .8,
                );
                polyx([[-xs2/2, 0], [0, 1], [xs2/2, 0]]);
            }

            squarex(
                xs = mid_w + lext,
                y2 = top_y,
                //r3 = w/2,
                //r4 = w/2
            );

            if (!cut)
            squarex(
                x1 = -mid_w/2, xs = mid_w + pcb_switch_travel + .6,
                y1 = top_y, ys = 2.5,
                r1 = 0, r2 = -.6,
                r4 = 1, r3 = 1,
                n = $fn/4
            );
        }

        tx(mid_w/2) ty([top_y, size.z + 1.0]) rz(45) square(.3, center=true);

    }



}

module direction_switch_inner(
    expand=[0, 0, 0], spacing = 2.54 * 2, swofs = 0, sww = 0,
    corner_cut = false, center_cut = false
) {
    size = pcb_switch_size + expand;
    rfz2(spacing / 2, -spacing / 2) {
        z1 = center_cut ? -spacing/2 - eps : -size.x/2;
        lxt(z1, size.x/2) {
            squarex(xc = 0, xs = size.y, y1 = -eps, y2 = size.z);

            tx(swofs)
            let(w = 1.58)
            squarex(xc = 0, xs = w + sww, y1 = -eps, y2 = size.z + 3.7, r3 = w/2, r4 = w/2);
        }
    }
    if (corner_cut) {
        xzy() lxt(-eps, size.z) rfy2(spacing / 2, -spacing / 2) {
            rfx() rfy() translate([size.y, size.x] / 2) squarex(x1 = -eps, y1 = 0, x2 = 1, y2 = -1);
        }
    }
}


module direction_switch() {
    expand = [.6, pcb_switch_travel, .3];
    size = pcb_switch_size + expand;


    hw = 2.54 * 2;

    z1 = 1.0 / 2;

    difference() {
        lxt(-hw - 1, hw + 1 + 2.54) {
            direction_switch_outer_2d(false);
        }

        direction_switch_inner(expand, center_cut=true);
    }
}

module direction_switch_jig() {
    ysize = 20.25;
    difference() {
        cubex(yc=0, ys=ysize, x1 = -16, x2 = 16, z1 = -8, z2 = 0);
        ty(1.27)
        rx(-90) {
            tx(-8)
            direction_switch_inner([0, 0,  0], sww = 3.5, corner_cut=true);

            tx(8)
            direction_switch_inner([.1, .1,  0], sww = 3.5, corner_cut=true);
        }
    }
    rfy() ty(ysize/2) cubex(xc=0, xs=32, y1 = 0, y2 = 2, z1=-8, z2 = 2);

}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
        } else {
            complete();
        }
    }
    else if (part == "direction-switch") {
        if ($preview) {
            direction_switch();
        } else {
            direction_switch();
        }
    }
    else if (part == "direction-switch-jig") {
        if ($preview) {
            direction_switch_jig();
        } else {
            direction_switch_jig();
        }
    }
    else if (part == "battery-holder") {
        if ($preview) {
            battery_holder();
        } else {
            battery_holder();
        }
    }
    else if (part == "base") {
        if ($preview) {
            base();
        } else {
            base();
        }
    }
    else if (part == "top") {
        if ($preview) {
            top();
        } else {
            ry(180)
            top();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
