include <common.scad>

$fn = $preview ? 30 : 50;

part = "main"; // [main, tip]
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-100 : 1 : 100]

height = 90;
inner_width = 26.4;
outer_width = 38;
shelf_height = height - 12;

module main() {
    fcr = (outer_width - inner_width)/2;
    rcr = (outer_width/2);
    cubex(
        xc=0, xs=90,
        y1=-30, y2=40,
        z1=0, z2=2,
        r=4
    );
    difference() {
        union() {
            cubex(
                xc=0, xs=outer_width,
                yc=0, ys=outer_width,
                z1=0, z2=height,
                r1=fcr, r2=fcr,
                r3=rcr, r4=rcr
            );
        }
        minkowski(convexity=4) {
            shelf_z1 = 2;
            shelf_z2 = shelf_height - 5;
            shelf_z3 = shelf_height;
            shelf_z4 = height - 6;
            shelf_z5 = height;

            shelf_d1 = inner_width;
            shelf_d2 = inner_width - 6;
            shelf_d3 = inner_width + 6;
            union() {
                cylx(shelf_z1, shelf_z2+eps, d=shelf_d1);
                cylx(shelf_z2, shelf_z3+eps, d1=shelf_d1, d2=shelf_d2);
                cylx(shelf_z3, shelf_z4+eps, d=shelf_d1);
                cylx(shelf_z4, shelf_z5+eps, d1=shelf_d1, d2=shelf_d3);
            }
            cubex(xc=0, xs=eps, y1=0, y2=-outer_width, zc=0, zs=eps);
        }
    }
}

preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "main") {
        main();
    }
    else {
        assert(false, str("invalid part ", part));
    }
}