s = source('injector', path='injector-main.scad')
s.part('complete', stl=False)
main = s.part(
    'main', default=True, stl=False,
    desc='Main injector body',
    #out_xform='ry(180)'
)

base = s.part(
    'base', desc='Injector base',
    colors=['', 'brim']
)

stop = s.part(
    'syringe-stop', stl=False, desc='Syringe release lever',
)
grabber = s.part(
    'syringe-grabber', stl=False,
    colors=['', 'brim'],
    desc='Flexible syringe alignment piece - print in TPU'
)
holder = s.part(
    'syringe-holder', stl=False,
    desc='Hand tool to assist with pushing and pulling syringe'
)
puller = s.part(
    'syringe-puller', stl=False,
    desc='Elastic syringe puller'
)

for pt in [main, stop, grabber, holder, puller]:
    pt.variant('wide', syringe_type="wide", desc_add='(BD syringe)')
    pt.variant('narrow', syringe_type="narrow", desc_add='(Easy Glide syringe)')

base.variant('nb', syringe_type="wide", desc_add=' (narrow base)', injector_base_width = 40)

p = s.part('brim', desc='Brim tabs for main injector body (to prevent warping)')
#p.variant('nb', desc='Brim tabs for main injector body (narrow base)', injector_base_width = 40)

s.part('inlay-estradiol', desc='Estradiol molecule inlay for main injector body')

s.part('syringe-puller-screw', out_xform='rx(180)', colors=['', 'brim'],
       desc='Screw to hold elastic in syringe-puller')

s.part('safety-lock', #colors=['', 'brim'],
       desc='Safety pin to prevent release lever from being pulled')

p = s.part('align-tool', desc="Alignment tool for marking injection spot")
p.variant('nb', desc_add=' (narrow base)', injector_base_width = 40)

s.part('base-screw')
s.part('base-screw-tool')
s.part('base-screw-multi', colors=['', 'brim'])
s.part('base-screw-tool-multi', colors=['', 'brim'])

s.part('inlay-test', colors=['', 'inlay-e'])
s.part('base-fit-test')
s.part('main-fit-test')

#s.part('fit-test')
s.automain()

s = source('plunger-puller', automain=True)

s.part('complete', stl=False)
s.part('screw-rod', colors=['', 'brim'])
s.part('screw-follower', colors=['', 'brim'], out_xform='ry(180)')
s.part('screw-follower-loop', out_xform='rx(-90)')
s.part('screw-follower-thread-insert-a')
s.part('screw-follower-thread-insert-b', out_xform='ry(180)')
s.part('base', colors=['', 'brim'], out_xform='rz(90)')
s.part('ptfe-adapter')
s.part('ptfe-tube-segment', colors=['', 'brim'])
s.part('ptfe-tube-segment-multi', colors=['', 'brim'])
s.part('syringe-interface', colors=['', 'brim'])
s.part('syringe-interface-bend', colors=['', 'brim'])
s.part('injector-interface', colors=['', 'brim'], out_xform='ry(180)')
s.part('plunger-attachment', colors=['', 'brim'])

s = source('needle-pulley', automain = True)

s.part('complete', stl=False)
s.part('base', colors=['', 'brim'], out_xform='rz(90)')
s.part('adapter', colors=['', 'brim'], out_xform='ry(180)')
s.part('top', colors=['', 'brim'])
s.part('motor-interface')
#s.part('pinch-test')
s.part('spacing-test')
# s = source('cap-puller')

s = source('motor-remote', automain = True)

s.part('complete', stl=False)
s.part('direction-switch')
s.part('direction-switch-jig')
s.part('battery-holder')
s.part('base')
s.part('top', colors=['', 'inlay'], out_xform='ry(180)')

s = source('hand-puller', automain = True)
s.part('complete', stl=False)
s.part('main', colors=['', 'support-volume'])
s.part('pusher', colors=['', 'brim'])
s.part('pincher-ring')
s.part('screw', colors=['', 'brim'], out_xform='ry(180)')

# p = s.part('main', capsize='23g')
# p.subpart('18g', capsize='18g')

# s.part('stopper')
# s.part('endclip')

s = source('cap-puller-articulating')
s.part('complete', stl=False)
s.part('main', out_xform='rx(-90)', capsize='23g')
s.part('stopper')
s.part('top', out_xform='rx(-90)')

s.automain()

# s = source('syringe-tray')
# s.part('complete', stl=False)
# s.part('main')
# s.part('lid', out_xform='rx(180)')
# s.part('latch')
# s.part('lidpattern', out_xform='rx(180)')
# s.part('lidpattern-top', out_xform='rx(180)')

# s.automain()

# s = source('syringe-tray-box')
# p = s.part('main', default=True)
# p.subpart('test', box_count=1, test_slice=1, box_height=60)
# p.subpart('small', box_count=1, box_height=60)

# s.part('spring')
# s.part('springx8', mod='springx', args='8')

# s.automain()

#s = source('grid-modifier')
#s.part('l1', height=0.2, gridsize=25)

#s = source('checker-modifier')
#s.part('l1', height=0.2, gridsize=25)

# s = source('fill-holder')
# s.part('main', friction_fit=0).subpart('ff', friction_fit=1)
# s.part('tip', friction_fit=0).subpart('ff', friction_fit=1)

# s = source('fill-holder-stand')

# # misc old parts
# source('test-tip')
# source('base')
# source('slide')
# source('support')
