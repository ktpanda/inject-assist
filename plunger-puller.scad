include <common.scad>

/* [Common] */
part = "complete"; // [complete, screw-rod, screw-follower, screw-follower-loop, screw-follower-thread-insert-a, screw-follower-thread-insert-b, base, ptfe-adapter, ptfe-tube-segment, ptfe-tube-segment-multi, syringe-interface, syringe-interface-bend, injector-interface, plunger-attachment]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .2 : 140]

/* [Preview parts] */
preview_orientation = "normal"; // [normal, injector]
preview_base = true;
preview_ptfe_adapter = true;
preview_screw_rod = true;
preview_follower_loop = false;
preview_follower = false;
preview_follower_insert = false;
preview_follower_screw = false;
preview_injector_interface = false;
preview_gearbox = false;
preview_injector = false;

preview_cutout = false;
remote_debug = false;

$preview_brim = true;

screw_turn = 0; // [-1 : 0.05 : 10]
motor_turn = 0; // [-4 : 0.1 : 100]
motor_shift = 0; // [0 : .5 : 10]

/* [Parameters] */

short_screw = true;

module __nc__() {} // end of customizer variables

/*************************************************************
* Fixed params                                              */

screw_rod_thread_length = 14 + 92 * (short_screw ? .5 : 1);
screw_rod_dia = 10;

// height of center of screw rod above base
screw_rod_z = 11;

base_z = -8;
base_z2 = 36;

screw_follower_size = [24, 12, 26];

base_thick = 3;

base_x1 = -16;
base_x2 = 16;
base_width = base_x2 - base_x1;


screw_pitch = short_screw ? 1.3 : 2.6;
screw_pitch_slope = tan(52);

gearbox_w = 22.5;
gearbox_h = 18.6;

gearbox_total_h = 29.6 + 6;

gearbox_x2 = 14 - 2.4;
gearbox_x1 = gearbox_x2 - 37;
gearbox_screw_x = -11.25 - 8.6;

gearbox_screw_spacing = 17.6;

battery_size_9v = [26.8, 16.75, 45];

board_mount_screw_spacing = [24, 84];

ptfe_adapter_size = [20, 12];

// remote


//------------------------------------------------------------

/*************************************************************
* Derived params                                             */

function screw_def(pitch, dia, slope=1.0) = [pitch, dia + (slope * pitch), dia];

screw_rod_outer = screw_def(screw_pitch, screw_rod_dia, screw_pitch_slope);
screw_rod_inner_a = screw_def(screw_pitch, screw_rod_dia + 0.55, screw_pitch_slope);
screw_rod_inner_b = screw_def(screw_pitch, screw_rod_dia + 0.7, screw_pitch_slope);


screw_rod_inset_a = 6;
screw_rod_inset_b = 2;

screw_rod_length = screw_rod_thread_length + screw_rod_inset_a + screw_rod_inset_b;

base_y1 = -gearbox_tab_z1;
base_y2 = screw_rod_length + 14;

screw_follower_insert_dia = screw_rod_dia + 8.6;

// height of puller cable above base
cable_zofs = sin(90 - (360/16)) * screw_follower_insert_dia / 2;

cable_z = screw_rod_z + cable_zofs;

gearbox_y2 = -gearbox_h / 2;
gearbox_y1 = base_y1;

gearbox_spacing = 5;

module preview_cutout() {
    if (preview_cutout) {
        render(10)
        xslice(x1=0, x2=-100) children();
    } else {
        children();
    }
}

//------------------------------------------------------------

module preview_xform() {
    if (preview_orientation == "injector") {
        rx(90)
        children();

    } else {
        children();

    }
}

module complete() {
    $preview_brim = false;
    preview_xform() {
        ty(-motor_shift) {
            if (preview_screw_rod) {
                tz(screw_rod_z)
                rx(-90)
                rz(-motor_turn * 360)
                screw_rod();
            }

            if (preview_gearbox) {
                tz(screw_rod_z)
                ty(-gearbox_h - eps)
                rx(-90)
                rz(90)
                gearbox_model();
            }


            ty(screw_rod_inset_a + (motor_turn) * screw_pitch) {
                if (preview_follower_loop) {
                    screw_follower_loop();
                } else if (preview_follower) {
                    screw_follower();
                }

                tz(screw_rod_z)
                rx(-90)
                if (preview_follower_insert && !preview_follower_loop) {
                    screw_follower_thread_insert_a();
                    screw_follower_thread_insert_b();
                }

                preview_screw_follower();
            }

        }

        if (preview_base) {
            base();
        }

        if (preview_ptfe_adapter) {
            ty(screw_rod_length + 2)
            ptfe_adapter();
        }

        if (preview_injector_interface) {
            injector_interface();
        }

        if (preview_injector) {
            translate([0, base_y1 - 20, base_z - 80])
            rx(-90)
            rz(180)
            color("#ff88d8")
            render(10)
            import("injector-narrow.stl");
        }
    }
}

module screw_rod_base(length, inset_a, inset_b, taper_bot=1) {
    cylx(0, length, d=screw_rod_dia);

    tz(inset_a)
    let(z2 = length - inset_a - inset_b)
    intersection() {
        screw_taper(z2, screw_rod_outer, taper_bot=taper_bot, taper_top=1);
        *cylx(0, z2, d=screw_rod_outer[1] - .5);
    }

}

module screw_rod(brim=true) {
    r2 = 2;

    part_color(0)
    difference() {
        union() {
            defn = screw_rod_outer;
            screw_rod_base(screw_rod_length - 1, screw_rod_inset_a, screw_rod_inset_b - 1, 1);


            tz(screw_rod_inset_a) lathe(d=[
                [-3.5, screw_rod_dia],
                [0, screw_rod_dia + 5],
            ]);

            tz(screw_rod_length)
            lathe(r=[
                [-1, 5],
                [1.6, 3],
                [5, 3]
            ]);
        }

        render(10)
        lxt(-eps, 9) intersection() {
            circle(d=5.6);
            square([6, 3.8], center=true);
        }

    }

    part_color("brim", "yellow", flag=$preview_brim) difference() {
        rz(half_open_range_a(0, 360, stepn=360, stepd=3)) ty(screw_rod_dia / 2 - 1) brim_tab(l=2, d=16);
        cylx(0, screw_rod_inset_a, d=screw_rod_dia);
    }

    *if ($preview) {
        color("green", .5) {
            cylx(screw_rod_inset_a, zs=screw_rod_thread_length, d=20);
        }
    }

}

module screw_rod_inner(length) {
    tz(-eps) screw_taper(length + eps2, screw_rod_inner_b, taper_bot=-1, taper_top=-1);
    cylx(-eps, length + eps, d=screw_rod_inner_b[2] + .6);

    rfy() ty(screw_rod_inner_b[1]/2 + .2) lxt(-eps, length + eps) rfx() polyx([
        [0, 0],
        [1, 0],
        [5, -7],
        [0, -7],
    ]);

}

module screw_follower_thread_insert_base(inner) {
    size = screw_follower_size;

    xeps = inner ? eps : 0;
    d1 = screw_follower_insert_dia + (inner ? .4 : 0);
    d2 = d1 + 2.4;

    pts = [
        for (ang = half_open_range_a(0, 360, stepn=360, stepd=8))
        rotpt([1, 0], ang + (360/16))
    ];

    union() {
        lxt(-xeps, size.y + xeps) scale(d1/2) polyx(pts);
        rfz2(0, size.y) lxt(-xeps, 2 + (inner ? 0 : -0.32)) scale(d2/2) polyx(pts);
    }
}

module screw_follower_thread_insert() {
    difference() {
        screw_follower_thread_insert_base(false);

        y1 = sin(90 - (360/16)) * screw_follower_insert_dia / 2;
        ty(-y1)
        lxt(-eps, screw_follower_size.y) {
            squarex(xc=0, xs=2, y1 = 0, y2 = -3);
        }

        tz(-eps)
        screw_taper(screw_follower_size.y + eps2, screw_rod_inner_a, taper_top = -1, taper_bot = -1);
    }
}

module screw_follower_thread_insert_a() {
    render(10)
    zslice(z1=-eps, z2=6.0)
    screw_follower_thread_insert();
}

module screw_follower_thread_insert_b() {
    render(10)
    zslice(z1 = 6.6, z2 = screw_follower_size.y + eps)
    screw_follower_thread_insert();
}

module screw_follower() {
    size = screw_follower_size;
    part_color(0)
    difference() {
        cubex(
            xc = 0, y1 = 0, z1 = 0,
            xs = size.x, ys = size.y, zs = size.z,
            r = -1
        );

        tz(screw_rod_z) rx(-90) {
            screw_follower_thread_insert_base(true);
        }


        let(hw = 1.5, h = 1)
        tz(cable_z) {
            xzy()
            lxt(-eps, size.y + eps)
            squarex(xc=0, xs=2, y1=0, y2=2);

            ty(size.y/2 + 1.5)
            screw_taper(size.z - cable_z + eps, puller_screw_inner, taper_bot = -1, taper_top = -1);
        }
    }

    part_color("brim", "yellow", flag=$preview_brim) difference() {
        tz(size.z) ry(180)
        rfx2(size.x/2, -size.x/2) {
            rfy2(size.y, 0) tx(-4) brim_tab(l=2, d=10);
            ty(size.y / 2) rz(-90) brim_tab(l=2, d=10);
        }
    }
}

module preview_screw_follower() {
    size = screw_follower_size;

    if (preview_follower_screw) {
        translate([0, size.y/2 + 1.5, cable_z])
        turn_screw(pulxler_screw_inner, screw_turn)
        syringe_puller_screw(brim=false);
    }
}

module screw_follower_loop() {
    size = screw_follower_size;
    difference() {
        xzy() lxt(0, size.y) {
            squarex(
                xc = 0, y1 = 0,
                xs = size.x, ys = size.z,
                r = 1
            );
            let(r = 2.75)
            squarex(
                xc = 0, xs = r * 2,
                y1 = size.z, y2 = size.z + r,
                r3 = r, r4 = r
            );
        }

        scl = [1, 1];

        base_dia = tube_dia + .2;

        pts = [
            each arcpts2(base_dia/2, 0, 180),
            [-base_dia/2, -3],
            [base_dia/2, -3],
        ];

        path = let(y2=size.y - 4) join_paths([
            path_start(tmat(z=cable_z, y=size.y + 1) * rzmat(180)),
            path_straight(y2 + 1),
            path_bend(180, (size.z - cable_z)/2, xang=90),
            path_straight(y2 + 1),
        ]);

        echo(len = at(path, -1)[1] - 6);

        intersection() {
            path_extrude(pts, path);
            xzy() lxt(-10, size.y + 1) {
                hull() ty([cable_z, size.z]) circle(d=base_dia);
            }

            let(x1 = .4, x2 = base_dia/2, y1 = size.y - 2)
            lxt(cable_z - 4, size.z + 4) polyx(rfx([
                [x2, -eps],
                [x2, y1],
                [x1, y1 + (x2 - x1)],
                [x1, size.y + eps]
            ]));
        }

        tz([cable_z, size.z])
        xzy()
        *let(
            zb = 0,
            ze = size.z,
            z1 = 9,
            z2 = z1 + 1,

            d1 = tube_dia + 1.4,
            d2 = tube_dia + .4,
            d3 = tube_dia,
            d4 = tube_dia - .6,

        )
        lathe(d=[
            [zb - eps, d3],
            //[zb, d1],
            //[zb + 2, d2],
            [z1, d3],
            [z1, d4],
            [ze + eps, d4]
        ]);

        tz(screw_rod_z)
        rx(-90)
        tz(-eps)
        screw_taper(screw_follower_size.y + eps2, screw_rod_inner_a, taper_top = -1, taper_bot = -1);
    }

}

module base() {
    battery_pos = let(sz = battery_size_9v) [base_x2 + sz.y/2, base_y2 - sz.z + 4, base_z + sz.x/2 + 3];

    part_color(0)
    difference() {
        union() {
            cubex(
                x1 = base_x1, x2 = base_x2,
                y1 = base_y1, y2 = base_y2,
                z1 = base_z, z2 = screw_follower_size.z,
                r3 = 2, r4 = 2
            );

            cubex(
                x1 = base_x1, x2 = base_x2,
                y1 = base_y1, y2 = gearbox_spacing,
                z1 = screw_follower_size.z, z2 = base_z2,
                //r1 = 2, r2 = 2
            );
        }

        render(10)
        union() {
            // main rod space
            cubex(
                xc = 0, xs = screw_follower_size.x + .5,
                y1 = gearbox_spacing, y2 = screw_rod_length + 2,
                z1 = -1.2, z2 = screw_follower_size.z + eps,
            );

            // rod start
            let(
                w = (screw_follower_size.x) / 2 - 2,
                z1 = -1.2,
                z2 = screw_follower_size.z + 3,
                z3 = base_z2,
            )
            xzy()
            lxt(-eps, gearbox_spacing + eps) {
                rfx() polyx([
                    [0, z1],
                    [w, z1],

                    [w, z2 - w],
                    [0, z2],
                ]);
            }

            // rod end
            translate([0, screw_rod_length, screw_rod_z])
            xzy()

            lxt(0, 6) hull() {
                circle(r=3.2);
                rfy()
                polyx([[0, 0], [eps, 0], [0, 4.0]]);
            }

            tz(screw_rod_z - gearbox_screw_x) {
                rfx()
                tx(gearbox_screw_spacing/2) xzy() {
                    lxt(2.4, gearbox_spacing + eps) {
                        rz(30 + 60*2)
                        hexagon(d=nut_diameter_m3);
                    }

                    cylx(-eps, 3+eps, d=screw_diameter_m3);
                }
            }

            // ptfe adapter space
            ty(screw_rod_length + 2) {
                lxt(cable_z - 4, screw_follower_size.z + eps) {
                    squarex(
                        xc = 0, xs = ptfe_adapter_size.x,
                        y1 = 0, y2 = 12,
                        r = -2
                    );
                }

                rfx()
                translate([ptfe_adapter_size.x/2, ptfe_adapter_size.y/2, screw_follower_size.z - 2])
                cubex(
                    x1 = -eps, x2 = 1.5,
                    yc = 0, ys = 6,
                    z1 = -3, z2 = 0
                );
            }

            tz(screw_rod_z)
            gearbox_cutout();

            // gearbox tab screw
            tz(screw_rod_z - gearbox_x2 - 2.5) xzy() cylx(gearbox_y2 - eps, 6, d=2.3);

            // board mount screws
            *let(spc = board_mount_screw_spacing)
            translate([base_x2, base_y1 + 8, (base_z + screw_follower_size.z) / 2]) yzx() {
                rfy() ty(spc.x / 2) tx([0, spc.y]) cylx(-6, eps, d=2.3);
            }
        }
    }

    part_color("brim", "yellow", flag=$preview_brim) {
        let(ofs = 6, base_x3 = base_x2 + battery_size_9v.y + 2, base_y3 = (base_y1 + base_y2) / 2)
        tz(base_z) {
            for (pt = [
                [base_x1, base_y1 + ofs, 90],
                [base_x2, base_y1 + ofs, -90],
                [base_x1, base_y2 - ofs, 90],

                [base_x1, base_y3, 90],
                [base_x2, base_y3, -90],

                [base_x1 + ofs, base_y1, 180],
                [base_x2 - ofs, base_y1, 180],
                [base_x1 + ofs, base_y2, 0],
                [base_x2 - ofs, base_y2, 0],

                [base_x2, base_y2 - ofs, -90],

            ]) translate([pt.x, pt.y]) rz(pt.z) {
                brim_tab(l=2, w=5, d=16);
            }
        }
        //rfx2(base_x1, base_x2) rfy2(base_y1, base_y2)
        //rz(180)
    }
}

module ptfe_adapter() {

    z1 = cable_z - 4;
    z2 = screw_follower_size.z;
    height = z2 - z1;

    pw = 16;
    pl = ptfe_adapter_size.y;

    main_w = ptfe_adapter_size.x - .3;

    bend_ang = 1;

    bevel = 2.0;

    bend_thick = 1.0;

    ptfe_d1 = 1.4;

    path_data = ptfe_tube_data(join_paths([
        path_start(tmat(z=4, y=eps) * rymat(90) * rxmat(180)),
        path_straight(pl + eps)
    ]), d1=ptfe_d1);

    y1 = -path_data[3].y;
    y2 = -path_data[4].y;

    tz(z1)
    difference() {
        union() {

            rfx() {
                lxt(0, bend_thick) {
                    squarex(
                        x1 = 0, x2 = main_w/2,
                        y1 = 0, y2 = pl,
                        r2 = -bevel, r4 = -bevel
                    );
                }

                lxt(bend_thick, height) {
                    squarex(
                        x1 = 0, x2 = pw / 2 - 1.3,
                        y1 = 0, y2 = pl,
                    );
                }

                ofs=[main_w/2, 0, bend_thick];
                mat = tmat(ofs) * tmat(xz=tan(bend_ang)) * tmat(-ofs);

                multmatrix(mat) {
                    lxt(0, height + 2)
                    intersection() {
                        squarex(
                            x1 = 0, x2 = main_w/2,
                            y1 = 0, y2 = pl,
                            r2 = -bevel, r4 = -bevel
                        );
                        squarex(
                            x1 = pw/2, x2 = main_w/2,
                            y1 = 0, y2 = pl,
                        );
                    }

                    translate([main_w / 2, pl / 2, height - 2.2])
                    xzy()
                    lxt(zc=0, zs=5) polyx([
                        [0, 0],
                        [1.1, 0],
                        [0, -2.8]
                    ]);
                }

                if (short_screw) {
                    lxt(height, zs=6) {
                        ty(pl/2)
                        squarex(xc = 0, yc = 0, xs = 10, ys = 10, r=1);

                    }

                }
            }


            tz(4)
            xzy() lxt(pl, y2)
            squarex(
                xc=0, xs=7.5,
                yc=0, ys=8,
                r=-1
            );
        }

        translate([0, pl/2, height])
        if (short_screw) {
            screw_taper(6 + eps, puller_screw_inner, taper_bot = -1, taper_top = -1);
            xzy() lxt(zs=10+eps)
            polyx([
                [-1, 0],
                [-1, 1],
                [0, 2],
                [1, 1],
                [1, 0],
            ]);
        }

        rz(180)
        ptfe_holder_tube_inner(path_data, d1=ptfe_d1);
    }

}

module syringe_interface() {
    part_color(0)
    ry(180)
    plunger_puller_syringe_interface();

    part_color("brim", "yellow", flag=$preview_brim) {

        tz(-25) ty(2.5) rfx() tx(4) rz(-90) brim_tab(d=12, w=4);
    }
}

module syringe_interface_bend() {
    part_color(0)
    ry(180)
    plunger_puller_syringe_interface(tube_ang=-90);

    part_color("brim", "yellow", flag=$preview_brim) {
        tz(-20)
        brim_tabs([
            [0, 15, 0],
            [-8, 0, 180],
            [8, 0, 180],
            [3, 11, -90],
            [-3, 11, 90],
        ], d=12, w=4);
    }
}

module gearbox_screw_xform() {
    tz(screw_rod_z - gearbox_screw_x)
    rfx() tx(gearbox_screw_spacing/2) xzy()
    children();
}

module injector_interface() {
    y1 = 0;
    y4 = -22;
    y2 = y4 + 7;

    z2 = base_z2;
    z3 = base_z - 16;

    part_color(0)
    //xslice(x1=0, x2=-100)
    difference() {
        union() {
            ty(base_y1) {
                yzx()
                lxt(base_x1, base_x2)
                polyx([
                    [y1, base_z],
                    [y1, z2],
                    [y4 + 10, z2],
                    [y4, base_z2 - 18],
                    [y4, base_z]
                ]);

                xzy() lxt(y4, y2) rfx()
                let(y1 = 10, y2 = -10, y3 = -35, x1 = (injector_base_width + 6) / 2)
                polyx([
                    [0, y1],
                    [base_x2, y1],
                    [x1, y2],
                    [x1, y3],
                    [0, y3],
                ]);

            }

            tz(z2)
            rfx() tx(gearbox_screw_spacing/2)
            ty(-gearbox_h)
            cubex(
                xs = 10,
                z1 = -15, z2 = 0,
                y1 = -7, y2 = 0
            );
        }

        // gearbox tab screw
        ty(base_y1)
        tz(screw_rod_z - gearbox_x2 - 2.5) xzy() countersink_m2(y4 - eps, head_extra = 10);

        cubex(
            xc = 0, xs = gearbox_w + .4,
            y1 = -gearbox_h, y2 = 0,
            z1 = base_z - eps, z2 = 40,
        );

        tz(screw_rod_z) xzy() {
            lxt(0, -gearbox_h - 14) {
                hull() {
                    circle(d=12);
                    *let (h=7.5)
                    polyx([[-eps, 0], [0, h], [0, -h]]);
                }
            }
        }

        let(yy = base_y1 + y4)
        gearbox_screw_xform() {
            cylx(0, yy - eps, d=screw_diameter_m3);
            *cylx(-gearbox_h - 2, yy - eps, d=6);
        }

        let(thk = 2.9, ww = injector_base_width + .4, thk2 = thk + 2.4, z1 = -35 - base_z)
        translate([0, base_y1, base_z]) {

            ty(y4 + 3.5) {
                difference() {
                    render(10) intersection() {
                        xzy()
                        lxt(zc=0, zs=thk2) {
                            squarex(xc=0, xs=ww, y1=0, y2=-100, r = 5);
                        }

                        union() {
                            lxt(0, z1) rfx() rfy() polyx([
                                [0, 0],
                                [ww/2, 0],
                                [ww/2, thk2/2],
                                [ww/2 - 3, thk2/2],
                                [ww/2 - 4.6, thk/2],
                                [0, thk/2]
                            ]);
                            yzx() lxt(zc=0, zs=ww) rfx() {

                                polyx([
                                    [0, z1 - 1],
                                    [thk2/2, z1 - 1],
                                    [thk2/2, z1],
                                    [thk/2, z1 + 4],
                                    [thk/2, -7],
                                    [thk2/2, -4],
                                    [thk2/2, 0],
                                    [0, 0],
                                ]);
                            }
                        }

                    }
                }

            }

            xzy() rfx() translate([injector_base_width / 2 - 6, -6.2, y4]) rz(30) countersink_m2(-eps, 7 + eps, head_extra=0, nut_length = 2.5);
        }
    }

    part_color("brim", "yellow", flag=$preview_brim) {

        tz(z2) sz(-1)
        brim_tabs([
            [base_x1, base_y1 - 10, 90],
            [base_x2, base_y1 - 10, -90],
            [gearbox_screw_spacing/2, -gearbox_h - 7, 180],
            [-gearbox_screw_spacing/2, -gearbox_h - 7, 180],
            [0, -gearbox_h, 0],
            [base_x1 + 2.3, base_y1, 0],
            [base_x2 - 2.3, base_y1, 0],
        ], w=4, d=12);
    }
}

module ptfe_tube_segment_base() {
    height = 140 / 6;

    outer_dia = 5;

    intersection() {
        xzy() lxt(zs=outer_dia) polyx(rfx([
            [outer_dia/2, 0],
            [outer_dia/2, height - 2],
            [0, height]
        ]));
        lxt(0, height) difference() {
            circle(d=outer_dia);
            circle(d=tube_dia + .5);
        }
    }
}

module ptfe_tube_segment() {
    part_color(0)
    ptfe_tube_segment_base();

    part_color("brim", "yellow", flag=$preview_brim) {

    }
}

module ptfe_tube_segment_multi() {
    tube_pos = [
        for (xx = [0, 1, 2], yy = [0, 1])
        [(xx + .5 * (yy % 2)) * 8, yy * 6]
    ];

    part_color(0)
    for (pt = tube_pos)
    translate(pt)
    ptfe_tube_segment_base();


    part_color("brim", "yellow", flag=$preview_brim) {
        lxt(0, .4) difference() {
            d1 = outer_dia - 3.5;
            union() {
                linehull(tube_pos) circle(d=d1);
                linehull([tube_pos[0], tube_pos[4]]) circle(d=d1);
                linehull([tube_pos[1], tube_pos[5]]) circle(d=d1);
            }

            for (pt = tube_pos)
            translate(pt)
            circle(d=5);
        }
    }
}

module preview_remote() {
}

module syringe_interface_up() {
}

module shipping_clip() {
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
        } else {
            complete();
        }
    }
    else if (part == "screw-rod") {
        if ($preview) {
            screw_rod();
        } else {
            screw_rod();
        }
    }
    else if (part == "screw-follower") {
        if ($preview) {
            screw_follower();
            preview_screw_follower();
        } else {
            ry(180)
            screw_follower();
        }
    }
    else if (part == "screw-follower-loop") {
        if ($preview) {
            screw_follower_loop();
        } else {
            rx(-90)
            screw_follower_loop();
        }
    }
    else if (part == "screw-follower-thread-insert-a") {
        if ($preview) {
            screw_follower_thread_insert_a();
        } else {
            screw_follower_thread_insert_a();
        }
    }
    else if (part == "screw-follower-thread-insert-b") {
        if ($preview) {
            screw_follower_thread_insert_b();
        } else {
            ry(180)
            screw_follower_thread_insert_b();
        }
    }
    else if (part == "base") {
        if ($preview) {
            base();
        } else {
            rz(90)
            base();
        }
    }
    else if (part == "ptfe-adapter") {
        if ($preview) {
            ptfe_adapter();
        } else {
            ptfe_adapter();
        }
    }
    else if (part == "ptfe-tube-segment") {
        if ($preview) {
            ptfe_tube_segment();
        } else {
            ptfe_tube_segment();
        }
    }
    else if (part == "ptfe-tube-segment-multi") {
        if ($preview) {
            ptfe_tube_segment_multi();
        } else {
            ptfe_tube_segment_multi();
        }
    }
    else if (part == "syringe-interface") {
        if ($preview) {
            syringe_interface();
        } else {
            syringe_interface();
        }
    }
    else if (part == "syringe-interface-bend") {
        if ($preview) {
            syringe_interface_bend();
        } else {
            syringe_interface_bend();
        }
    }
    else if (part == "injector-interface") {
        if ($preview) {
            injector_interface();
        } else {
            ry(180)
            injector_interface();
        }
    }
    else if (part == "plunger-attachment") {
        if ($preview) {
            plunger_attachment();
        } else {
            plunger_attachment();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
