highres = False
colorscheme = 'BeforeDawn'

imgsize = [1080, 1080] * 2

camera_dist = 400
camera_pos = [0, 0, 0]

# ctrl-6
camera_rot = [90, 0, 270]


view_whole = ([0, 20, 75], [0, 25, 180 + 40], 520)
view_close_front = ([0, 0, 90], [0, 30, 180 + 40], 280)
view_close_front_2 = ([0, 20, 90], [0, 30, 180 + 40], 280)
view_close_rear = ([0, -10, 90], [0, 30, 180 - 40], 280)
view_under = ([0, -10, 0], [0, -15, 180], 200)
view_base = ([0, 40, 12], [0, 15, 180 + 40], 300)

def move_view(vp, dt, do_wait=True):
    pos, ang, dist = vp
    if dt == 0:
        view_pos.snap(pos)
        view_ang.snap(ang)
        view_dist.snap(dist)
    else:
        view_pos.move(ev=pos, dt=dt)
        view_ang.move(ev=ang, dt=dt)
        view_dist.move(ev=dist, dt=dt)
        if do_wait:
            wait(view_dist)

defvar('view_pos', view_whole[0])
defvar('view_ang', view_whole[1])
defvar('view_dist', view_whole[2])

defvar('preview_main', True)
defvar('preview_base', True)
defvar('preview_syringe_stopper', True)
defvar('preview_syringe', False)
defvar('preview_needle_cap', False)
defvar('preview_grabber', True)
defvar('preview_screw', False)
defvar('preview_screw_tool', False)
defvar('preview_safety_lock', True)
defvar('preview_syringe_holder', False)
defvar('preview_syringe_puller', True)
defvar('preview_syringe_puller_screw', True)
defvar('preview_plunger_puller', False)
defvar('preview_plunger_attachment', False)
defvar('preview_ptfe_tube', False)
defvar('preview_align_tool', False)
defvar('preview_hand_puller', False)
defvar('preview_hand_puller_rod', False)
defvar('preview_hand_puller_screw', False);

defvar('puller_perspective', False)

defvar('ptfe_ang', 0)
defvar('hand_puller_ang_1', 0)
defvar('hand_puller_ang_2', 180)
defvar('hand_puller_pos', [0, 0, 0])
defvar('hand_puller_slide', 0)

defvar('attachment_ang', 0)
defvar('puller_ang', 0)
defvar('screw_turn', 0)
defvar('base_screw_turn', 0)
defvar('base_screw_ofs', [0, 0, 0])
defvar('base_screw_tool_ofs', [0, 0, 0])

defvar('base_ofs', [0, 0, 0], speed=10)
defvar('syringe_ofs', [0, 0, 0], speed=10)
defvar('needle_cap_ofs', [0, 0, 0], speed=10)
defvar('syringe_rot_origin', [0, 0, 100], speed=20)
defvar('syringe_pos', 0, speed=50)
defvar('syringe_ang', 0, speed=60)
defvar('stopper_ang', 26)
defvar('syringe_ofs_2', [0, 0, 0], speed=20)

defvar('plunger_pos', 0)

defvar('puller_alpha', 0, speed=2.0)
defvar('puller_pos', [0, 0, 0], speed=20)

defvar('attach_alpha', 0, speed=2.0)
defvar('attach_pos', [0, 0, 0], speed=20)

defvar('main_color', [1.0, .53, .847]);
defvar('accent_color_1', [.13, .13, .13]);
defvar('accent_color_2', [.93, .93, .93]);

def go(modname):
    with open(f'animate-vars.scad', 'w') as fp:
        for var in a.vars:
            fp.write(f'${var.name} = {toscad(var.keyframes[0].value)};\n')

    include('ktpanda/consts.scad')
    use('ktpanda/funcs.scad')
    use('ktpanda/partbin.scad')
    use('ktpanda/shapes.scad')

    use('animate.scad')

    scad(f'highres = {toscad(highres)};\n')

    scad(r'''
    $fn = (!highres && $preview) ? 30 : 128;
    ''')

    generate(modname = modname, xdefs = [
    ])

    scad(f'{modname}() animate_main();')
