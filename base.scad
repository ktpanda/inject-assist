include <ktpanda/funcs.scad>
include <ktpanda/partbin.scad>
include <ktpanda/shapes.scad>



$fn = 50;
fit_clearance = 0.1;

rod_spacing = 40;
rod_diameter = 8;
inner_width = 25;
main_length = 18;

base_thick = 4;

support_width = 50;
support_length = 40;

module mount_cut() {
    tx(-support_width) ty(-support_length/2)
    corners(support_width*2, support_length)
    tx(12) ty(8)
    {
        tz(-50)
        cylinder(h=100, d=screw_diameter_m3);

        tz(1)
        cylinder(h=10, d=nut_diameter_m3);

        sz(-1) tz(3)
        hexnut_hole(h=20, d=nut_diameter_m3);
    }
}

module main() {
    difference() {
        union() {
            //cylinder(h=base_thick, d=rod_spacing + 20);
            cubex(
                x1=-support_width, x2=support_width,
                yc=0, ys=support_length,
                z1=0, z2=base_thick, r=3
            );
            for (xx = [-1, 1]) {
                sx(xx)
                tx(rod_spacing/2)
                tz(base_thick) {
                    cylinder(h=18-base_thick, d=12);
                    cylinder(h=3, d1=15, d2=12);
                }

            }

        }
        //tz(-eps)
        //cylinder(h=4+eps2, d=rod_spacing - 12);

        cubex(
            xc=0, xs=inner_width+3,
            yc=0, ys=inner_width+3,
            z1=-eps, z2=30
        );

        for (xx = [-1, 1]) {
            sx(xx)
            tx(rod_spacing/2)
            {
                tz(-eps) {
                    cylinder(h=150, d=rod_diameter);
                    cubex(
                        x1=-13, x2=0,
                        yc=0, ys=1,
                        z1=0, z2=20
                    );
                }
                tz(17)
                cylinder(h=1+eps, d1=rod_diameter, d2=rod_diameter+2);
            }
            *sx(xx) {
                ww=26;
                cubex(
                    x1=support_width-3, x2=support_width-9,
                    yc=0, ys=ww,
                    z1=3-eps, z2=base_thick+eps
                );
                cubex(
                    x2=support_width+eps, x1=support_width-9,
                    yc=0, ys=35,
                    z1=-eps, z2=3
                );
                *tz(3.5)
                tx(support_width - 6)
                rx(-90)
                tz(-ww/2)
                cylinder(h=ww, r=2.5);

            }
        }

        mount_cut();
    }

}

module handle() {
    difference() {
        cubex(
            x1=0, x2=10,
            yc=0, ys=support_length - 6,
            z1=-3, z2=0, r2=3, r4=3
        );

        cubex(
            x1=-eps, x2=7,
            yc=0, ys=support_length - 12,
            z1=-3-eps, z2=eps
        );
    }
}

module support() {
    leg_circ = 740;
    leg_rad = leg_circ/(PI*2);

    difference() {
        cubex(
            x1=-support_width, x2=support_width,
            yc=0, ys=support_length,
            z1=-20, z2=0, r=3
        );
        cubex(
            xc=0, xs=inner_width+3,
            yc=0, ys=inner_width+3,
            z1=-20, z2=30
        );


        tz(-leg_rad-2)
        ty(-support_length/2-eps)
        rx(-90)
        cylinder(h=support_length+eps2, r=leg_rad, $fn=200);

        mount_cut();
    }

    for (xx = [-1, 1]) {
        sx(xx)
        tz(-8)
        tx(support_width)
        handle();

    }
}

//yslice(yc=0, ys=1, max=400)
main();
if ($preview)
support();
