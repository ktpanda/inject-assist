include <common.scad>

/* [Common] */
part = "complete"; // [complete, base, adapter, top, motor-interface, spacing-test]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .2 : 140]

/* [Preview parts] */
preview_base = true;
preview_motor_interface = true;
preview_gearbox = true;
preview_top = true;
preview_adapter = false;
preview_needle = false;
$preview_brim = true;

preview_cutout = false;
motor_turn = 0; // [-4 : 0.1 : 100]

/* [Parameters] */

module __nc__() {} // end of customizer variables

/*************************************************************
* Fixed params                                              */

true_needle_dia = 1.22;
needle_dia = 2.0;

inner_dia = 12;
outer_dia = 15;
top_height = 15;

needle_length = 19.8;

adapter_length = 9 + 6.5 + 2;

ptfe_d1 = 1.4;

//------------------------------------------------------------

/*************************************************************
* Derived params                                             */

top_z = adapter_length + needle_length - top_height + 2.5;

adapter_dia = inner_dia - 2;

//------------------------------------------------------------

module needle_model() {
    color("#cccccc")
    cylx(0, needle_length, d=true_needle_dia);
}

module preview_cutout() {
    if (preview_cutout) {
        render(10)
        xslice(x1=0, x2=-100) children();
    } else {
        children();
    }
}

module complete() {
    $preview_brim = false;

    if (preview_base) {
        tz(-gearbox_h - 2)
        base();
    }

    if (preview_motor_interface) {
        motor_interface();
    }

    if (preview_top) {
        tz(top_z)
        top();
    }

    if (preview_adapter) {
        rz(-motor_turn * 360)
        adapter();
    }

    if (preview_needle) {
        tz(adapter_length - 1)
        needle_model();
    }

    if (preview_gearbox) {
        tz(-gearbox_h - 2)
        rz(90)
        gearbox_model();
    }
}

module adapter() {
    part_color(0)
    difference() {
        preview_cutout()
        lxt(0, adapter_length)
        circle(d=adapter_dia);


        lxt(-eps, 9) intersection() {
            circle(d=5.6);
            square([6, 3.8], center=true);
        }

        lxt(-eps, adapter_length - 1.4) {
            rz([45, -45]) square([5.0, 1.1], center=true);
        }

        let(
            d1 = 3.2,
            d2 = true_needle_dia + .2,
            d3 = true_needle_dia + 1,
            z1 = -eps,
            z2 = adapter_length - 1.4,
            z4 = adapter_length - .6,
            z5 = adapter_length
        )
        lathe(d=[
            [z1, d1],
            [z2, d1],
            [z2, d2],
            [z4, d2],
            [z5, d3],
            [z5 + eps, d3]
        ]);

        rfy()
        render(10)
        color("#00ffcc")
        let(
            w1 = .35,
            w2 = 1.6,

            y1 = 3.2,
            y2 = adapter_dia/2,
            ym = y2 - .8,

            zb = 0,
            z1 = 6,
            z2 = z1 + 2,
            ze = adapter_length
        ) {
            ty(y1)
            difference() {
                lathe(d=[
                    [zb - eps, w2],
                    [z1, w2],
                    [z2, w1],
                    [ze + eps, w1]
                ]);
                cubex(
                    xc = 0, xs = w2,
                    y1 = -.2, y2 = -w2,
                    z1 = zb - eps, z2 = ze + eps
                );
            }

            xzy() lxt(y1, y2) rfx() polyx([
                [0, zb - eps],
                [w1/2, zb - eps],
                [w1/2, ze - 3],
                [w2/2, ze - 1],
                [w2/2, ze + eps],
                [0, ze + eps]
            ]);
            *cubex(
                xc = 0, xs = w1,
                y1 = y1, y2 = y2,
                z1 = zb - eps, z2 = ze + eps
            );
        }


        *xzy() {
            for (i = [0, 1])
            let(ofs = i ? 0 : 3)
            lxt(i ? ym - eps : y1, i ? y2 : ym) rfx() polyx([
                [0, zb - eps],
                [w2/2, zb - eps],
                [w2/2, z1 + ofs],
                [w1/2, z2 + ofs],
                [w1/2, ze - 2],
                [w2/2, ze],
                [w2/2, ze + eps],
                [0, ze + eps],

            ]);
        }


    }

    part_color("brim", "yellow", flag=$preview_brim)
    tz(adapter_length) rx(180) render(10) difference() {
        rz(45)
        rz(half_open_range_a(0, 360, stepn=360, stepd=4)) ty(adapter_dia / 2 - 1) brim_tab(l=2, d=10);
        cylx(-1, 4, d=adapter_dia);
    }

}

module motor_interface() {
    total_height = top_z + top_height - 2;
    odia = outer_dia + 4;
    idia = outer_dia + .3;

    y1 = gearbox_x1;
    y2 = gearbox_x2 + 10;

    cut_z = top_z - 6;

    difference() {
        preview_cutout()
        union() {
            cylx(-2, total_height, d=odia);
            lxt(-2, 2) {
                squarex(xc=0, xs=gearbox_w + 10, y1 = y1, y2 = y2, r=2);
            }
        }

        let(
            z1 = -2 - eps,
            z2 = top_z,
            z3 = total_height + eps,
            d2 = idia,
        )
        lathe(d=[
            [z1, inner_dia + 2],
            [adapter_length, inner_dia + 2],
            [adapter_length + 1, inner_dia],
            [z2, inner_dia],
            [z2, d2],
            [z3, d2]
        ]);

        // top ptfe tube cutout
        xzy()
        cubex(
            xc=0, xs=7.8,
            z1 = 0, z2 = 20,
            y1 = top_z, y2 = top_z + top_height,
            r = -.8
        );

        lxt(cut_z, total_height + eps) square([outer_dia + 5, 5], center=true);

        ty([y2 - 5, gearbox_screw_x]) rfx() tx(gearbox_screw_spacing/2) {
            cylx(-2 - eps, 2 + eps, d = screw_diameter_m3);
            lxt(0, 2 + eps) hexagon(nut_diameter_m3);
        }
    }

    render(10)
    intersection() {
        lxt(cut_z, total_height) square([odia + 1, 4], center=true);
        let(
            r1 = odia / 2,
            r2 = idia / 2,
            r3 = r2 - .6,

            zb = cut_z,
            ze = total_height,
            z3 = ze - 4,
            z2 = z3 - 1,
            z1 = z2 - 1,
        )
        rotate_extrude()
        polyx([
            [r1, zb],
            [r1, ze],
            [r2, ze],
            [r3, z3],
            [r3, z2],
            [r2, z1],
            [r2, zb],

        ]);


    }
}

module pinch_test() {
    lxt(0, 5) difference() {
        square(16, center=true);
        polyx([
            [0, -5],
            [-.5, 8.5],
            [.5, 8.5],
        ]);
    }
}

module top() {

    path_data = ptfe_tube_data(join_paths([
        path_start(tmat(z=4, y=eps) * rymat(90) * rxmat(180)),
        path_straight(7)
    ]), d1=ptfe_d1);

    y1 = -path_data[3].y;
    y2 = -path_data[4].y;

    part_color(0)
    difference() {
        color("#00ccff")
        preview_cutout()
        union() {
            cylx(0, top_height, d=outer_dia);

            tz(4)
            xzy() lxt(0, y2)
            squarex(
                xc=0, xs=7.5,
                yc=0, ys=8,
                r=-1
            );
        }

        rz(180)
        ptfe_holder_tube_inner(path_data, d1=ptfe_d1);

        let(
            z1 = -eps,
            z2 = top_height - 10,
            z3 = z2 + (inner_dia - needle_dia) / 2,
            z4 = top_height - 1
        )
        lathe(d=[
            [z1, inner_dia],
            [z2, inner_dia],
            [z3, needle_dia],
            [z4, needle_dia],
        ]);
    }

    part_color("brim", "yellow", flag=$preview_brim)
    render(10) difference() {
        rz(half_open_range_a(45, 360+45, stepn=360, stepd=4)) ty(outer_dia / 2 - 1) brim_tab(l=2, d=10);
        cylx(-1, 4, d=outer_dia);
    }
}

module base() {
    y1 = -125;
    y2 = gearbox_x2 + 10;

    width = gearbox_w + 18;

    battery_pos = [0, -75 - battery_size_9v.z, gearbox_h - battery_size_9v.y/2 - 3];

    part_color(0)
    difference() {
        union() {
            lxt(-12, gearbox_h) {
                squarex(xc=0, xs=width, y1 = y1, y2 = y2, r=2);
            }
        }
        ty(.5)
        tz(gearbox_h + eps)
        rx(90)
        gearbox_cutout(true, 70);

        ty([y2 - 5, gearbox_screw_x]) rfx() tx(gearbox_screw_spacing/2) {
            cylx(-15, gearbox_h + eps, d = screw_diameter_m3);
        }

        let(sz = battery_size_9v)
        translate(battery_pos) {
            cubex(
                xc = 0, xs = sz.x + .4,
                z1 = -sz.y/2, z2 = sz.y/2 + 3 + eps,
                y1 = -20, y2 = sz.z + 1
            );
            cubex(
                xc = 0, xs = sz.x + 6.4,
                z1 = -sz.y/2 - 3, z2 = sz.y/2 + 3 + eps,
                y1 = 0, y2 = 30.5
            );
        }
    }

    tz(-12)
    part_color("brim", "yellow", flag=$preview_brim)
    render(10) {

        rfy2(y2, y1) rfx() tx(width/2 - 4)
        brim_tab(l=2, w=4, d=16);

        ty([y1 + 5, y2 - 5, lerp(y1, y2, .5)])
        rfx() tx(width/2) rz(-90)

        brim_tab(l=2, w=4, d=16);

    }
}


module spacing_test() {
    lxt(0, 1.6) difference() {
        hull() {
            circle(d=8);
            tx(gearbox_screw_x) rfy() ty(gearbox_screw_spacing/2) circle(d=6);
        }
        circle(d=5.5);
        tx(gearbox_screw_x) rfy() ty(gearbox_screw_spacing/2) circle(d=screw_diameter_m3);

    }
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
        } else {
            complete();
        }
    }
    else if (part == "base") {
        if ($preview) {
            base();
        } else {
            rz(90)
            base();
        }
    }
    else if (part == "adapter") {
        if ($preview) {
            adapter();
        } else {
            ry(180)
            adapter();
        }
    }
    else if (part == "top") {
        if ($preview) {
            top();
        } else {
            top();
        }
    }
    else if (part == "motor-interface") {
        if ($preview) {
            motor_interface();
        } else {
            motor_interface();
        }
    }
    else if (part == "spacing-test") {
        if ($preview) {
            spacing_test();
        } else {
            spacing_test();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
