include <common.scad>

part = "main"; // [main, stopper, endclip]
capsize = "23g"; // [23g, 18g]
preview_cap = true;
preview_syringe = true;
preview_endclip = true;
preview_syringe_pos = 0; // [0 : 0.5 : 80]
preview_stopper = true;

$fn = 50;

fit_clearance = 0.2;

syringe_length = 74;
cap_length = 43.2;

front_length_a = 2.5;
front_length_b = 5;

rear_length = 12;

zp1 = 46;
zp2 = 5 + cap_length;

stopper_ang = 6;
stopper_len = 45;
stopper_screw_depth = -7.2;
stopper_head_width = 7.5;
stopper_width = 4.8;
stopper_paddle_width = 11;
stopper_depth = 6.5;

stopper_depth_ofs = 4 * tan(stopper_ang);

cap_lip_diameter = 8.6;

module cylinder_cutout(z1, z2, d, d2=undef) {
    d2 = is_num(d2) ? d2 : d;

    tz(z1-eps)
    cylinder(h=z2-z1+eps2, d=d);

    if (d2)
    cubex(
        xc=0, xs=d2,
        y1=-front_length_b-eps, y2=0,
        z1=z1-eps, z2=z2+eps
    );
}

module tip_23g(hh) {
    cylinder(d=cap_tip_width_23g + fit_clearance*2, h=hh);
    for (ang = [0, 90, 180, 270]) rz(ang) {
        tx(cap_tip_width_23g/2 + fit_clearance)
        cylinder(h=hh, d=1.35);
    }
}


//!hub_tip_18g();

module interior_23g() {
    cap_lip_diameter = 8.6;
    cap_hub_length = 14;

    cylx(-14, -1, d=cap_diameter_23g + 2*fit_clearance);
    cylx(-1 - fit_clearance, fit_clearance, d=cap_lip_diameter + fit_clearance);
    tz(-cap_length)
    tip_23g(cap_length+eps2);
}

module interior_18g() {
    cap_diameter_18g = 6.6;
    cap_diameter_18g_2 = 6.2;
    cap_hub_length = 25;

    cylx(-cap_length, -1, d=cap_diameter_23g + 2*fit_clearance);
    cylx(-1 - fit_clearance, fit_clearance, d=cap_lip_diameter + fit_clearance);
}

module screw_m2() {
    cylx(z1=-10, z2=eps, d=screw_diameter_m2);
    cylx(z1=0, z2=1.6, d1=screw_diameter_m2, d2=4.9);
    cylx(z1=1.6-eps, z2=5, d=4.9);
}

//screw_m2();

module cap_stopper() {
    y1 = -.5;
    y2 = -7.5;
    y3 = -12;
    y4 = -stopper_len + 3;
    y5 = -stopper_len + .5;
    difference() {
        union() {
            cubex(
                xc=0, xs=stopper_head_width,
                y1=y1, y2=y2,
                z1=0, z2=.9
            );
            *difference() {
                cubex(
                    xc=0, xs=stopper_head_width,
                    y1=y1, y2=y2,
                    z1=0, z2=1.8
                );
                ty(-4)
                rx(stopper_ang)
                tz(stopper_depth)
                tz(stopper_screw_depth) {
                    cubex(
                        xc=0, xs=10,
                        yc=0, ys=10,
                        z1=1.8, z2=10
                    );
                }
            }
        }
        ty(-4)
        rx(stopper_ang)
        tz(stopper_depth)
        tz(stopper_screw_depth)
        screw_m2();
    }
    cubex(
        xc=0, xs=stopper_width,
        y1=y2, y2=y3,
        z1=0, z2=.9
    );
    difference() {
        union() {
            cubex(
                xc=0, xs=stopper_width,
                y1=y3, y2=y4,
                z1=0, z2=.9
            );
            cubex(
                xc=0, xs=stopper_paddle_width,
                y1=y4, y2=y5,
                z1=0, z2=.9
            );
        }
        cubex(
            xc=0, xs=2,
            y1=y5+20, y2=y5-eps,
            z1=.3, z2=2
        );
    }

    *ty(-4)
    rx(stopper_ang)
    tz(stopper_screw_depth)
    {
        screw_m2();
    }

}
//!cap_stopper();

module main_body() {
    difference() {
        union() {
            cubex(
                xc=0, xs=16,
                y1=-front_length_a, y2=rear_length,
                z1=zp1, z2=syringe_length,
                r1=2, r2=2
            );
            cubex(
                xc=0, xs=16,
                y1=-front_length_b, y2=rear_length,
                z1=-cap_length + 20, z2=zp1,
                r1=2, r2=2
            );

            *cubex(
                xc=0, xs=30,
                yc=0, ys=30,
                z1=-cap_length - 5, z2=-cap_length - 3,
                r=5
            );

        }

        *tz(stopper_len)
        ty(7)
        rx(-60)
        cubex(
            xc=0, xs=6.3,
            y1=-7, y2=0,
            z1=-5, z2=0
        );

        // main groove
        cubex(
            xc=0, xs=stopper_width + .6,
            y1=0, y2=stopper_depth - stopper_depth_ofs,
            z1=0, z2=stopper_len - 8 + eps
        );

        spw = stopper_paddle_width + 1.2;
        // wide point at bottom
        cubex(
            xc=0, xs=spw,
            y1=cap_lip_diameter/2 - .1 - eps, y2=stopper_depth - stopper_depth_ofs,
            z1=0, z2=4
        );

        ty(cap_lip_diameter/2 - .1)
        linear_extrude(syringe_length+eps)
        polygon([
            [-spw/2, -1.2],
            [-spw/2,  0],
            [ spw/2,  0],
            [ spw/2, -1.2],
            [0, -4],
        ]);

        tz(stopper_len - 4)
        {

            ty(stopper_depth)
            ry(-90)
            linear_extrude(stopper_head_width + .3, center=true, convexity=3)
            polygon([
                [4, stopper_depth_ofs],
                [-4, -stopper_depth_ofs],
                [-4, -stopper_depth],
                [4, -stopper_depth]
            ]);


            ty(7)
            tz(4)
            rx(-stopper_ang) {
                *cubex(
                    xc=0, xs=stopper_head_width + .3,
                    y1=-6, y2=0,
                    z1=-9, z2=0
                );
            }

            rx(90) {
                //tz(-7)
                tz(stopper_screw_depth)
                screw_m2();
                tz(-12-eps)
                hexnut_hole(h=3, d=nut_diameter_m2);
            }
        }
        *cubex(
            xc=0, xs=16+eps,
            y1=-front_length_b-eps, y2=1,
            z1=-45-eps, zs=25
        );

        cylinder_cutout(0, zp1, syringe_diameter + fit_clearance, 6);
        cylinder_cutout(zp1, syringe_length, syringe_diameter + fit_clearance);
    }
    tz(stopper_len - 4)
    ty(9)
    rx(90)
    bridge_support();

}

module interior() {
    if (capsize == "18g") interior_18g();
    if (capsize == "23g") interior_23g();
}

module main() {
    difference() {
        main_body();
        interior();
    }
    if ($preview) {
        if (preview_stopper) {
            color("green")
            tz(stopper_len - 4)
            rx(90)
            tz(-stopper_depth)
            rx(-stopper_ang)
            ty(4)
            cap_stopper();
        }

        if (preview_syringe) {
            color("blue", 0.5)
            tz(preview_syringe_pos-18)
            syringe_model();
        }

        if (preview_cap) {
            color("red", 0.5)
            interior();
        }

        if (preview_endclip) {
            color("gold")
            render(convexity=4)
            tz(syringe_length+12)
            ry(180)
            end_clip();
        }
    }
}

module end_clip(h=12, ovr=6) {
    difference() {
        cubex(
            xc=0, xs=19.5,
            y1=-front_length_b+1.2, y2=rear_length-eps,
            z1=0, z2=h+ovr,
            r1=2, r2=2
        );
        cylx(z1=11, z2=h+ovr+eps, d=syringe_diameter + fit_clearance);
        cylx(z1=10, z2=11+eps, d1=syringe_diameter_2+.5, d2=syringe_diameter + fit_clearance);
        cylx(z1=-eps, z2=10+eps, d=syringe_diameter_2+.5);
        cubex(
            xc=0, xs=syringe_diameter_2+.5,
            y1=0, ys=-16,
            z1=-eps, z2=h+ovr+eps
        );

        minkowski(convexity=4) {
            render(convexity=2)
            intersection() {
                tz(syringe_length + h)
                sz(-1)
                difference() {
                    main_body();
                    interior();
                }
                cubex(
                    xc=0, xs=19,
                    y1=-front_length_b-2, y2=rear_length+2,
                    z1=h - 1, z2=h + ovr + 1
                );
            }
            cube(0.075, center=true);
        }
    }


}

if (part == "main") {
    if ($preview) {
        //zslice(z1=-5, z2=2)
        //xslice(x1=-4.5, x2=0.5)
        rx(-90)
        main();
    } else {
        rx(-90)
        main();
    }
}
if (part == "stopper") {
    cap_stopper();
}
if (part == "endclip") {
    end_clip();
}
