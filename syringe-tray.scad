include <common.scad>

/* [Common] */
part = "complete"; // [complete, main, lid, latch, lidpattern, lidpattern-top]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : 1 : 50]

/* [Preview parts] */
preview_main = true;
lid_ang = 0; // [0 : 1 : 180]

/* [Parameters] */
enable_pattern = false;

module __nc__() {} // end of customizer variables

layer_height = .2;

fit_clearance = 0.1;

cap_height = 11;
cap_diameter = 13.4;

tray_width_extra = 10;
tray_count = 5;
tray_spacing = 18;

depth = 16;

outer_radius = 8;

hinge_size = 8.1;

pri_width = tray_count * tray_spacing;

pattern_layers = 2;
pattern_thickness = layer_height * pattern_layers;

outer_extent_a = [
    tray_width_extra,
    98,
    16
];
outer_extent_b = [
    tray_width_extra + pri_width,
    80,
    16
];

height1 = 67;
height2 = 8.4;
height3 = 1.5;

tower_y1 = -12;
tower_y2 = syringe_diameter/2;

z3 = height1 + height2;
z4 = z3 + height3;
z1 = height1;
z2 = z1 + (syringe_diameter_2 - syringe_diameter);


module complete() {
    main();
    rc = [0, -outer_extent_a.y - hinge_size/2, 0];

    translate(rc)
    rx(lid_ang)
    translate(-rc)
    tz(0.4)
    lid();

    ty(outer_extent_b.y)
    tx(pri_width/2)
    tz(-6)
    rx(-90)
    rz(180)
    latch();
}

module syringe_cutout() {
    rx(-90)
    tz(-z4) {
        cylx(
            -cap_height - 2*fit_clearance, eps,
            d=cap_diameter + 2*fit_clearance
        );
        cylx(
            -eps, z4+eps,
            d=syringe_diameter + fit_clearance
        );

        cylx(
            z1 - eps, z2 + eps,
            d1=syringe_diameter + fit_clearance,
            d2=syringe_diameter_2 + fit_clearance
        );
        cylx(
            z2 - eps, z3 + eps,
            d=syringe_diameter_2 + fit_clearance
        );

        cylx(
            z3 - eps, z4 + eps,
            d1=syringe_diameter_2 + fit_clearance,
            d2=syringe_diameter_2 + fit_clearance + 2
        );
    }
    cubex(
        xc=0, xs=tray_spacing + eps,
        x2=tray_width_extra + tray_count * tray_spacing - 3,
        y1=0, y2=4,
        z1=-4, z2=4
    );
    cubex(
        xc=0, xs=tray_spacing - 2,
        x2=tray_width_extra + tray_count * tray_spacing - 3,
        y1=0, y2=4,
        z1=-outer_extent_a.z + 1, z2=outer_extent_a.z - 1
    );

    rx(-90)
    cylx(z1=4 - eps, z2=outer_extent_b.y - 10, d=12);

    *cubex(
        xc=0, xs=tray_spacing - 3,
        x2=tray_width_extra + tray_count * tray_spacing - 3,
        y1=10-eps, y2=outer_extent_b.y - 8,
        z1=-7, z2=7
    );
}

module syringe_cutouts() {
    for (i = [0 : 1 : tray_count-1]) {
        tx(tray_spacing * (i + .5)) {
            syringe_cutout();
        }
    }
}

module lid_groove(fc) {
    r = 2 + fc;
    sz(-1)
    for (yy = [-outer_extent_a.y + 40, outer_extent_b.y - 15])
    ty(yy) {
        tz(-5)
        cylinder(h=8+eps, r=r);
        tz(3)
        cylinder(h=r/2, r1=r, r2=r/2);
    }
}

module mainbox_outer() {
    minkowski(convexity=4) {
        cubex(
            x1 = -outer_extent_a.x + outer_radius,
            x2 = outer_extent_b.x - outer_radius,
            y1 = -outer_extent_a.y + outer_radius,
            y2 = outer_extent_b.y - outer_radius,
            z1 = -outer_extent_a.z,
            z2 = 0 - outer_radius/2
        );
        union() {
            cylinder(h=outer_radius/2, r1=outer_radius, r2=outer_radius/2);
            cylinder(h=1, r=outer_radius);
        }

    }

}

module tray_groove() {
    for (xx = [-outer_extent_a.x + 20, outer_extent_b.x - 20]) tx(xx) {
        sz(.6) {
            ry(45)
            cubex(xc=0, xs=5, zc=0, zs=5, y1=-outer_extent_a.y-eps, y2=outer_extent_b.y+eps);
            cubex(xc=0, xs=5*sqrt(2), z1=0, zs=-5, y1=-outer_extent_a.y-eps, y2=outer_extent_b.y+eps);
        }
    }
}

module infill_split(y1=-outer_extent_a.y, y2=outer_extent_b.y, xs=.1, zs=0.4) {
    npts = 5;
    tx(-outer_extent_a.x)
    for (xx = [1 : 1 : npts-1]) {
        tx(xx*(outer_extent_a.x+outer_extent_b.x)/npts)
        cubex(
            xc=0, xs=xs,
            y1=y1, y2=y2,
            z1=0, zs=zs
        );
    }
}

module hinge_transform() {
    ty(-outer_extent_a.y+eps2)
    tx(pri_width / 2)
    for (xx = [-1, 1]) sx(xx)
    tx(pri_width / 2 - 10)
    children();
}


module screw_cutout() {
    tx(pri_width/2)
    ty(outer_extent_b.y)
    xzy() {
        tz(-8) {
            cylinder(h=8, d=screw_diameter_m3);
            tz(1)
            hull() {
                for (yy = [0, 10]) ty(yy)
                rz(30)
                hexnut_hole(h=2.5, d=nut_diameter_m3);
            }
        }
    }
}

function reverse(v) = [for (i = [len(v) - 1 : -1 : 0]) v[i]];

module latch_cut(l, dia, main_dia) {
    ang_a = 90;
    ang_b = 0;
    npts = 50;
    r1_a = l + main_dia/2 - 0.5;
    r1_b = l + main_dia/2 + 1.5;
    r2_a = l - main_dia/2;
    r2_b = dia/2;

    function polar(r, t) = [r*cos(t), r*sin(t)];
    function latchpts(v, ra, rb) = [
        for (i=v) polar(lerp(ra, rb, i), lerp(ang_a, ang_b, i))
    ];

    ppts = [
        each latchpts([0 : 1/npts : 1], r1_a, r1_b),
        each latchpts([1 : -1/npts : 0], r2_a, r2_b)
    ];

    polygon(ppts);
}

module latch(h1=5, h2=5, l=11.2, dia=10) {
    head_dia = 5.62;

    s_thick = 1.6;


    difference() {
        union() {
            hull() {
                for (yy = [0, l]) ty(yy)
                cylinder(h1, d=dia);
            }
            //cylx(z1=0, z2=h2, d=dia);
        }

        cylx(z1=-eps, z2=h2+eps, d=screw_diameter_m3);
        cylx(z1=s_thick, z2=h2+eps, d=head_dia+.1);

        tz(s_thick)
        linear_extrude(h1+eps2, convexity=3)
        latch_cut(l, dia, head_dia);

        tz(-eps)
        linear_extrude(h1+eps2, convexity=3)
        latch_cut(l, 1.5*dia, screw_diameter_m3);

        ty(l) {
            cylx(z1=s_thick, z2=h1+eps, d=head_dia);
            cylx(z1=-eps, z2=h2+eps, d=screw_diameter_m3);
        }

    }
}

module e_molecule() {
    scl = 0.5;
    imgw = 159;
    imgh = 97;

    tz(outer_extent_b.z - pattern_thickness)
    tx(pri_width/2)
    ty(20)

    color("gold")
    linear_extrude(pattern_thickness+eps)
    scale([scl, scl])
    tx(-imgw/2)
    ty(-imgh/2)
    ty(-5) tx(-5)
    offset(.75)
    import("estradiol-molecule.svg");
}


module lidpattern() {
    e_molecule();

    color("gold")
    tz(outer_extent_b.z - pattern_thickness)
    difference() {
        infill_split(
            y1=-outer_extent_a.y + outer_radius/2 + 1,
            y2=outer_extent_b.y - outer_radius/2 - 1,
            xs=3,
            zs=pattern_thickness + eps
        );

        linear_extrude(pattern_thickness*3, center=true, convexity=10)
        offset(6)
        //offset(-5)
        projection()
        //hull()
        e_molecule();
    }
}

module lid() {
    difference() {
        color("#333333")
        minkowski() {
            cubex(
                x1 = -outer_extent_a.x + outer_radius,
                x2 = outer_extent_b.x - outer_radius,
                y1 = -outer_extent_a.y + outer_radius,
                y2 = outer_extent_b.y - outer_radius,
                z1 = -outer_radius,
                z2 = outer_extent_b.z - outer_radius/2
            );
            cylinder(h=outer_radius/2, r1=outer_radius, r2=outer_radius/2);
        }
        minkowski() {
            tz(.2)
            mainbox_outer();
            cube(0.005, center=true);
        }

        syringe_cutouts();

        *tz(outer_extent_b.z)
        sz(-1)
        tray_groove();

        sz(-1)
        tz(-5)
        screw_cutout();

        if (enable_pattern) {
            lidpattern();
        }



        *tz(outer_extent_b.z)
        infill_split();
    }

    tz(-.2) {
        tx(-outer_extent_a.x + 8)
        lid_groove(-fit_clearance);

        tx(outer_extent_b.x - 8)
        lid_groove(-fit_clearance);

        ty(-1)
        hinge_transform()
        hinge_inner(base_size=hinge_size-2, xlen=1+eps2, vofs1=4.5, vofs2=0.65);
    }

}
//!tx(outer_extent_b.x-outer_extent_a.x) ry(180) lid();

module main() {
    difference() {
        color("#333333")
        mainbox_outer();

        tz(eps)
        tx(-outer_extent_a.x + 8)
        lid_groove(fit_clearance);

        tz(eps)
        tx(outer_extent_b.x - 8)
        lid_groove(fit_clearance);

        syringe_cutouts();

        cubex(
            x1 = -outer_extent_a.x,
            x2 = outer_extent_b.x,
            y1 = -outer_extent_a.y,
            y2 = outer_extent_b.y,
            z1 = -.8,
            z2 = eps
        );

        *tz(-outer_extent_a.z)
        tray_groove();

        tz(-6)
        screw_cutout();

        *tz(-outer_extent_a.z)
        infill_split(zs=0.2);
    }

    difference() {
        cubex(
            x1=-outer_extent_a.x + 28, x2=outer_extent_b.x - 28,
            y1=outer_extent_b.y-eps, ys=15+eps,
            z1=-outer_extent_b.z, zs=4,
            r3=8, r4=8
        );
        cubex(
            x1=-outer_extent_a.x + 31, x2=outer_extent_b.x - 31,
            y1=outer_extent_b.y-eps2, ys=12+eps2,
            z1=-outer_extent_b.z-eps, zs=4+eps2,
            r3=5, r4=5
        );
    }

    tz(-hinge_size/2)
    hinge_transform()
    sx(-1)
    hinge_outer(base_size=hinge_size, ww=16, cutofs=1);
}

module lidpattern_top() {
    tz(outer_extent_b.z-pattern_thickness)
    render(10)
    intersection() {
        cubex(
            x1=-outer_extent_a.x, x2=outer_extent_b.x,
            y1=-outer_extent_a.y+5, y2=outer_extent_b.y-5,
            z1=-layer_height, z2=0
        );

        // higher $fn causes CGAL error; we don't really need smooth lines here
        sz(-1)
        linear_extrude(layer_height)
        offset(1.5, $fn=10) projection(0) lidpattern();
    }
}

module preview_main() {
}

module preview_lid() {
}

module preview_latch() {
}

module preview_lidpattern() {
}

module preview_lidpattern_top() {
}

module preview_complete() {
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
            preview_complete();
        } else {
            complete();
        }
    }
    else if (part == "main") {
        if ($preview) {
            main();
            preview_main();
        } else {
            main();
        }
    }
    else if (part == "lid") {
        if ($preview) {
            lid();
            preview_lid();
        } else {
            rx(180)
            lid();
        }
    }
    else if (part == "latch") {
        if ($preview) {
            latch();
            preview_latch();
        } else {
            latch();
        }
    }
    else if (part == "lidpattern") {
        if ($preview) {
            lidpattern();
            preview_lidpattern();
        } else {
            rx(180)
            lidpattern();
        }
    }
    else if (part == "lidpattern-top") {
        if ($preview) {
            lidpattern_top();
            preview_lidpattern_top();
        } else {
            rx(180)
            lidpattern_top();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
