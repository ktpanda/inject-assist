include <common.scad>
use <injector.scad>
use <cap-puller.scad>

$fn = $preview ? 30 : 50;

part = "main"; // [main, tip]
friction_fit = true;
preview_ang = 0; // [0 : 1: 360]
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-100 : 1 : 100]
preview_tip = true;

height = 90;
ofs = 6;

module hub_tip_18g(fc=fit_clearance) {
    zp1 = 0;
    zp2 = needle_fin_length_18g;
    zp3 = zp2 + .7;
    zp4 = zp2 + 4;
    zp5 = needle_hub_length;


    cylx(
        z1=zp1-eps,
        z2=zp2+eps,
        d=needle_fin_hub_diameter_18g + 2*fc
    );

    cylx(
        z1=zp2-eps,
        z2=zp3+eps,
        d1=needle_fin_hub_diameter_18g + 2*fc,
        d2=needle_hub_diameter*.75 + fc*2
    );
    cylx(
        z1=zp3-eps,
        z2=zp4+eps,
        d1=needle_hub_diameter*.75 + fc*2,
        d2=needle_hub_diameter + fc*2
    );

    tz(needle_fin_length_18g*2)
    cylinder(
        h=needle_hub_length - needle_fin_length_18g*2 + eps,
        d=needle_hub_diameter + fc*2
    );
    for (ang = [0 : 45 : 360]) rz(ang) {
        rx(90)
        linear_extrude(needle_fin_thickness_18g + fc, center=true)
        polygon([
            [4/2 + fc, -eps],
            [4.5/2 + fc, 3.6],
            [5.7/2 + fc, 3.7],
            [5.7/2 + fc, 11],
            [0, 11],
            [0, -eps],
        ]);

        *cubex(
            xc=0, xs=needle_fin_width_18g + fc*2,
            yc=0, ys=needle_fin_thickness_18g + fc,
            z1=-eps, z2=needle_fin_length_18g*2+eps
        );
    }
}

module view_cutout(ang, ofs) {
    pz1 = needle_hub_length + 15;
    pz2 = height+eps - ofs;
    pw = 1.5;

    rz(ang * 45)
    ry(-90)
    linear_extrude(30)
    polygon([
        [pz1, -pw],
        [pz2, -pw],
        [pz2+3, 0],
        [pz2,  pw],
        [pz1,  pw],
    ]);
}

module tip_outer_groove() {
    cubex(
        xc=0, xs=10+eps,
        yc=0, ys=2,
        z1=5, z2=10.5
    );
}

module tip_outer(fc=0, groove=false) {
    cylx(0, 5+eps, d=26);
    //outer ring
    difference() {
        cylx(5, 14+eps, d=26);
        cylx(5-eps, 14+eps2, d=16.4);
    }

    cylx(5, 8+eps, d1=12 + fc, d2=8 + fc);
    if (friction_fit) {
        cylx(8, 10.2+eps, d=8 + fc);
        cylx(10.2, 12+eps, d1=8+fc, d2=8.3 + fc);
    } else {
        cylx(8, 10.5+eps, d=8 + fc);
        if (groove) {
            cylx(9, 10.5+eps, d1=8 + fc, d2=10 + fc);
            tip_outer_groove();
        } else {
            intersection() {
                cylx(9, 10.5+eps, d1=8 + fc, d2=10 + fc);
                tip_outer_groove();
            }
        }
    }
}

module main() {
    tz(-ofs)
    difference() {
        union() {
            //main_tower_outer(false, false, height=height, y1=-8);
            cylx(0, height, d=16);
        }

        ofs1 = 8;
        ofs2 = 15;
        //view_cutout(0, ofs1);
        //view_cutout(1, ofs1);
        //view_cutout(2, ofs1);
        //view_cutout(3, ofs1);
        //view_cutout(4, ofs1);
        //view_cutout(5, ofs2);
        //view_cutout(6, ofs2);
        //view_cutout(7, ofs2);

        cubex(
            xc=0, xs=30,
            yc=3, ys=30,
            z1=-10, z2=ofs+5+eps2
        );

        main_tower_inner(height, false);

        tz(ofs)
        tip_outer(friction_fit ? 0.2 : 0.1, true);
    }
}

module tip() {
    difference() {
        tip_outer(friction_fit ? 0.2 : 0);
        hub_tip_18g();
    }
}

preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "main") {
        main();
        if ($preview && preview_tip) {
            rz(preview_ang)
            color("gold")
            tip();
        }
    }
    else if (part == "tip") {
        tip();
    }
    else {
        assert(false, str("invalid part ", part));
    }
}